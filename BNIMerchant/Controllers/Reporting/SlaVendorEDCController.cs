﻿using BNIMerchant.Filter;
using BNIMerchant.Helpers;
using BNIMerchant.Models;
using BNIMerchant.Models.Reporting;
using BNIMerchant.Services;
using BNIMerchant.Wrappers;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;

namespace BNIMerchant.Controllers.Reporting
{
    [Route("api/[controller]")]
    [ApiController]
    public class SlaVendorEDCController : ControllerBase
    {
        private readonly BNIContext _context;
        private readonly IUriService uriService;

        public SlaVendorEDCController(BNIContext context, IUriService uriService)
        {
            _context = context;
            this.uriService = uriService;
        }

        [HttpGet]
        public async Task<IActionResult> GetAll([FromQuery] PaginationFilter filter)
        {
            var route = Request.Path.Value;
            var validFilter = new PaginationFilter(filter.PageNumber, filter.PageSize);
            var pagedData = await _context.sla_vendor_edc
                //.Skip((validFilter.PageNumber - 1) * validFilter.PageSize)
                //.Take(validFilter.PageSize)
                .ToListAsync();
            var totalRecords = await _context.sla_vendor_edc.CountAsync();
            var pagedReponse = PaginationHelper.CreatePagedReponse<SlaVendorEDC>(pagedData, validFilter, totalRecords, uriService, route);
            return Ok(pagedData);
        }

        // GET: api/SlaVendorEDC/5
        [HttpGet("{id}", Name = "Get")]
        public async Task<IActionResult> GetById(int id)
        {
            var hasil = await _context.sla_vendor_edc.Where(a => a.id == id).FirstOrDefaultAsync();
            return Ok(new Response<SlaVendorEDC>(hasil));
        }

        // POST: api/SlaVendorEDC
        [HttpPost]
        public async Task<ActionResult<SlaVendorEDC>> PostRole(SlaVendorEDC role)
        {
            _context.sla_vendor_edc.Add(role);
            await _context.SaveChangesAsync();

            var pagedData = _context.sla_vendor_edc;
            return Ok(pagedData);

        }

        // PUT: api/SlaVendorEDC/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutRole(int id, SlaVendorEDC sla_vendor_edc)
        {
            if (id != sla_vendor_edc.id)
            {
                return BadRequest();
            }

            _context.Entry(sla_vendor_edc).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
                var hasil = await _context.sla_vendor_edc.Where(a => a.id == id).FirstOrDefaultAsync();
                return Ok(new Response<SlaVendorEDC>(hasil));
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!RoleExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }


        }

        private bool RoleExists(int id)
        {
            return _context.sla_vendor_edc.Any(e => e.id == id);
        }

        // DELETE api/<RolesController>/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteRole(int id)
        {
            var role = await _context.sla_vendor_edc.FindAsync(id);
            if (role == null)
            {
                return NotFound();
            }

            _context.sla_vendor_edc.Remove(role);
            await _context.SaveChangesAsync();

            //return NoContent();

            var pagedData = _context.sla_vendor_edc;
            return Ok(pagedData);
        }
    }
}
