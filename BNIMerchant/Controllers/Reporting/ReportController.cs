﻿using BNIMerchant.Filter;
using BNIMerchant.Helpers;
using BNIMerchant.Models;
using BNIMerchant.Models.Reporting;
using BNIMerchant.Services;
using BNIMerchant.Wrappers;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;

namespace BNIMerchant.Controllers.Reporting
{
    [Route("api/[controller]")]
    [ApiController]
    public class ReportController : ControllerBase
    {
        private readonly BNIContext _context;
        private readonly IUriService uriService;

        public ReportController(BNIContext context, IUriService uriService)
        {
            _context = context;
            this.uriService = uriService;
        }

        // GET: api/Report
        [HttpGet]
        public async Task<IActionResult> GetAll([FromQuery] PaginationFilter filter)
        {
            var route = Request.Path.Value;
            var validFilter = new PaginationFilter(filter.PageNumber, filter.PageSize);
            var pagedData = await _context.reporting
                //.Skip((validFilter.PageNumber - 1) * validFilter.PageSize)
                //.Take(validFilter.PageSize)
                .ToListAsync();
            var totalRecords = await _context.reporting.CountAsync();
            var pagedReponse = PaginationHelper.CreatePagedReponse<Report>(pagedData, validFilter, totalRecords, uriService, route);
            return Ok(pagedData);
        }

        // GET: api/Report/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetById(int id)
        {
            var hasil = await _context.reporting.Where(a => a.id == id).FirstOrDefaultAsync();
            return Ok(new Response<Report>(hasil));
        }


        // POST: api/Report
        [HttpPost]
        public async Task<ActionResult<Report>> PostReport(Report reporting)
        {
            _context.reporting.Add(reporting);
            await _context.SaveChangesAsync();

            var pagedData = _context.reporting;
            return Ok(pagedData);

        }

        // PUT: api/Report/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutRole(int id, Report reporting)
        {
            if (id != reporting.id)
            {
                return BadRequest();
            }

            _context.Entry(reporting).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
                var hasil = await _context.reporting.Where(a => a.id == id).FirstOrDefaultAsync();
                return Ok(new Response<Report>(hasil));
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!RoleExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }


        }

        private bool RoleExists(int id)
        {
            return _context.reporting.Any(e => e.id == id);
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteReport(int id)
        {
            var role = await _context.reporting.FindAsync(id);
            if (role == null)
            {
                return NotFound();
            }

            _context.reporting.Remove(role);
            await _context.SaveChangesAsync();

            //return NoContent();

            var pagedData = _context.reporting;
            return Ok(pagedData);
        }
    }
}
