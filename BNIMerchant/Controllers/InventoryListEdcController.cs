﻿using BNIMerchant.Filter;
using BNIMerchant.Helpers;
using BNIMerchant.Models;
using BNIMerchant.Services;
using BNIMerchant.Wrappers;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace BNIMerchant.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class InventoryListEdcController : ControllerBase
    {
        private readonly BNIContext _context;
        private readonly IUriService uriService;

        public InventoryListEdcController(BNIContext context, IUriService uriService)
        {
            _context = context;
            this.uriService = uriService;
        }

        [HttpPost("fetchData")]
        public IActionResult fetchData()
        {
            try
            {
                var draw = Request.Form["draw"].FirstOrDefault();
                var start = Request.Form["start"].FirstOrDefault();
                var length = Request.Form["length"].FirstOrDefault();
                var sortColumn = Request.Form["columns[" + Request.Form["order[0][column]"].FirstOrDefault() + "][name]"].FirstOrDefault();
                var sortColumnDirection = Request.Form["order[0][dir]"].FirstOrDefault();
                var searchValue = Request.Form["search[value]"].FirstOrDefault();
                int pageSize = length != null ? Convert.ToInt32(length) : 0;
                int skip = start != null ? Convert.ToInt32(start) : 0;
                int recordsTotal = 0;
                var result = _context.Inventory.AsQueryable();
                if (!(string.IsNullOrEmpty(sortColumn) && string.IsNullOrEmpty(sortColumnDirection)))
                {
                    sortColumn = sortColumn + '_' + sortColumnDirection;
                    switch (sortColumn)
                    {
                        case "tid_bni_asc":
                            result = result.OrderBy(s => s.tid_bni);
                            break;
                        case "tid_bni_desc":
                            result = result.OrderByDescending(s => s.tid_bni);
                            break;
                        case "serial_number_sim_card_asc":
                            result = result.OrderBy(s => s.serial_number_sim_card);
                            break;
                        case "serial_number_sim_card_desc":
                            result = result.OrderByDescending(s => s.serial_number_sim_card);
                            break;
                        case "kota_asc":
                            result = result.OrderBy(s => s.kota);
                            break;
                        case "kota_desc":
                            result = result.OrderByDescending(s => s.kota);
                            break;
                        case "status_asc":
                            result = result.OrderBy(s => s.status);
                            break;
                        case "status_desc":
                            result = result.OrderByDescending(s => s.status);
                            break;
                        case "status1_asc":
                            result = result.OrderBy(s => s.status1);
                            break;
                        case "status1_desc":
                            result = result.OrderByDescending(s => s.status1);
                            break;
                        default:
                            result = result.OrderBy(s => s.tid_bni);
                            break;
                    }
                }
                if (!string.IsNullOrEmpty(searchValue))
                {
                    result = result.Where(m => m.tid_bni.Contains(searchValue) || m.serial_number_sim_card.Contains(searchValue)
                                                || m.kota.Contains(searchValue) || m.status.Contains(searchValue)
                                                || m.status1.Contains(searchValue));
                }
                recordsTotal = result.Count();
                var data = result.Skip(skip).Take(pageSize).ToList();
                var jsonData = new { draw = draw, recordsFiltered = recordsTotal, recordsTotal = recordsTotal, data = data };
                return Ok(jsonData);
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        [HttpGet]
        public async Task<IActionResult> GetAll([FromQuery] PaginationFilter filter)
        {
            var route = Request.Path.Value;
            var validFilter = new PaginationFilter(filter.PageNumber, filter.PageSize);
            var pagedData = await _context.Inventory
                //.Skip((validFilter.PageNumber - 1) * validFilter.PageSize)
                //.Take(validFilter.PageSize)
                .ToListAsync();
            var totalRecords = await _context.Inventory.CountAsync();
            var pagedReponse = PaginationHelper.CreatePagedReponse<InventoryListEdc>(pagedData, validFilter, totalRecords, uriService, route);
            return Ok(pagedData);
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetById(int id)
        {
            var hasil = await _context.Inventory.Where(a => a.id_inventory == id).FirstOrDefaultAsync();
            return Ok(new Response<InventoryListEdc>(hasil));
        }

        [HttpPost]
        public async Task<ActionResult<InventoryListEdc>> PostInventoryListEdc(InventoryListEdc edc_inventory_management)
        {
            _context.Inventory.Add(edc_inventory_management);

            await _context.SaveChangesAsync();

            var pagedData = _context.Inventory;
            return Ok(new ResponseStatus { Status = "Success", Message = "Inventory has been added", Data = edc_inventory_management });

        }

        // PUT: api/User/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutInventoryListEdc(int id, InventoryListEdc edc_inventory_management)
        {
            //if (id != edc_inventory_management.id_inventory)
            //{
            //    return BadRequest();
            //}

            //_context.Entry(edc_inventory_management).State = EntityState.Modified;

            try
            {

                var hasil = await _context.Inventory.Where(a => a.id_inventory == id).FirstOrDefaultAsync();
                hasil = edc_inventory_management;

                await _context.SaveChangesAsync();
                return Ok(new Response<InventoryListEdc>(hasil));
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!InventoryListEdcExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }


        }

        private bool InventoryListEdcExists(int id)
        {
            return _context.Inventory.Any(e => e.id_inventory == id);
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteInventoryListEdc(int id)
        {
            var edc_inventory_management = await _context.Inventory.FindAsync(id);
            if (edc_inventory_management == null)
            {
                return NotFound();
            }

            _context.Inventory.Remove(edc_inventory_management);
            await _context.SaveChangesAsync();

            //return NoContent();

            var pagedData = _context.Inventory;
            return Ok(pagedData);
        }
    }
}
