﻿using BNIMerchant.Auth;
using BNIMerchant.Models;
using BNIMerchant.Models.StatusPemasangan;
using BNIMerchant.Wrappers;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections;
using System.IO;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;

namespace BNIMerchant.Controllers.StatusPemasangan
{
    [Route("api/[controller]")]
    [ApiController]
    public class StatusPemasanganController : ControllerBase
    {
        private readonly BNIContext _context;
        private readonly IAuthenticateManager authenticateManager;
        private static IWebHostEnvironment _environment;
        public StatusPemasanganController(BNIContext context, IAuthenticateManager authenticateManager, IWebHostEnvironment environment)
        {
            _context = context;
            this.authenticateManager = authenticateManager;
            _environment = environment;
        }

        [HttpPost("fetchData")]
        public IActionResult fetchData()
        {
            try
            {
                var draw = Request.Form["draw"].FirstOrDefault();
                var start = Request.Form["start"].FirstOrDefault();
                var length = Request.Form["length"].FirstOrDefault();
                var sortColumn = Request.Form["columns[" + Request.Form["order[0][column]"].FirstOrDefault() + "][name]"].FirstOrDefault();
                var sortColumnDirection = Request.Form["order[0][dir]"].FirstOrDefault();
                var searchValue = Request.Form["search[value]"].FirstOrDefault();
                int pageSize = length != null ? Convert.ToInt32(length) : 0;
                int skip = start != null ? Convert.ToInt32(start) : 0;
                int recordsTotal = 0;
                var result = _context.Status_Pemasangan.AsQueryable();
                if (!(string.IsNullOrEmpty(sortColumn) && string.IsNullOrEmpty(sortColumnDirection)))
                {
                    sortColumn = sortColumn + '_' + sortColumnDirection;
                    switch (sortColumn)
                    {
                        case "no_tugas_asc":
                            result = result.OrderBy(s => s.no_tugas);
                            break;
                        case "no_tugas_desc":
                            result = result.OrderByDescending(s => s.no_tugas);
                            break;
                        case "merchant_asc":
                            result = result.OrderBy(s => s.merchant);
                            break;
                        case "merchant_desc":
                            result = result.OrderByDescending(s => s.merchant);
                            break;
                        case "vendor_asc":
                            result = result.OrderBy(s => s.vendor);
                            break;
                        case "vendor_desc":
                            result = result.OrderByDescending(s => s.vendor);
                            break;
                        case "tid_asc":
                            result = result.OrderBy(s => s.tid);
                            break;
                        case "tid_desc":
                            result = result.OrderByDescending(s => s.tid);
                            break;
                        case "pic_vendor_asc":
                            result = result.OrderBy(s => s.pic_vendor);
                            break;
                        case "pic_vendor_desc":
                            result = result.OrderByDescending(s => s.pic_vendor);
                            break;
                        case "tanggal_penugasan_asc":
                            result = result.OrderBy(s => s.tanggal_penugasan);
                            break;
                        case "tanggal_penugasan_desc":
                            result = result.OrderByDescending(s => s.tanggal_penugasan);
                            break;
                        case "tanggal_dipasang_asc":
                            result = result.OrderBy(s => s.tanggal_dipasang);
                            break;
                        case "tanggal_dipasang_desc":
                            result = result.OrderByDescending(s => s.tanggal_dipasang);
                            break;
                        case "status_asc":
                            result = result.OrderBy(s => s.status);
                            break;
                        case "status_desc":
                            result = result.OrderByDescending(s => s.status);
                            break;
                        default:
                            result = result.OrderBy(s => s.no_tugas);
                            break;
                    }
                }
                if (!string.IsNullOrEmpty(searchValue))
                {
                    result = result.Where(m => m.no_tugas.Contains(searchValue) || m.merchant.Contains(searchValue)
                                                || m.vendor.Contains(searchValue) || m.tid.Contains(searchValue)
                                                || m.pic_vendor.Contains(searchValue) || m.tanggal_penugasan.ToString().Contains(searchValue)
                                                || m.tanggal_dipasang.ToString().Contains(searchValue) || m.status.Contains(searchValue));
                }
                recordsTotal = result.Count();
                var data = result.Skip(skip).Take(pageSize).ToList();
                var jsonData = new { draw = draw, recordsFiltered = recordsTotal, recordsTotal = recordsTotal, data = data };
                return Ok(jsonData);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public object ValidationStaging(Status_Pemasangan data, int if1, int if2, int valueStaging, int id_user)
        {
            if (data == null)
                return Ok(new ResponseStatus { Status = "Error", Message = "Data is empty or invalid input id", Data = null });
            else
            {
                if (data.staging == if1)
                    return Ok(new ResponseStatus { Status = "Success", Message = "You're in this stage", Data = data });
                else if (data.staging == if2)
                {
                    data.staging = valueStaging;
                    data.updated_time = DateTime.Now;
                    data.update_by = id_user;
                    _context.SaveChanges();
                    return Ok(new ResponseStatus { Status = "Success", Message = "Data Status Pemasangan has been retrieve", Data = data });
                }
                else
                    return Ok(new ResponseStatus { Status = "Error", Message = "You're currently not in this stage", Data = null });
            }
        }

        public object CheckAuth(string token)
        {
            var authorization = authenticateManager.isLoggedIn(token);
            if (authorization is false)
                return StatusCode(StatusCodes.Status401Unauthorized, new ResponseStatus { Status = "Error", Message = "You can't access this method !" });
            return StatusCode(100);
        }

        //Staging tahap 1 (Open)
        [HttpGet("StagingOpen/{id}")]
        public async Task<IActionResult> StagingOpen(int id, [FromHeader] string token)
        {
            CheckAuth(token);

            var authorization = authenticateManager.isLoggedIn(token);
            var hasil = await _context.Status_Pemasangan.Where(a => a.id_pemasangan == id).FirstOrDefaultAsync();

            var OkResult = ValidationStaging(hasil, 1, 0, 1, authorization.id_user);

            return Ok(OkResult);
        }

        //Staging tahap 2 (Acknowledge)
        [HttpGet("StagingAcknowledge/{id}")]
        public async Task<IActionResult> StagingAcknowledge(int id, [FromHeader] string token)
        {
            CheckAuth(token);

            var authorization = authenticateManager.isLoggedIn(token);
            var hasil = await _context.Status_Pemasangan.Where(a => a.id_pemasangan == id).FirstOrDefaultAsync();

            var OkResult = ValidationStaging(hasil, 2, 1, 2, authorization.id_user);

            return Ok(OkResult);
        }

        //Staging tahap 3 (Working)
        [HttpGet("StagingWorking")]
        public async Task<IActionResult> StagingWorking([FromBody] JsonElement data, [FromHeader] string token)
        {
            CheckAuth(token);

            var authorization = authenticateManager.isLoggedIn(token);
            var model = await _context.Status_Pemasangan.Where(x => x.id_pemasangan == data.GetProperty("id_pemasangan").GetInt32()).FirstOrDefaultAsync();

            if (model.staging == 3)
                return Ok(new ResponseStatus { Status = "Success", Message = "Data Tahap 3", Data = model });
            else if (model.staging == 2)
            {
                if (data.GetProperty("pic").ToString() == "" && data.GetProperty("hp").ToString() == "")
                    return Ok(new ResponseStatus { Status = "Success", Message = "Data Tahap 1", Data = model });
                else
                {
                    model.pic_vendor = data.GetProperty("pic").ToString();
                    model.hp = data.GetProperty("hp").ToString();
                    model.staging = 3;
                    model.update_by = authorization.id_user;
                    model.updated_time = DateTime.Now;
                    await _context.SaveChangesAsync();

                    return Ok(new ResponseStatus { Status = "Success", Message = "Data Tahap 2", Data = model });
                }
            }
            else
                return Ok(new ResponseStatus { Status = "Error", Message = "You're currently not in this stage", Data = null });
        }

        public class StagingSolvedModel
        {
            public IFormFile Toko { get; set; }
            public IFormFile FKM { get; set; }
            public IFormFile MesinEDC { get; set; }
            public IFormFile SlipTransaksi { get; set; }
            public int id_pemasangan { get; set; }
        }

        //Staging tahap 4 (Solving)
        [HttpGet("StagingSolving")]
        public async Task<IActionResult> StagingSolving([FromForm] StagingSolvedModel data, [FromHeader] string token)
        {
            CheckAuth(token);

            var authorization = authenticateManager.isLoggedIn(token);
            var model = await _context.Status_Pemasangan.Where(x => x.id_pemasangan == data.id_pemasangan).FirstOrDefaultAsync();

            var dateAndTime = DateTime.Now;
            var date = dateAndTime.ToString("yyyy/MM/dd");

            ArrayList listFile = new ArrayList();
            listFile.Add(data.Toko);
            listFile.Add(data.FKM);
            listFile.Add(data.MesinEDC);
            listFile.Add(data.SlipTransaksi);

            if (model.staging == 3)
            {
                if (model.status == "Pemasangan Gagal")
                {
                    return Ok(new ResponseStatus { Status = "Error", Message = "You're failed in stage 3, please fill 'Berita Acara' data", Data = null });
                }
                else if (model.status == "Working")
                {
                    if (!Directory.Exists(_environment.WebRootPath + "\\Upload\\Data_Status_Pemasangan\\" + date))
                    {
                        Directory.CreateDirectory(_environment.WebRootPath + "\\Upload\\Data_Status_Pemasangan\\" + date);
                    }

                    foreach (IFormFile afterListingData in listFile)
                    {
                        using (FileStream fileStream = System.IO.File.Create(_environment.WebRootPath + "\\Upload\\Data_Status_Pemasangan\\" + date + "\\" + (Int32)(DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1))).TotalSeconds + '_' + afterListingData.FileName))
                        {
                            afterListingData.CopyTo(fileStream);
                            fileStream.Flush();

                            var name_file = (Int32)(DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1))).TotalSeconds + "_" + afterListingData.FileName;

                            switch (afterListingData.Name)
                            {
                                case "Toko":
                                    model.photo_toko = name_file;
                                    break;
                                case "FKM":
                                    model.photo_fkm = name_file;
                                    break;
                                case "MesinEDC":
                                    model.photo_mesin_edc = name_file;
                                    break;
                                case "SlipTransaksi":
                                    model.photo_slip_transaksi = name_file;
                                    break;
                                default:
                                    break;
                            }
                            await _context.SaveChangesAsync();
                        }
                    }

                    model.status = "Pemasangan Berhasil";
                    model.staging = 4;
                    model.tanggal_dipasang = DateTime.Now;
                    model.updated_time = DateTime.Now;
                    model.update_by = authorization.id_user;
                    await _context.SaveChangesAsync();

                    return Ok(new ResponseStatus { Status = "Success", Message = "Data Status Pemasangan has done", Data = model });
                }
                else
                    return Ok(new ResponseStatus { Status = "Error", Message = "Complete your data in staging 3 first !", Data = null });
            }
            else if (model.staging == 4)
                return Ok(new ResponseStatus { Status = "Success", Message = "Data Status Pemasangan has been retrieve", Data = model });
            else
                return Ok(new ResponseStatus { Status = "Error", Message = "You're currently not in this stage", Data = null });
        }

        // Pemasangan Gagal
        [HttpPost("ValidateStaging/{id}")]
        public async Task<IActionResult> ValidateStaging(int id_pemasangan, [FromHeader] string token)
        {
            CheckAuth(token);

            var authorization = authenticateManager.isLoggedIn(token);
            var model = await _context.Status_Pemasangan.Where(x => x.id_pemasangan == id_pemasangan).FirstOrDefaultAsync();

            if (model == null)
                return Ok(new ResponseStatus { Status = "Error", Message = "Data is empty or invalid input id", Data = null });
            else
            {
                model.status = "Pemasangan Gagal";
                model.updated_time = DateTime.Now;
                model.update_by = authorization.id_user;

                await _context.SaveChangesAsync();
            }

            return Ok(new ResponseStatus { Status = "Success", Message = "Data has been changed", Data = model });
        }

        // Input Berita Acara
        [HttpPost("BeritaAcara/{id}")]
        public async Task<IActionResult> BeritaAcara(int id_pemasangan, [FromForm] JsonElement data, [FromForm] IFormFile evidence, [FromHeader] string token)
        {
            CheckAuth(token);

            var authorization = authenticateManager.isLoggedIn(token);
            var model = await _context.Status_Pemasangan.Where(x => x.id_pemasangan == id_pemasangan).FirstOrDefaultAsync();

            var dateAndTime = DateTime.Now;
            var date = dateAndTime.ToString("yyyy/MM/dd");

            if (model == null)
                return Ok(new ResponseStatus { Status = "Error", Message = "Data is empty or invalid input id", Data = null });
            else
            {
                if (evidence.Length > 0)
                {
                    try
                    {
                        if (!Directory.Exists(_environment.WebRootPath + "\\Upload\\Data_Status_Pemasangan\\" + date))
                            Directory.CreateDirectory(_environment.WebRootPath + "\\Upload\\Data_Status_Pemasangan\\" + date);

                        using (FileStream fileStream = System.IO.File.Create(_environment.WebRootPath + "\\Upload\\Data_Status_Pemasangan\\" + date + "\\" + (Int32)(DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1))).TotalSeconds + '_' + evidence.FileName))
                        {
                            evidence.CopyTo(fileStream);
                            fileStream.Flush();

                            var directory = (_environment.WebRootPath + "\\Upload\\Data MRF\\" + date + "\\" + (Int32)(DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1))).TotalSeconds + '_' + evidence.FileName);
                            var name_file = (Int32)(DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1))).TotalSeconds + "_" + evidence.FileName;

                            model.reason = data.GetProperty("reason").ToString();
                            model.info_remark = data.GetProperty("info_remark").ToString();
                            model.photo_evidence = name_file;
                            model.tid = null;
                            model.tanggal_dipasang = null;
                            model.update_by = authorization.id_user;
                            model.updated_time = DateTime.Now;

                            await _context.SaveChangesAsync();

                            return Ok(new ResponseStatus { Status = "Success", Message = "Data has been changed", Data = model });
                        }

                    }
                    catch (Exception)
                    {
                        throw;
                    }
                }
                else
                    return Ok(new ResponseStatus { Status = "Error", Message = "File hasn't uploaded" });
            }
        }
    }
}
