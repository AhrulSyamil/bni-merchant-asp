﻿using BNIMerchant.Filter;
using BNIMerchant.Helpers;
using BNIMerchant.Models;
using BNIMerchant.Models.Tools;
using BNIMerchant.Services;
using BNIMerchant.Wrappers;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;

namespace BNIMerchant.Controllers.Tools
{
    [Route("api/[controller]")]
    [ApiController]
    public class PicPelaksanaanController : ControllerBase
    {
        private readonly BNIContext _context;
        private readonly IUriService uriService;

        public PicPelaksanaanController(BNIContext context, IUriService uriService)
        {
            _context = context;
            this.uriService = uriService;
        }

        // GET: api/PicPelaksanaan
        [HttpGet]
        public async Task<IActionResult> GetAll([FromQuery] PaginationFilter filter)
        {
            var route = Request.Path.Value;
            var validFilter = new PaginationFilter(filter.PageNumber, filter.PageSize);
            var pagedData = await _context.pic_pelaksanaan
                //.Skip((validFilter.PageNumber - 1) * validFilter.PageSize)
                //.Take(validFilter.PageSize)
                .ToListAsync();
            var totalRecords = await _context.pic_pelaksanaan.CountAsync();
            var pagedReponse = PaginationHelper.CreatePagedReponse<PicPelaksanaan>(pagedData, validFilter, totalRecords, uriService, route);
            return Ok(pagedData);
        }


        // GET: api/PicPelaksanaan/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetById(int id)
        {
            var hasil = await _context.pic_pelaksanaan.Where(a => a.id == id).FirstOrDefaultAsync();
            return Ok(new Response<PicPelaksanaan>(hasil));
        }

        // POST: api/PicPelaksanaan
        [HttpPost]
        public async Task<ActionResult<PicPelaksanaan>> PostRole(PicPelaksanaan pic_pelaksanaan)
        {
            _context.pic_pelaksanaan.Add(pic_pelaksanaan);
            await _context.SaveChangesAsync();

            var pagedData = _context.pic_pelaksanaan;
            return Ok(pagedData);

        }

        // PUT: api/PicPelaksanaan/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutRole(int id, PicPelaksanaan pic_pelaksanaan)
        {
            if (id != pic_pelaksanaan.id)
            {
                return BadRequest();
            }

            _context.Entry(pic_pelaksanaan).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
                var hasil = await _context.pic_pelaksanaan.Where(a => a.id == id).FirstOrDefaultAsync();
                return Ok(new Response<PicPelaksanaan>(hasil));
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!RoleExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }


        }

        private bool RoleExists(int id)
        {
            return _context.pic_pelaksanaan.Any(e => e.id == id);
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeletePicPelaksanaan(int id)
        {
            var role = await _context.pic_pelaksanaan.FindAsync(id);
            if (role == null)
            {
                return NotFound();
            }

            _context.pic_pelaksanaan.Remove(role);
            await _context.SaveChangesAsync();

            //return NoContent();

            var pagedData = _context.pic_pelaksanaan;
            return Ok(pagedData);
        }
    }
}
