﻿using BNIMerchant.Filter;
using BNIMerchant.Helpers;
using BNIMerchant.Models;
using BNIMerchant.Models.Tools;
using BNIMerchant.Services;
using BNIMerchant.Wrappers;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;

namespace BNIMerchant.Controllers.Tools
{
    [Route("api/[controller]")]
    [ApiController]
    public class PengaturanIklanController : ControllerBase
    {
        private readonly BNIContext _context;
        private readonly IUriService uriService;

        public PengaturanIklanController (BNIContext context, IUriService uriService)
        {
            _context = context;
            this.uriService = uriService;
        }

        // GET: api/PengaturanIklan
        [HttpGet]
        public async Task<IActionResult> GetAll([FromQuery] PaginationFilter filter)
        {
            var route = Request.Path.Value;
            var validFilter = new PaginationFilter(filter.PageNumber, filter.PageSize);
            var pagedData = await _context.pengaturan_iklan
                //.Skip((validFilter.PageNumber - 1) * validFilter.PageSize)
                //.Take(validFilter.PageSize)
                .ToListAsync();
            var totalRecords = await _context.pengaturan_iklan.CountAsync();
            var pagedReponse = PaginationHelper.CreatePagedReponse<PengaturanIklan>(pagedData, validFilter, totalRecords, uriService, route);
            return Ok(pagedData);
        }

        // GET: api/PengaturanIklan/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetById(int id)
        {
            var hasil = await _context.pengaturan_iklan.Where(a => a.id == id).FirstOrDefaultAsync();
            return Ok(new Response<PengaturanIklan>(hasil));
        }

        // POST: api/PengaturanIklan
        [HttpPost]
        public async Task<ActionResult<PengaturanIklan>> PostPengaturanIklan(PengaturanIklan pengaturan_iklan)
        {
            _context.pengaturan_iklan.Add(pengaturan_iklan);
            await _context.SaveChangesAsync();

            var pagedData = _context.pengaturan_iklan;
            return Ok(pagedData);

        }

        // PUT: api/PengaturanIklan/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutPengaturanIklan(int id, PengaturanIklan pengaturan_iklan)
        {
            if (id != pengaturan_iklan.id)
            {
                return BadRequest();
            }

            _context.Entry(pengaturan_iklan).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
                var hasil = await _context.pengaturan_iklan.Where(a => a.id == id).FirstOrDefaultAsync();
                return Ok(new Response<PengaturanIklan>(hasil));
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!RoleExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }


        }

        private bool RoleExists(int id)
        {
            return _context.pengaturan_iklan.Any(e => e.id == id);
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeletePengaturanIklan(int id)
        {
            var pengaturan_iklan = await _context.pengaturan_iklan.FindAsync(id);
            if (pengaturan_iklan == null)
            {
                return NotFound();
            }

            _context.pengaturan_iklan.Remove(pengaturan_iklan);
            await _context.SaveChangesAsync();

            //return NoContent();

            var pagedData = _context.pengaturan_iklan;
            return Ok(pagedData);
        }
    }
}
