﻿using BNIMerchant.Auth;
using BNIMerchant.Filter;
using BNIMerchant.Helpers;
using BNIMerchant.Models;
using BNIMerchant.Models.MenuManagement;
using BNIMerchant.Services;
using BNIMerchant.Wrappers;
using ClosedXML.Excel;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Syncfusion.XlsIO;
using System;
using System.IO;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;

namespace BNIMerchant.Controllers.MenuManagement
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly BNIContext _context;
        private readonly IUriService uriService;
        private static IWebHostEnvironment _environment;
        private readonly IAuthenticateManager authenticateManager;

        public UserController(BNIContext context, IUriService uriService, IAuthenticateManager authenticateManager, IWebHostEnvironment environment)
        {
            _context = context;
            this.uriService = uriService;
            _environment = environment;
            this.authenticateManager = authenticateManager;
        }

        [HttpGet("select2-user")]
        public IActionResult Select2User()
        {
            var resultCount = 10;
            var offset = (Convert.ToInt32(Request.Query["page"].FirstOrDefault()) - 1) * resultCount;

            var users = _context.user.AsQueryable();

            var searchValue = Request.Query["search"].FirstOrDefault();

            if (!string.IsNullOrEmpty(searchValue))
            {
                users = users.Where(m => m.username.Contains(searchValue)
                                            || m.email.Contains(searchValue));
            }
            var option = users.Skip(offset).Take(resultCount).ToList();

            var count = users.Count();

            var endCount = offset + resultCount;
            var morePage = count > endCount;
            return Ok(new
            {
                results = option.Select(x => new { id = x.id_user, text = x.username }),
                pagination = new { more = morePage }
            });
        }

        [HttpPost("fetchData")]
        public IActionResult fetchData()
        {
            try
            {
                var draw = Request.Form["draw"].FirstOrDefault();
                var start = Request.Form["start"].FirstOrDefault();
                var length = Request.Form["length"].FirstOrDefault();
                var sortColumn = Request.Form["columns[" + Request.Form["order[0][column]"].FirstOrDefault() + "][name]"].FirstOrDefault();
                var sortColumnDirection = Request.Form["order[0][dir]"].FirstOrDefault();
                var searchValue = Request.Form["search[value]"].FirstOrDefault();
                int pageSize = length != null ? Convert.ToInt32(length) : 0;
                int skip = start != null ? Convert.ToInt32(start) : 0;
                int recordsTotal = 0;
                var result = _context.user
                .Join(
                    _context.role,
                    User => User.id_role,
                    Role => Role.id_role,
                    (User, Role) => new
                    {
                        id_user = User.id_user,
                        id_role = User.id_role,
                        username = User.username,
                        email = User.email,
                        role = Role.name,
                        isDeleted = User.isDeleted
                    }
                ).AsQueryable();
                if (!(string.IsNullOrEmpty(sortColumn) && string.IsNullOrEmpty(sortColumnDirection)))
                {
                    sortColumn = sortColumn + '_' + sortColumnDirection;
                    switch (sortColumn)
                    {
                        case "username_asc":
                            result = result.OrderBy(s => s.username);
                            break;
                        case "username_desc":
                            result = result.OrderByDescending(s => s.username);
                            break;
                        case "email_asc":
                            result = result.OrderBy(s => s.email);
                            break;
                        case "email_desc":
                            result = result.OrderByDescending(s => s.email);
                            break;
                        default:
                            result = result.OrderBy(s => s.username);
                            break;
                    }
                }
                if (!string.IsNullOrEmpty(searchValue))
                {
                    result = result.Where(m => m.username.Contains(searchValue)
                                                || m.email.Contains(searchValue));
                }
                recordsTotal = result.Count();
                var data = result.Skip(skip).Take(pageSize).ToList();
                var jsonData = new { draw = draw, recordsFiltered = recordsTotal, recordsTotal = recordsTotal, data = data };
                return Ok(jsonData);
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        [HttpGet]
        public async Task<IActionResult> GetAll([FromQuery] PaginationFilter filter, [FromHeader] string token)
        {
            var authorization = authenticateManager.isLoggedIn(token);
            if (authorization is false)
                return StatusCode(StatusCodes.Status401Unauthorized, new ResponseStatus { Status = "Error", Message = "You can't access this method !" });

            var route = Request.Path.Value;
            var validFilter = new PaginationFilter(filter.PageNumber, filter.PageSize);
            var pagedData = await _context.user
                .Skip((validFilter.PageNumber - 1) * validFilter.PageSize)
                .Take(validFilter.PageSize)
                .ToListAsync();
            var totalRecords = await _context.user.CountAsync();
            var pagedResponse = PaginationHelper.CreatePagedReponse<User>(pagedData, validFilter, totalRecords, uriService, route);
            return Ok(pagedResponse);
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetById(int id, [FromHeader] string token)
        {
            var authorization = authenticateManager.isLoggedIn(token);
            if (authorization is false)
                return StatusCode(StatusCodes.Status401Unauthorized, new ResponseStatus { Status = "Error", Message = "You can't access this method !" });

            var hasil = await _context.user.Where(a => a.id_user == id).FirstOrDefaultAsync();
            return Ok(new ResponseStatus { Status = "Success", Message = "Data has been retrieve", Data = hasil });
        }

        // POST: api/User
        [HttpPost]
        public async Task<ActionResult<User>> PostUser(User user, [FromHeader] string token)
        {
            var authorization = authenticateManager.isLoggedIn(token);
            if (authorization is false)
                return StatusCode(StatusCodes.Status401Unauthorized, new ResponseStatus { Status = "Error", Message = "You can't access this method !" });

            var validate = await _context.user.Where(x => x.username == user.username || x.email == user.email).FirstOrDefaultAsync();
            if (validate != null)
                return Ok(new ResponseStatus { Status = "Error", Message = "Username or Email already taken !" });
            var newToken = authenticateManager.CreateToken(user.username);
            User userWithToken = new User()
            {
                id_role = user.id_role,
                id_wilayah = user.id_wilayah,
                username = user.username,
                email = user.email,
                password = authenticateManager.Sha256Encrypt(user.password),
                session_login = null,
                status = 1,
                token = newToken,
                create_time = DateTime.Now,
                update_time = DateTime.Now
            };
            _context.user.Add(userWithToken);
            await _context.SaveChangesAsync();

            return Ok(new ResponseStatus { Status = "Success", Message = "User has been added", Data = userWithToken });
        }

        // PUT: api/User/5
        [HttpPut("{id}")]
        public async Task<ActionResult<User>> PutUser(int id, User user, [FromHeader] string token)
        {
            var authorization = authenticateManager.isLoggedIn(token);
            if (authorization is false)
                return StatusCode(StatusCodes.Status401Unauthorized, new ResponseStatus { Status = "Error", Message = "You can't access this method !" });

            try
            {
                var validate = await _context.user.Where(x => (x.username == user.username || x.email == user.email) && x.id_user != id).FirstOrDefaultAsync();
                if (validate != null)
                    return Ok(new ResponseStatus { Status = "Error", Message = "Username or Email already taken !" });
                var hasil = await _context.user.Where(a => a.id_user == id).FirstOrDefaultAsync();
                hasil.username = user.username;
                hasil.id_role = user.id_role;
                hasil.id_wilayah = user.id_wilayah;
                if(user.password != null)
                {
                    hasil.password = authenticateManager.Sha256Encrypt(user.password);
                }
                hasil.email = user.email;

                await _context.SaveChangesAsync();
                return Ok(new ResponseStatus { Status = "Success", Message = "User has been updated", Data = hasil });
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!_context.user.Any(e => e.id_user == id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }
        }

        // DELETE: api/User/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteUser(int id, [FromHeader] string token)
        {
            var authorization = authenticateManager.isLoggedIn(token);
            if (authorization is false)
                return StatusCode(StatusCodes.Status401Unauthorized, new ResponseStatus { Status = "Error", Message = "You can't access this method !" });

            var user = await _context.user.FindAsync(id);
            if (user == null)
            {
                return NotFound();
            }

            _context.user.Remove(user);
            _context.SaveChanges();

            var pagedData = _context.user;
            return Ok(new ResponseStatus { Status = "Success", Message = "User has been deleted", Data = pagedData });
        }
    }
}
