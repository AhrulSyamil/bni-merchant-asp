﻿using BNIMerchant.Models;
using BNIMerchant.Models.MenuManagement;
using BNIMerchant.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BNIMerchant.Controllers.MenuManagement
{
    [Route("api/[controller]")]
    [ApiController]
    public class ValuesController : ControllerBase
    {
        private readonly BNIContext _context;
        private readonly IUriService uriService;

        public ValuesController(BNIContext context, IUriService uriService)
        {
            _context = context;
            this.uriService = uriService;
        }

        [HttpPost("AddRoleList")]
        [ActionName("AddRoleList")]
        public async Task<ActionResult<JSONModel>> AddRoleList([FromBody] JSONModel[] roles)
        {
            foreach(JSONModel z in roles)
            {
                //_context.role.Add(z);
            }
            await _context.SaveChangesAsync();

            var pagedData = _context.role;

            return Ok(pagedData);

        }
    }
}
