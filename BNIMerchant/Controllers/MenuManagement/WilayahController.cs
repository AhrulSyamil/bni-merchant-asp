﻿using BNIMerchant.Auth;
using BNIMerchant.Filter;
using BNIMerchant.Helpers;
using BNIMerchant.Models;
using BNIMerchant.Models.MenuManagement;
using BNIMerchant.Services;
using BNIMerchant.Wrappers;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;

namespace BNIMerchant.Controllers.MenuManagement
{
    [Route("api/[controller]")]
    [ApiController]
    public class WilayahController : Controller
    {
        private readonly BNIContext _context;
        private readonly IUriService uriService;
        private readonly IAuthenticateManager authenticateManager;

        public WilayahController(BNIContext context, IUriService uriService, IAuthenticateManager authenticateManager)
        {
            _context = context;
            this.uriService = uriService;
            this.authenticateManager = authenticateManager;
        }

        [HttpGet("select2-wilayah")]
        public IActionResult Select2Wilayah()
        {
            var resultCount = 10;
            var offset = (Convert.ToInt32(Request.Query["page"].FirstOrDefault()) - 1) * resultCount;

            var roles = _context.Wilayah.AsQueryable();

            var searchValue = Request.Query["search"].FirstOrDefault();

            if (!string.IsNullOrEmpty(searchValue))
            {
                roles = roles.Where(m => m.name.Contains(searchValue)
                                            || m.description.Contains(searchValue));
            }
            var option = roles.Skip(offset).Take(resultCount).ToList();

            var count = roles.Count();

            var endCount = offset + resultCount;
            var morePage = count > endCount;
            return Ok(new
            {
                results = option.Select(x => new { id = x.id_wilayah, text = x.name }),
                pagination = new { more = morePage }
            });
        }

        [HttpPost("fetchData")]
        public IActionResult fetchData()
        {
            try
            {
                var draw = Request.Form["draw"].FirstOrDefault();
                var start = Request.Form["start"].FirstOrDefault();
                var length = Request.Form["length"].FirstOrDefault();
                var sortColumn = Request.Form["columns[" + Request.Form["order[0][column]"].FirstOrDefault() + "][name]"].FirstOrDefault();
                var sortColumnDirection = Request.Form["order[0][dir]"].FirstOrDefault();
                var searchValue = Request.Form["search[value]"].FirstOrDefault();
                int pageSize = length != null ? Convert.ToInt32(length) : 0;
                int skip = start != null ? Convert.ToInt32(start) : 0;
                int recordsTotal = 0;
                var result = _context.Wilayah.AsQueryable();
                if (!(string.IsNullOrEmpty(sortColumn) && string.IsNullOrEmpty(sortColumnDirection)))
                {
                    sortColumn = sortColumn + '_' + sortColumnDirection;
                    switch (sortColumn)
                    {
                        case "type_asc":
                            result = result.OrderBy(s => s.type);
                            break;
                        case "type_desc":
                            result = result.OrderByDescending(s => s.type);
                            break;
                        case "name_asc":
                            result = result.OrderBy(s => s.name);
                            break;
                        case "name_desc":
                            result = result.OrderByDescending(s => s.name);
                            break;
                        case "code_asc":
                            result = result.OrderBy(s => s.code);
                            break;
                        case "code_desc":
                            result = result.OrderByDescending(s => s.code);
                            break;
                        case "description_asc":
                            result = result.OrderBy(s => s.description);
                            break;
                        case "description_desc":
                            result = result.OrderByDescending(s => s.description);
                            break;
                        default:
                            result = result.OrderBy(s => s.name);
                            break;
                    }
                }
                if (!string.IsNullOrEmpty(searchValue))
                {
                    result = result.Where(m => m.type.Contains(searchValue)
                                            || m.name.Contains(searchValue)
                                            || m.description.Contains(searchValue));
                }
                recordsTotal = result.Count();
                var data = result.Skip(skip).Take(pageSize).ToList();
                var jsonData = new { draw = draw, recordsFiltered = recordsTotal, recordsTotal = recordsTotal, data = data };
                return Ok(jsonData);
            }
            catch (Exception)
            {
                throw;
            }
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetById(int id)
        {
            var hasil = await _context.Wilayah.Where(a => a.id_wilayah == id).FirstOrDefaultAsync();
            return Ok(new ResponseStatus { Status = "Success", Message = "Data has been retrieve", Data = hasil });
        }

        [HttpPost]
        public async Task<IActionResult> PostWilayah([FromHeader] string token, Wilayah wilayah)
        {
            var authorization = authenticateManager.isLoggedIn(token);
            if (authorization is false)
                return StatusCode(StatusCodes.Status401Unauthorized, new ResponseStatus { Status = "Error", Message = "You can't access this method !" });

            try
            {

                Wilayah data_wilayah = new Wilayah()
                {
                    type = wilayah.type,
                    name = wilayah.name,
                    code = wilayah.code,
                    description = wilayah.description,
                    status = 1,
                    create_time = DateTime.Now,
                    create_by = authorization.id_user,
                    update_time = DateTime.Now,
                    update_by = authorization.id_user,
                };

                _context.Wilayah.Add(data_wilayah);
                await _context.SaveChangesAsync();

                return Ok(new ResponseStatus { Status = "Success", Message = "Region has been added", Data = data_wilayah });
            }
            catch (Exception)
            {
                throw;
            }
        }

        

        // PUT: api/Wilayah/5
        [HttpPut("{id}")]
        public IActionResult PutWilayah(int id, Wilayah wilayah, [FromHeader] string token)
        {
            try
            {
                var TargetWilayah = _context.Wilayah.Where(x => x.id_wilayah == id).FirstOrDefault();

                TargetWilayah.type = wilayah.type;
                TargetWilayah.name = wilayah.name;
                TargetWilayah.code = wilayah.code;
                TargetWilayah.description = wilayah.description;
                TargetWilayah.status = 1;
                TargetWilayah.update_time = DateTime.Now;

                _context.SaveChanges();
                return Ok(new ResponseStatus { Status = "Success", Message = "Region has been updated", Data = TargetWilayah });
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!_context.Wilayah.Any(e => e.id_wilayah == id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }
        }

        // DELETE: api/Role/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteWilayah(int id, [FromHeader] string token)
        {
            
            var wilayah = await _context.Wilayah.FindAsync(id);
            if (wilayah == null)
            {
                return NotFound();
            }

            _context.Wilayah.Remove(wilayah);
            await _context.SaveChangesAsync();

            var pagedData = _context.Wilayah;
            return Ok(pagedData);
        }



    }
}
