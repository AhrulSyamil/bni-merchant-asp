﻿using BNIMerchant.Auth;
using BNIMerchant.Filter;
using BNIMerchant.Helpers;
using BNIMerchant.Models;
using BNIMerchant.Models.MenuManagement;
using BNIMerchant.Models.Menus;
using BNIMerchant.Services;
using BNIMerchant.Wrappers;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;

namespace BNIMerchant.Controllers.MenuManagement
{
    [Route("api/[controller]")]
    [ApiController]
    public class RoleController : Controller
    {
        private readonly BNIContext _context;
        private readonly IUriService uriService;
        private readonly IAuthenticateManager authenticateManager;

        public RoleController(BNIContext context, IUriService uriService, IAuthenticateManager authenticateManager)
        {
            _context = context;
            this.uriService = uriService;
            this.authenticateManager = authenticateManager;
        }

        public object CheckAuth(string token)
        {
            var authorization = authenticateManager.isLoggedIn(token);
            if (authorization is false)
                return StatusCode(StatusCodes.Status401Unauthorized, new ResponseStatus { Status = "Error", Message = "Unauthorized!" });
            return authorization;
        }

        [HttpGet("select2-role")]
        public IActionResult Select2Role()
        {
            var resultCount = 10;
            var offset = (Convert.ToInt32(Request.Query["page"].FirstOrDefault()) - 1) * resultCount;

            var roles = _context.role.AsQueryable();

            var searchValue = Request.Query["search"].FirstOrDefault();
            
            if (!string.IsNullOrEmpty(searchValue))
            {
                roles = roles.Where(m => m.name.Contains(searchValue)
                                            || m.description.Contains(searchValue));
            }
            var option = roles.Skip(offset).Take(resultCount).ToList();

            var count = roles.Count();

            var endCount = offset + resultCount;
            var morePage = count > endCount;
            return Ok(new
            {
                results = option.Select(x => new { id = x.id_role, text = x.name, type = x.type }),
                pagination = new { more = morePage }
            });
        }

        [HttpPost("fetchData")]
        public IActionResult fetchData([FromHeader] string token)
        {
            CheckAuth(token);

            try
            {
                var draw = Request.Form["draw"].FirstOrDefault();
                var start = Request.Form["start"].FirstOrDefault();
                var length = Request.Form["length"].FirstOrDefault();
                var sortColumn = Request.Form["columns[" + Request.Form["order[0][column]"].FirstOrDefault() + "][name]"].FirstOrDefault();
                var sortColumnDirection = Request.Form["order[0][dir]"].FirstOrDefault();
                var searchValue = Request.Form["search[value]"].FirstOrDefault();
                int pageSize = length != null ? Convert.ToInt32(length) : 0;
                int skip = start != null ? Convert.ToInt32(start) : 0;
                int recordsTotal = 0;
                var result = _context.role.AsQueryable();
                if (!(string.IsNullOrEmpty(sortColumn) && string.IsNullOrEmpty(sortColumnDirection)))
                {
                    sortColumn = sortColumn + '_' + sortColumnDirection;
                    switch (sortColumn)
                    {
                        case "name_asc":
                            result = result.OrderBy(s => s.name);
                            break;
                        case "name_desc":
                            result = result.OrderByDescending(s => s.name);
                            break;
                        case "description_asc":
                            result = result.OrderBy(s => s.description);
                            break;
                        case "description_desc":
                            result = result.OrderByDescending(s => s.description);
                            break;
                        default:
                            result = result.OrderBy(s => s.name);
                            break;
                    }
                }
                if (!string.IsNullOrEmpty(searchValue))
                {
                    result = result.Where(m => m.name.Contains(searchValue)
                                                || m.description.Contains(searchValue));
                }
                recordsTotal = result.Count();
                var data = result.Skip(skip).Take(pageSize).ToList();
                var jsonData = new { draw = draw, recordsFiltered = recordsTotal, recordsTotal = recordsTotal, data = data };
                return Ok(jsonData);
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        [HttpGet]
        public async Task<IActionResult> GetAll([FromQuery] PaginationFilter filter, [FromHeader] string token)
        {
            CheckAuth(token);

            var route = Request.Path.Value;
            var validFilter = new PaginationFilter(filter.PageNumber, filter.PageSize);
            var pagedData = await _context.role
                .Skip((validFilter.PageNumber - 1) * validFilter.PageSize)
                .Take(validFilter.PageSize)
                .ToListAsync();
            var totalRecords = await _context.user.CountAsync();
            var pagedResponse = PaginationHelper.CreatePagedReponse<Role>(pagedData, validFilter, totalRecords, uriService, route);
            return Ok(pagedResponse);
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetById(int id, [FromHeader] string token)
        {
            CheckAuth(token);

            var role = await _context.role.Where(a => a.id_role == id).FirstOrDefaultAsync();
            var menus = await _context.Menu_Role.Where(b => b.id_role == role.id_role).Select(x => x.id_menu).ToArrayAsync();
            return Ok(new ResponseStatus { Status = "Success", Message = "Data has been retrieve", Data = new { role = role, menus = menus } });
        }

        // POST: api/Role
        [HttpPost]
        public IActionResult PostRole(JsonElement data, [FromHeader] string token)
        {
            CheckAuth(token);

            var role = data.GetProperty("role");
            var id_menus = data.GetProperty("id_menu").EnumerateArray();

            Role dataRole = new Role()
            {
                name = role.GetProperty("name").ToString(),
                type = role.GetProperty("type").ToString(),
                description = role.GetProperty("description").ToString(),
                status = 1,
                create_time = DateTime.Now,
                update_time = DateTime.Now
            };
            _context.role.Add(dataRole);
            _context.SaveChanges();

            foreach (var id_menu in id_menus)
            {
                Menu_Role dataMenu = new Menu_Role()
                {
                    id_menu = id_menu.GetInt32(),
                    id_role = dataRole.id_role
                };

                _context.Menu_Role.Add(dataMenu);
                _context.SaveChanges();
            }

            return Ok(new ResponseStatus { Status = "Success", Message = "Role and Privilege has been inserted", Data = dataRole });
        }

        // PUT: api/Role/5
        [HttpPut("{id}")]
        public IActionResult PutRole(int id, JsonElement data, [FromHeader] string token)
        {
            CheckAuth(token);

            var authorization = authenticateManager.isLoggedIn(token); if (authorization is false)
                return StatusCode(StatusCodes.Status401Unauthorized, new ResponseStatus { Status = "Error", Message = "You can't access this method !" });

            var role = data.GetProperty("role");
            var id_menus = data.GetProperty("id_menu").EnumerateArray();

            try
            {
                var roleTarget = _context.role.Where(x => x.id_role == id).FirstOrDefault();
                var id_menu_targets = _context.Menu_Role.Where(x => x.id_role == roleTarget.id_role).ToList();

                roleTarget.name = role.GetProperty("name").ToString();
                roleTarget.type = role.GetProperty("type").ToString();
                roleTarget.description = role.GetProperty("description").ToString();
                roleTarget.status = 1;
                roleTarget.update_time = DateTime.Now;

                foreach (var id_menu_target in id_menu_targets)
                {
                    _context.Menu_Role.Remove(id_menu_target);
                    _context.SaveChanges();
                }

                foreach (var id_menu in id_menus)
                {
                    Menu_Role dataMenu = new Menu_Role()
                    {
                        id_menu = id_menu.GetInt32(),
                        id_role = roleTarget.id_role
                    };
                    _context.Menu_Role.Add(dataMenu);
                }

                _context.SaveChanges();
                return Ok(new ResponseStatus { Status = "Success", Message = "Role and privilege has been updated", Data = roleTarget });
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!_context.role.Any(e => e.id_role == id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }
        }

        // DELETE: api/Role/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteRole(int id, [FromHeader] string token)
        {
            CheckAuth(token);

            var role = await _context.role.FindAsync(id);
            if (role == null)
            {
                return NotFound();
            }

            _context.role.Remove(role);
            await _context.SaveChangesAsync();

            var pagedData = _context.role;
            return Ok(pagedData);
        }

        [HttpGet("ListMenu")]
        public async Task<IActionResult> ListMenu([FromHeader] string token)
        {
            CheckAuth(token);

            var listMenu = await _context.Menu.ToListAsync();

            return Ok(new ResponseStatus { Status = "Success", Message = "Menu has been retrieve", Data = listMenu });
        }

        [HttpGet("GetMenuRole")]
        public async Task<IActionResult> GetMenuRole([FromHeader] string token)
        {
            var authorization = authenticateManager.isLoggedIn(token);
            if (authorization is false)
                return StatusCode(StatusCodes.Status401Unauthorized, new ResponseStatus { Status = "Error", Message = "Unauthorized!" });
          
            int id_role = authorization.id_role;

            var menus = await (from role in _context.role
                         join menu_role in _context.Menu_Role on role.id_role equals menu_role.id_role
                         join menu in _context.Menu on menu_role.id_menu equals menu.id_menu
                         where menu_role.id_role == id_role
                         orderby menu.order ascending
                         select new
                         {
                             menu
                         }
                         ).AsQueryable().Select(x => x.menu).ToListAsync();

            return Ok(new ResponseStatus { Status = "Success", Message = "Menu has been retrieve", Data = menus });
        }

    }
}
