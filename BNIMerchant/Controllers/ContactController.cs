﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BNIMerchant.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace BNIMerchant.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ContactController 
    {
        public Contact[] Get()
        {
            return new Contact[]
            {
        new Contact
        {
            Id = 1,
            Name = "Glenn Block"
        },
        new Contact
        {
            Id = 2,
            Name = "Dan Roth"
        }
            };
        }
    }
}
