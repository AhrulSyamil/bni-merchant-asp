﻿using BNIMerchant.Filter;
using BNIMerchant.Helpers;
using BNIMerchant.Models;
using BNIMerchant.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading.Tasks;
using BNIMerchant.Models.RequestPemasanganSPK;
using BNIMerchant.Wrappers;
using Microsoft.AspNetCore.Hosting;
using System.IO;
using BNIMerchant.Models.Data;
using System.Collections.Generic;
using System.Text.Json;
using BNIMerchant.Auth;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System.Text.Json.Serialization;
using ClosedXML.Excel;
using BNIMerchant.Models.StatusPemasangan;
using System.Globalization;

namespace BNIMerchant.Controllers.RequestPemasanganSPK
{
    [Route("api/[controller]")]
    [ApiController]
    public class RequestPemasanganSPKController : Controller
    {
        private readonly BNIContext _context;
        private readonly IUriService uriService;
        private static IWebHostEnvironment _environment;
        private readonly IAuthenticateManager authenticateManager;

        public RequestPemasanganSPKController(BNIContext context, IUriService uriService, IWebHostEnvironment environment, IAuthenticateManager authenticateManager)
        {
            _context = context;
            this.uriService = uriService;
            _environment = environment;
            this.authenticateManager = authenticateManager;
        }

        public class VerifikasiCardLinkModel
        {
            public int id_MRF { get; set; }
            public List<int> id_CL { get; set; }
        }

        public class SettingProfilingModel
        {
            public List<Data_TAR> Data_TAR { get; set; }
            public List<Data_Mismer_Detail> Data_Mismer { get; set; }
        }

        public class RequestPemasanganModel
        {
            public IFormFile? files { get; set; }

            public string? cl { get; set; }
            public string? tar { get; set; }
            public string? mismer { get; set; }
        }

        [HttpPost("getFile")]
        public async Task<IActionResult> GetFile([FromBody] JsonElement request)
        {
            //var path = @"C:\DATA\Development\ASP\BNIMerchant\BNIMerchant\wwwroot\Upload\Data MRF\2021/03/01\1614589745_Tutorial Penggunaan FortiClient VPN 01.03pdf.pdf";
            var path = request.GetProperty("path").ToString();
            var memory = new MemoryStream();
            using (var stream = new FileStream(path, FileMode.Open))
            {
                await stream.CopyToAsync(memory);
            }
            memory.Position = 0;
            var ext = Path.GetExtension(path).ToLowerInvariant();
            return File(memory, GetMimeTypes()[ext], Path.GetFileName(path));
        }

        private Dictionary<string, string> GetMimeTypes()
        {
            return new Dictionary<string, string>
            {
                { ".txt", "text/plain"},
                { ".pdf", "application/pdf"},
                { ".doc", "application/vnd.ms-word"},
                { ".docx", "application/vnd.ms-word"},
                { ".xls", "application/vnd.ms-excel"},
                {".xlsx", "application/vnd.openxmlformatsofficedocument.spreadsheetml.sheet"},
                { ".png", "image/png"},
                { ".jpg", "image/jpeg"},
                { ".jpeg", "image/jpeg"},
                { ".gif", "image/gif"},
                { ".csv", "text/csv"}
            };
        }

        // Index

        [HttpPost("fetchData")]
        public IActionResult fetchData([FromHeader] string token)
        {
            var authorization = authenticateManager.isLoggedIn(token);
            if (authorization is false)
                return StatusCode(StatusCodes.Status401Unauthorized, new ResponseStatus { Status = "Error", Message = "You can't access this method !" });

            try
            {
                var draw = Request.Form["draw"].FirstOrDefault();
                var start = Request.Form["start"].FirstOrDefault();
                var length = Request.Form["length"].FirstOrDefault();
                var sortColumn = Request.Form["columns[" + Request.Form["order[0][column]"].FirstOrDefault() + "][name]"].FirstOrDefault();
                var sortColumnDirection = Request.Form["order[0][dir]"].FirstOrDefault();
                var searchValue = Request.Form["search[value]"].FirstOrDefault();
                int pageSize = length != null ? Convert.ToInt32(length) : 0;
                int skip = start != null ? Convert.ToInt32(start) : 0;
                int recordsTotal = 0;
                var result = (from header in _context.RequestPemasanganSPK_Header
                              join detail in _context.Detail_RequestPemasanganSPK on header.id_header equals detail.id_header_RPSPK
                              join user in _context.user on detail.create_by equals user.id_user
                              join role in _context.role on user.id_role equals role.id_role
                              join mrf in _context.Data_MRF on header.id_AttachMRF equals mrf.id_MRF
                              where header.staging == detail.staging
                              select new
                              {
                                  id_header = header.id_header,
                                  id_request = header.id_request,
                                  id_AttachMRF = header.id_AttachMRF,
                                  id_AttachCL = header.id_AttachCL,
                                  id_AttachProfiling = header.id_AttachMRF,
                                  id_AttachMismer = header.id_AttachMRF,
                                  id_AttachTar = header.id_AttachMRF,
                                  id_wilayah = user.id_wilayah,
                                  id_user = user.id_user,
                                  user_type = role.type,
                                  staging = header.staging,
                                  staging_name = (header.staging == 1) ? "Request Pemasangan" :
                                            (header.staging == 2) ? "Verifikasi Card Link" :
                                            (header.staging == 3) ? "Upload TAR & MISMER" :
                                            (header.staging == 4) ? "Input Onwer Code" :
                                            (header.staging == 5) ? "Setting Profiling" : "Generate New SPK",
                                  status_CLProfiling = header.status_CLProfiling,
                                  status_TIDComplete = header.status_TIDComplete,
                                  create_by = _context.user.Where(x => x.id_user == _context.Detail_RequestPemasanganSPK.Where(x => x.id_header_RPSPK == header.id_header && x.staging == 1).FirstOrDefault().create_by).FirstOrDefault().username,
                                  create_time = detail.create_time,
                                  create_date = detail.create_time.ToString("yyyy-MM-dd"),
                                  mrf_path = mrf.directory
                              }).AsQueryable();

                if (authorization.role.type == "Wilayah" || authorization.role.type == "Vendor")
                {
                    string kode_wilayah = authorization.wilayah.code;
                    result = result.Where(x => _context.Data_CL_Detail.Where(cl => cl.id_CL == x.id_AttachCL).Select(s => s.KODEWILAYAH).Contains(kode_wilayah));
                }

                var dateTimeNow = DateTime.Now;
                var firstDayOfMonth = new DateTime(dateTimeNow.Year, dateTimeNow.Month, 1);
                var lastDayOfMonth = firstDayOfMonth.AddMonths(1).AddDays(-1);

                string[] filterStatus = Request.Form["filters[status][]"].ToArray();
                string[] filterWilayah = Request.Form["filters[wilayah][]"].ToArray();
                string[] filterCreateBy = Request.Form["filters[create_by][]"].ToArray();

                if (Request.Form["filters[fromDate]"].FirstOrDefault() != "")
                {
                    if (Request.Form["filters[fromDate]"].FirstOrDefault() != "") result = result.Where(x => x.create_time >= Convert.ToDateTime(Request.Form["filters[fromDate]"].FirstOrDefault()) && x.create_time <= Convert.ToDateTime(Request.Form["filters[toDate]"].FirstOrDefault()));
                    if (filterWilayah.Length > 0) result = result.Where(x => filterWilayah.Any(item => item == x.id_wilayah.ToString()));
                    if (filterCreateBy.Length > 0) result = result.Where(x => filterCreateBy.Any(item => item == x.id_user.ToString()));
                    if (filterStatus.Length > 0) result = result.Where(x => filterStatus.Any(item => item == x.staging_name));
                }
                else
                {
                    result = result.Where(x => x.create_time >= firstDayOfMonth && x.create_time <= lastDayOfMonth);
                }

                if (!(string.IsNullOrEmpty(sortColumn) && string.IsNullOrEmpty(sortColumnDirection)))
                {
                    sortColumn = sortColumn + '_' + sortColumnDirection;
                    switch (sortColumn)
                    {
                        case "id_request_asc":
                            result = result.OrderBy(s => s.id_request);
                            break;
                        case "id_request_desc":
                            result = result.OrderByDescending(s => s.id_request);
                            break;
                        case "create_by_asc":
                            result = result.OrderBy(s => s.create_by);
                            break;
                        case "create_by_desc":
                            result = result.OrderByDescending(s => s.create_by);
                            break;
                        case "staging_asc":
                            result = result.OrderBy(s => s.staging_name);
                            break;
                        case "staging_desc":
                            result = result.OrderByDescending(s => s.staging_name);
                            break;
                        case "create_time_asc":
                            result = result.OrderBy(s => s.create_time);
                            break;
                        case "create_time_desc":
                            result = result.OrderByDescending(s => s.create_time);
                            break;
                    }
                }
                if (!string.IsNullOrEmpty(searchValue))
                {
                    result = result.Where(m => m.id_request.Contains(searchValue) || m.create_by.Contains(searchValue) || m.staging_name.Contains(searchValue));
                }
                recordsTotal = result.Count();
                var data = result.Skip(skip).Take(pageSize).ToList();
                var jsonData = new { draw = draw, recordsFiltered = recordsTotal, recordsTotal = recordsTotal, data = data };
                return Ok(jsonData);
            }
            catch(Exception ex)
            {
                return Ok(ex);
            }
        }

        // Request Pemasangan (workflow tahap 1)
        public dynamic uploadMRF(IFormFile objFile, int create_by)
        {
            var dateAndTime = DateTime.Now;
            var date = dateAndTime.ToString("yyyy/MM/dd");

            if (objFile.Length > 0)
            {
                try
                {
                    if (!Directory.Exists(_environment.WebRootPath + "\\Upload\\Data MRF\\" + date))
                    {
                        Directory.CreateDirectory(_environment.WebRootPath + "\\Upload\\Data MRF\\" + date);
                    }
                    using (FileStream fileStream = System.IO.File.Create(_environment.WebRootPath + "\\Upload\\Data MRF\\" + date + "\\" + (Int32)(DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1))).TotalSeconds + '_' + objFile.FileName))
                    {
                        objFile.CopyTo(fileStream);
                        fileStream.Flush();

                        var directory = (_environment.WebRootPath + "\\Upload\\Data MRF\\" + date + "\\" + (Int32)(DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1))).TotalSeconds + '_' + objFile.FileName);
                        var name_file = (Int32)(DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1))).TotalSeconds + "_" + objFile.FileName;

                        Data_MRF data_MRF = new Data_MRF()
                        {
                            name_file = name_file,
                            create_by = create_by,
                            directory = directory
                        };
                        _context.Data_MRF.Add(data_MRF);
                        _context.SaveChanges();

                        return new ResponseStatus { Status = "Success", Message = "File has been uploaded", Data = data_MRF };
                    }

                }
                catch (Exception ex)
                {
                    throw;
                }
            }
            else
            {
                return new ResponseStatus { Status = "Error", Message = "File hasn't uploaded" };
            }
        }

        [HttpPost("uploadCL")]
        public dynamic uploadCL(List<Data_CL_Detail> Data_Detail, int create_by)
        {
            Data_CL Data_CL = new Data_CL()
            {
                create_by = create_by
            };
            _context.Data_CL.Add(Data_CL);
            _context.SaveChanges();

            foreach (var cl in Data_Detail)
            {
                Data_CL_Detail Data_CL_Detail = new Data_CL_Detail()
                {
                    id_CL = Data_CL.id_CL,
                    ORG = cl.ORG,
                    KATEGORI = cl.KATEGORI,
                    KODEWILAYAH = cl.KODEWILAYAH,
                    MID = cl.MID,
                    KODECBG = cl.KODECBG,
                    MERCHDBANAME = cl.MERCHDBANAME,
                    MERCHDBACITY = cl.MERCHDBACITY,
                    CONTACT = cl.CONTACT,
                    MEMO = cl.MEMO,
                    PHONE = cl.PHONE,
                    MSO = cl.MSO,
                    MMO = cl.MMO,
                    MERCHANTTYPE = cl.MERCHANTTYPE,
                    SOURCECODE = cl.SOURCECODE,
                    MERCHANTNAME = cl.MERCHANTNAME,
                    ADDRESS1 = cl.ADDRESS1,
                    ADDRESS2 = cl.ADDRESS2,
                    ADDRESS3 = cl.ADDRESS3,
                    ADDRESS4 = cl.ADDRESS4,
                    ZIP = cl.ZIP,
                    MCC = cl.MCC,
                    POS = cl.POS,
                    PLAN1 = cl.PLAN1,
                    PLAN2 = cl.PLAN2,
                    PLAN3 = cl.PLAN3,
                    INSTLSPL1 = cl.INSTLSPL1,
                    INSTLSPL2 = cl.INSTLSPL2,
                    AGENTBANK = cl.AGENTBANK,
                    BRANCH = cl.BRANCH,
                    FLAGMERCHANT = cl.FLAGMERCHANT,
                    NAMANASABAH = cl.NAMANASABAH,
                    NOMORREKENING = cl.NOMORREKENING,
                    NPWP = cl.NPWP,
                    NAMABANK = cl.NAMABANK,
                    MAILORDERIND = cl.MAILORDERIND,
                    VISABNI = cl.VISABNI,
                    VISAMCJCB = cl.VISAMCJCB,
                    MCBNI = cl.MCBNI,
                    MCDEBITBNI = cl.MCDEBITBNI,
                    LOCALDEBIT = cl.LOCALDEBIT,
                    JCBBNI = cl.JCBBNI,
                    RESERVED = cl.RESERVED,
                    PRIVATELABEL = cl.PRIVATELABEL,
                    BNIPREPAID = cl.BNIPREPAID,
                    WHITELIST = cl.WHITELIST,
                    BNMS = cl.BNMS,
                    create_by = create_by
                };
                _context.Data_CL_Detail.Add(Data_CL_Detail);
            }

            var data = _context.SaveChanges();

            return new ResponseStatus { Status = "Success", Message = "Data Card Link has been inserted", Data = Data_CL };
        }

        [HttpPost("RequestPemasangan")]
        public async Task<IActionResult> RequestPemasangan([FromForm] RequestPemasanganModel request, [FromHeader] string token)
        {
            var authorization = authenticateManager.isLoggedIn(token);
            if (authorization is false)
                return StatusCode(StatusCodes.Status401Unauthorized, new ResponseStatus { Status = "Error", Message = "You can't access this method !" });

            try
            {

                dynamic mrf = uploadMRF(request.files, authorization.id_user);

                List<Data_CL_Detail> data_cl_Detail = JsonConvert.DeserializeObject<List<Data_CL_Detail>>(request.cl);

                dynamic cl = uploadCL(data_cl_Detail, authorization.id_user);

                var datestring = $"{DateTime.Now.Year.ToString()}{DateTime.Now.ToString("MM")}";

                var check_id = _context.RequestPemasanganSPK_Header.Where(x => x.id_request.Substring(0, 6) == datestring).OrderByDescending(x => x.id_request).FirstOrDefault();
                var uniqid = "";
                if (check_id is null)
                {
                    uniqid = datestring + "00001";
                }
                else
                {
                    Int64 id_request = Int64.Parse(check_id.id_request) + 1;
                    uniqid = id_request.ToString();
                }

                RequestPemasanganSPK_Header RPSPK = new RequestPemasanganSPK_Header()
                {
                    staging = 1,
                    status = 1,
                    id_AttachMRF = mrf.Data.id_MRF,
                    id_AttachCL = cl.Data.id_CL,
                    id_request = uniqid
                };
                _context.RequestPemasanganSPK_Header.Add(RPSPK);
                await _context.SaveChangesAsync();

                DetailRequestPemasanganSPK DRPSPK = new DetailRequestPemasanganSPK()
                {
                    id_header_RPSPK = RPSPK.id_header,
                    staging = 1,
                    create_by = authorization.id_user,
                    create_time = DateTime.Now
                };
                _context.Detail_RequestPemasanganSPK.Add(DRPSPK);
                await _context.SaveChangesAsync();

                return Ok(new ResponseStatus { Status = "Success", Message = "Request Pemasangan has been submitted", Data = new { RPSPK = RPSPK, DRPSPK = DRPSPK } });
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        [HttpGet("RequestPemasangan/{id}")]
        public async Task<IActionResult> getRequestPemasangan([FromHeader] string token, int id)
        {
            var authorization = authenticateManager.isLoggedIn(token);
            if (authorization is false)
                return StatusCode(StatusCodes.Status401Unauthorized, new ResponseStatus { Status = "Error", Message = "You can't access this method !" });

            var MRF = (from header in _context.RequestPemasanganSPK_Header
                             join mrf in _context.Data_MRF on header.id_AttachMRF equals mrf.id_MRF
                             where header.id_header == id
                             select new
                             {
                                 mrf
                             }
                            ).AsQueryable().FirstOrDefault();

            var CL = (from header in _context.RequestPemasanganSPK_Header
                       join cl in _context.Data_CL on header.id_AttachCL equals cl.id_CL
                       join cl_detail in _context.Data_CL_Detail on cl.id_CL equals cl_detail.id_CL
                       where header.id_header == id
                           select new
                       {
                           cl_detail
                       }
                       ).AsQueryable();

            if (authorization.role.type == "Wilayah" || authorization.role.type == "Vendor")
            {
                string kode_wilayah = authorization.wilayah.code;
                CL = CL.Where(x => x.cl_detail.KODEWILAYAH == kode_wilayah);
            }

            var result_cl = CL.ToList();

            return Ok(new ResponseStatus { Status = "Success", Message = "Data Request Pemasangan has been retrieve", Data = new { mrf = MRF.mrf , cl = result_cl } });
        }

        [HttpGet("VerifikasiCardLink/MRF/{id}")]
        public IActionResult VerifikasiCardLinkMRF(int id)
        {
            try
            {
                var mrf = (from header in _context.RequestPemasanganSPK_Header
                           join mrf_header in _context.Data_MRF on header.id_AttachMRF equals mrf_header.id_MRF
                           where header.id_header == id
                           select new
                           {
                               id_header = header.id_header,
                               id_mrf = mrf_header.id_MRF,
                               name_mrf = mrf_header.name_file,
                               approved_mrf = mrf_header.isApproved,
                               create_by_mrf = mrf_header.create_by,
                               create_time_mrf = mrf_header.create_time,
                               directory_mrf = mrf_header.directory,
                           }).AsQueryable().FirstOrDefault();
                return Ok(new ResponseStatus { Status = "Success", Message = "Request Verifikasi CardLink has been retrieve", Data = mrf });
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        [HttpPost("VerifikasiCardLink/fetchData")]
        public IActionResult VerifikasiCardLinkCL([FromHeader] string token)
        {

            var authorization = authenticateManager.isLoggedIn(token);
            if (authorization is false)
                return StatusCode(StatusCodes.Status401Unauthorized, new ResponseStatus { Status = "Error", Message = "You can't access this method !" });

            var draw = Request.Form["draw"].FirstOrDefault();
            var start = Request.Form["start"].FirstOrDefault();
            var length = Request.Form["length"].FirstOrDefault();
            var sortColumn = Request.Form["columns[" + Request.Form["order[0][column]"].FirstOrDefault() + "][name]"].FirstOrDefault();
            var sortColumnDirection = Request.Form["order[0][dir]"].FirstOrDefault();
            var searchValue = Request.Form["search[value]"].FirstOrDefault();
            int pageSize = length != null ? Convert.ToInt32(length) : 0;
            int skip = start != null ? Convert.ToInt32(start) : 0;
            int recordsTotal = 0;
            var result = (from header in _context.RequestPemasanganSPK_Header
                          join cl_header in _context.Data_CL on header.id_AttachCL equals cl_header.id_CL
                          join cl_detail in _context.Data_CL_Detail on cl_header.id_CL equals cl_detail.id_CL
                          where header.id_header == int.Parse(Request.Form["id"].FirstOrDefault())
                          select new
                          {
                              id_header = header.id_header,
                              id_AttachMRF = header.id_AttachMRF,
                              id_AttachCL = header.id_AttachMRF,
                              id_AttachProfiling = header.id_AttachMRF,
                              id_AttachMismer = header.id_AttachMRF,
                              id_AttachTar = header.id_AttachMRF,
                              id_detail_cl = cl_detail.id_CL_Detail,
                              kode_wilayah = cl_detail.KODEWILAYAH,
                              mid = cl_detail.MID,
                              merchant = cl_detail.MERCHANTNAME,
                              address = cl_detail.ADDRESS1,
                              npwp = cl_detail.NPWP,
                              bank = cl_detail.NAMABANK,
                              is_approved = cl_detail.isApproved
                          }).AsQueryable();

            if (authorization.role.type == "Wilayah" || authorization.role.type == "Vendor")
            {
                string kode_wilayah = authorization.wilayah.code;
                result = result.Where(x => x.kode_wilayah == kode_wilayah);
            }

            if (!(string.IsNullOrEmpty(sortColumn) && string.IsNullOrEmpty(sortColumnDirection)))
            {
                sortColumn = sortColumn + '_' + sortColumnDirection;
                switch (sortColumn)
                {
                    case "mid_asc":
                        result = result.OrderBy(s => s.mid);
                        break;
                    case "mid_desc":
                        result = result.OrderByDescending(s => s.mid);
                        break;
                    case "merchant_asc":
                        result = result.OrderBy(s => s.merchant);
                        break;
                    case "merchant_desc":
                        result = result.OrderByDescending(s => s.merchant);
                        break;
                    case "address_asc":
                        result = result.OrderBy(s => s.address);
                        break;
                    case "address_desc":
                        result = result.OrderByDescending(s => s.address);
                        break;
                    case "npwp_asc":
                        result = result.OrderBy(s => s.npwp);
                        break;
                    case "npwp_desc":
                        result = result.OrderByDescending(s => s.npwp);
                        break;
                }
            }
            if (!string.IsNullOrEmpty(searchValue))
            {
                result = result.Where(m => m.mid.Contains(searchValue)
                                            || m.merchant.Contains(searchValue)
                                            || m.address.Contains(searchValue)
                                            || m.npwp.Contains(searchValue));
            }
            recordsTotal = result.Count();
            var data = result.Skip(skip).Take(pageSize).ToList();
            var jsonData = new { draw = draw, recordsFiltered = recordsTotal, recordsTotal = recordsTotal, data = data };
            return Ok(jsonData);
        }

        [HttpPut("VerifikasiCardLink/UpdateCL/{id}")]
        public IActionResult UpdateCL(int id, Data_CL_Detail request_cl)
        {
            try
            {
                var cl = _context.Data_CL_Detail.Where(x => x.id_CL_Detail == id).FirstOrDefault();
                cl.MID = request_cl.MID;

                _context.SaveChanges();
                return Ok(new ResponseStatus { Status = "Success", Message = "MID has been updated", Data = cl });
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!_context.Data_CL_Detail.Any(e => e.id_CL_Detail == id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }
        }

        [HttpPut("VerifikasiCardLink/RejectCL/{id}")]
        public IActionResult RejectCL(int id)
        {
            try
            {
                var cl = _context.Data_CL_Detail.Where(x => x.id_CL_Detail == id).FirstOrDefault();
                cl.isApproved = 2;

                _context.SaveChanges();
                return Ok(new ResponseStatus { Status = "Success", Message = "CardLink has been rejected", Data = cl });
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!_context.Data_CL_Detail.Any(e => e.id_CL_Detail == id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }
        }

        [HttpPut("VerifikasiCardLink/ApproveCL/{id}")]
        public IActionResult ApproveCL(int id)
        {
            try
            {
                var cl = _context.Data_CL_Detail.Where(x => x.id_CL_Detail == id).FirstOrDefault();
                cl.isApproved = 1;

                _context.SaveChanges();
                return Ok(new ResponseStatus { Status = "Success", Message = "CardLink has been approved", Data = cl });
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!_context.Data_CL_Detail.Any(e => e.id_CL_Detail == id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }
        }

        [HttpPut("SubmitVerifikasiCardLink/{id}")]
        public async Task<IActionResult> SubmitVerifikasiCardLink([FromHeader] string token, int id)
        {
            var authorization = authenticateManager.isLoggedIn(token);
            if (authorization is false)
                return StatusCode(StatusCodes.Status401Unauthorized, new ResponseStatus { Status = "Error", Message = "You can't access this method !" });

            try
            {

                var rpspk = _context.RequestPemasanganSPK_Header.Where(x => x.id_header == id).FirstOrDefault();
                rpspk.staging = 2;

                _context.SaveChanges();

                var mrf = _context.Data_MRF.Where(x => x.id_MRF == rpspk.id_AttachMRF).FirstOrDefault();
                mrf.isApproved = 1;
                mrf.approved_by = authorization.id_user;

                _context.SaveChanges();

                var cl = _context.Data_CL.Where(x => x.id_CL == rpspk.id_AttachCL).FirstOrDefault();
                cl.isApproved = 1;
                cl.approved_by = authorization.id_user;

                _context.SaveChanges();

                DetailRequestPemasanganSPK drpspk = new DetailRequestPemasanganSPK()
                {
                    id_header_RPSPK = rpspk.id_header,
                    staging = 2,
                    create_by = authorization.id_user,
                };

                _context.Detail_RequestPemasanganSPK.Add(drpspk);
                await _context.SaveChangesAsync();

                return Ok(new ResponseStatus { Status = "Success", Message = "Verification Card Link has been submitted", Data = new { RPSPK = rpspk, DRPSPK = drpspk } });
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        //[HttpGet("downloadTAR/{id}")]
        //public IActionResult downloadTAR(int id)
        //{
        //    using (var workbook = new XLWorkbook())
        //    {
        //        var worksheet = workbook.Worksheets.Add("Sheet1");
        //        var currentRow = 1;
        //        worksheet.Cell(currentRow, 1).Value = "MID";
        //        worksheet.Cell(currentRow, 2).Value = "TID";
        //        var cl = (from rpspk in _context.RequestPemasanganSPK_Header
        //                  join cl_header in _context.Data_CL on rpspk.id_AttachCL equals cl_header.id_CL
        //                  join cl_detail in _context.Data_CL_Detail on cl_header.id_CL equals cl_detail.id_CL
        //                  where cl_detail.isApproved == 1 && rpspk.id_header == id
        //                  select new
        //                  {
        //                      cl_detail.MID
        //                  }).ToList();
        //        foreach (var value in cl)
        //        {
        //            currentRow++;
        //            worksheet.Cell(currentRow, 1).Value = value.MID.ToString();
        //            worksheet.Cell(currentRow, 2).Value = "";
        //        }

        //        using (var stream = new MemoryStream())
        //        {
        //            workbook.SaveAs(stream);
        //            var content = stream.ToArray();

        //            return File(
        //                content,
        //                "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
        //                "TAR.xlsx");
        //        }
        //    }
        //}

        [HttpGet("downloadTAR/{id}")]
        public IActionResult downloadTAR(int id)
        {
            string basePath = _environment.WebRootPath + @"\Template\TAR.xlsx";
            using (var workbook = new XLWorkbook(basePath))
            {
                var worksheet = workbook.Worksheets.First();
                var currentRow = 1;

                var data = (from rpspk in _context.RequestPemasanganSPK_Header
                          join cl_header in _context.Data_CL on rpspk.id_AttachCL equals cl_header.id_CL
                          join cl_detail in _context.Data_CL_Detail on cl_header.id_CL equals cl_detail.id_CL
                          where cl_detail.isApproved == 1 && rpspk.id_header == id
                          select new
                          {
                              cl_detail.MID
                          }).ToList();

                foreach (var value in data)
                {
                    currentRow++;
                    worksheet.Cell(currentRow, 1).Value = "'" + value.MID.ToString();
                }

                using (var stream = new MemoryStream())
                {
                    workbook.SaveAs(stream);
                    var content = stream.ToArray();

                    return File(
                        content,
                        "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
                        "TAR.xlsx");
                }
            }
        }

        [HttpGet("downloadMismer/{id}")]
        public IActionResult downloadMismer(int id)
        {
            string basePath = _environment.WebRootPath + @"\Template\MISMER.xlsx";
            using (var workbook = new XLWorkbook(basePath))
            {
                var worksheet = workbook.Worksheets.First();
                var currentRow = 2;

                var firstRowUsed = worksheet.FirstRowUsed();
                var firstPossibleAddress = worksheet.Row(firstRowUsed.RowNumber()).FirstCell().Address;
                var lastPossibleAddress = worksheet.LastCellUsed().Address;

                var range = worksheet.Range(firstPossibleAddress, lastPossibleAddress).ColumnCount();

                var data = (from rpspk in _context.RequestPemasanganSPK_Header
                            join cl_header in _context.Data_CL on rpspk.id_AttachCL equals cl_header.id_CL
                            join cl_detail in _context.Data_CL_Detail on cl_header.id_CL equals cl_detail.id_CL
                            where cl_detail.isApproved == 1 && rpspk.id_header == id
                            select new
                            {
                                cl_detail.MID,
                                cl_detail.MERCHDBANAME,
                                cl_detail.MERCHDBACITY,
                                cl_detail.CONTACT,
                                cl_detail.PHONE,
                                cl_detail.MSO,
                                cl_detail.MMO,
                                cl_detail.SOURCECODE,
                                cl_detail.MERCHANTNAME,
                                cl_detail.ADDRESS1,
                                cl_detail.ADDRESS2,
                                cl_detail.ADDRESS3,
                                cl_detail.ADDRESS4,
                                cl_detail.ZIP,
                                cl_detail.MCC
                            }).ToList();

                foreach (var item in data)
                {
                    for (int i = 1; i <= range; i++)
                    {
                        if (i == 4)
                        {
                            worksheet.Cell(currentRow, i).Value = "'" + item.MID;
                        }
                        else if(i == 5)
                        {
                            worksheet.Cell(currentRow, i).Value = item.MERCHDBANAME;
                        }
                        else if (i == 6)
                        {
                            worksheet.Cell(currentRow, i).Value = item.MERCHDBACITY;
                        }
                        else if (i == 7)
                        {
                            worksheet.Cell(currentRow, i).Value = item.CONTACT;
                        }
                        else if (i == 9)
                        {
                            worksheet.Cell(currentRow, i).Value = item.PHONE;
                        }
                        else if (i == 10)
                        {
                            worksheet.Cell(currentRow, i).Value = item.MSO;
                        }
                        else if (i == 11)
                        {
                            worksheet.Cell(currentRow, i).Value = item.MMO;
                        }
                        else if (i == 12)
                        {
                            worksheet.Cell(currentRow, i).Value = item.SOURCECODE;
                        }
                        else if (i == 13)
                        {
                            worksheet.Cell(currentRow, i).Value = item.MERCHANTNAME;
                        }
                        else if (i == 14)
                        {
                            worksheet.Cell(currentRow, i).Value = item.ADDRESS1;
                        }
                        else if (i == 15)
                        {
                            worksheet.Cell(currentRow, i).Value = item.ADDRESS2;
                        }
                        else if (i == 16)
                        {
                            worksheet.Cell(currentRow, i).Value = item.ADDRESS3;
                        }
                        else if (i == 17)
                        {
                            worksheet.Cell(currentRow, i).Value = item.ADDRESS4;
                        }
                        else if (i == 18)
                        {
                            worksheet.Cell(currentRow, i).Value = "'" + item.ZIP.ToString();
                        }
                        else if (i == 19)
                        {
                            worksheet.Cell(currentRow, i).Value = "'" + item.MCC.ToString();
                        }
                        else
                        {
                            worksheet.Cell(currentRow, i).Value = "";
                        }
                    }
                    currentRow++;
                }

                using (var stream = new MemoryStream())
                {
                    workbook.SaveAs(stream);
                    var content = stream.ToArray();

                    return File(
                        content,
                        "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
                        "MISMER.xlsx");
                }
            }
        }

        [HttpGet("approvedCardLink/{id}")]
        public IActionResult approvedCardLink(int id)
        {
            var data = (from rpspk in _context.RequestPemasanganSPK_Header
                        join cl_header in _context.Data_CL on rpspk.id_AttachCL equals cl_header.id_CL
                        join cl_detail in _context.Data_CL_Detail on cl_header.id_CL equals cl_detail.id_CL
                        where cl_detail.isApproved == 1 && rpspk.id_header == id
                        select new
                        {
                            cl_detail.MID
                        }).AsQueryable().Select(x => x.MID).ToArray();

            return Ok(new ResponseStatus { Status = "Success", Message = "Approved card link data has been retrieved", Data = data });
        }

        [HttpPost("uploadTAR")]
        public dynamic uploadTAR(List<Data_TAR_Detail> Data_Detail, int create_by)
        {
            Data_TAR Data_TAR = new Data_TAR()
            {
                create_by = create_by
            };
            _context.Data_TAR.Add(Data_TAR);
            _context.SaveChanges();

            foreach (var tar in Data_Detail)
            {
                Data_TAR_Detail Data_TAR_Detail = new Data_TAR_Detail()
                {
                    id_TAR = Data_TAR.id_TAR,
                    MID = tar.MID,
                    TID = tar.TID,

                    create_by = create_by
                };
                _context.Data_TAR_Detail.Add(Data_TAR_Detail);
            }

            var data = _context.SaveChanges();

            return new ResponseStatus { Status = "Success", Message = "Data TAR has been inserted", Data = Data_TAR };
        }

        [HttpPost("uploadMismer")]
        public dynamic uploadMismer(List<Data_Mismer_Detail> Data_Detail, int create_by)
        {
            Data_Mismer Data_Mismer = new Data_Mismer()
            {
                create_by = create_by
            };
            _context.Data_Mismer.Add(Data_Mismer);
            _context.SaveChanges();

            foreach (var mismer in Data_Detail)
            {
                Data_Mismer_Detail Data_Mismer_Detail = new Data_Mismer_Detail()
                {
                    id_Mismer = Data_Mismer.id_Mismer,
                    SOURCEDATA = mismer.SOURCEDATA,
                    WILAYAH = mismer.WILAYAH,
                    KATEGORI_MERCHANT = mismer.KATEGORI_MERCHANT,
                    MID = mismer.MID,
                    DBANAME = mismer.DBANAME,
                    DBACITY = mismer.DBACITY,
                    CONTACT = mismer.CONTACT,
                    MSO = mismer.MSO,
                    MMO = mismer.MMO,
                    SOURCECODE = mismer.SOURCECODE,
                    MERCHANTNAME = mismer.MERCHANTNAME,
                    ADDRESS = mismer.ADDRESS,
                    KODEPOS = mismer.KODEPOS,
                    MCC = mismer.MCC,
                    JUMLAHEDC = mismer.JUMLAHEDC,
                    OWNER = mismer.OWNER,

                    create_by = create_by
                };
                _context.Data_Mismer_Detail.Add(Data_Mismer_Detail);    
            }

            var data = _context.SaveChanges();

            return new ResponseStatus { Status = "Success", Message = "Data Mismer has been inserted", Data = Data_Mismer };
        }

        [HttpGet("TARMISMER/{id}")]
        public async Task<IActionResult> getTARMISMER([FromHeader] string token, int id)
        {
            var authorization = authenticateManager.isLoggedIn(token);
            if (authorization is false)
                return StatusCode(StatusCodes.Status401Unauthorized, new ResponseStatus { Status = "Error", Message = "You can't access this method !" });

            var TAR = (from header in _context.RequestPemasanganSPK_Header
                      join tar in _context.Data_TAR on header.Id_AttachTAR equals tar.id_TAR
                      join tar_detail in _context.Data_TAR_Detail on tar.id_TAR equals tar_detail.id_TAR
                      join cl_detail in _context.Data_CL_Detail on tar_detail.MID equals cl_detail.MID
                      where header.id_header == id
                      select new
                      {
                          cl_detail,
                          tar_detail
                      }
                       ).AsQueryable();

            var MISMER = (from header in _context.RequestPemasanganSPK_Header
                       join mismer in _context.Data_Mismer on header.Id_AttachMismer equals mismer.id_Mismer
                       join mismer_detail in _context.Data_Mismer_Detail on mismer.id_Mismer equals mismer_detail.id_Mismer
                       join cl_detail in _context.Data_CL_Detail on mismer_detail.MID equals cl_detail.MID
                       where header.id_header == id
                       select new
                       {
                           cl_detail,
                           mismer_detail
                       }
                       ).AsQueryable();

            if (authorization.role.type == "Wilayah" || authorization.role.type == "Vendor")
            {
                string kode_wilayah = authorization.wilayah.code;
                TAR = TAR.Where(x => x.cl_detail.KODEWILAYAH == kode_wilayah);
                MISMER = MISMER.Where(x => x.cl_detail.KODEWILAYAH == kode_wilayah);
            }

            var result_tar = TAR.Select(s => s.tar_detail).ToList();
            var result_mismer = MISMER.Select(s => s.mismer_detail).ToList();

            return Ok(new ResponseStatus { Status = "Success", Message = "Data tar mismer has been retrieve", Data = new { tar = TAR, mismer = MISMER } });
        }

        [HttpPut("SubmitTarMismer/{id}")]
        public async Task<IActionResult> SubmitTarMismer([FromBody] JsonElement request, [FromHeader] string token, int id)
        {
            var authorization = authenticateManager.isLoggedIn(token);
            if (authorization is false)
                return StatusCode(StatusCodes.Status401Unauthorized, new ResponseStatus { Status = "Error", Message = "You can't access this method !" });
            try
            {
                List<Data_TAR_Detail> Data_TAR_Detail = JsonConvert.DeserializeObject<List<Data_TAR_Detail>>(request.GetProperty("tar").ToString());
                dynamic tar = uploadTAR(Data_TAR_Detail, authorization.id_user);

                List<Data_Mismer_Detail> Data_Mismer_Detail = JsonConvert.DeserializeObject<List<Data_Mismer_Detail>>(request.GetProperty("mismer").ToString());
                dynamic mismer = uploadMismer(Data_Mismer_Detail, authorization.id_user);

                var RPSPK = _context.RequestPemasanganSPK_Header.Where(x => x.id_header == id).FirstOrDefault();
                RPSPK.staging = 3;
                RPSPK.Id_AttachTAR = tar.Data.id_TAR;
                RPSPK.Id_AttachMismer = mismer.Data.id_Mismer;

                DetailRequestPemasanganSPK DRPSPK = new DetailRequestPemasanganSPK()
                {
                    id_header_RPSPK = id,
                    staging = 3,
                    create_by = authorization.id_user,
                    create_time = DateTime.Now
                };
                _context.Detail_RequestPemasanganSPK.Add(DRPSPK);

                var cl = (from rpspk in _context.RequestPemasanganSPK_Header
                        join cl_header in _context.Data_CL on rpspk.id_AttachCL equals cl_header.id_CL
                        join cl_detail in _context.Data_CL_Detail on cl_header.id_CL equals cl_detail.id_CL
                        where cl_detail.isApproved == 1 && rpspk.id_header == id
                        select new
                        {
                            cl_detail.id_CL_Detail
                        }).ToList();

                foreach (var value in cl)
                {
                    Data_Owner data_owner = new Data_Owner()
                    {
                        id_CL_Detail = value.id_CL_Detail,
                        create_by = authorization.id_user,
                        status_owner = "",
                        kode_owner = "",
                        merek_edc = "",
                        serial_number = "",
                        master = ""
                    };

                    _context.Data_Owner.Add(data_owner);
                }

                await _context.SaveChangesAsync();

                return Ok(new ResponseStatus { Status = "Success", Message = "TAR & MISMER has been submitted", Data = new { RPSPK = RPSPK, DRPSPK = DRPSPK } });
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        // Input Code Owner (workflow tahap 4)

        [HttpPost("InputOwnerCode/fetchData")]
        public IActionResult InputOwnerCodeFetchData([FromHeader] string token)
        {
            var authorization = authenticateManager.isLoggedIn(token);
            if (authorization is false)
                return StatusCode(StatusCodes.Status401Unauthorized, new ResponseStatus { Status = "Error", Message = "You can't access this method !" });

            try
            {
                var draw = Request.Form["draw"].FirstOrDefault();
                var start = Request.Form["start"].FirstOrDefault();
                var length = Request.Form["length"].FirstOrDefault();
                var sortColumn = Request.Form["columns[" + Request.Form["order[0][column]"].FirstOrDefault() + "][name]"].FirstOrDefault();
                var sortColumnDirection = Request.Form["order[0][dir]"].FirstOrDefault();
                var searchValue = Request.Form["search[value]"].FirstOrDefault();
                int pageSize = length != null ? Convert.ToInt32(length) : 0;
                int skip = start != null ? Convert.ToInt32(start) : 0;
                int recordsTotal = 0;
                var result = (from header in _context.RequestPemasanganSPK_Header
                              join cl_header in _context.Data_CL on header.id_AttachCL equals cl_header.id_CL
                              join cl_detail in _context.Data_CL_Detail on cl_header.id_CL equals cl_detail.id_CL
                              join tar_header in _context.Data_TAR on header.Id_AttachTAR equals tar_header.id_TAR
                              join tar_detail in _context.Data_TAR_Detail on tar_header.id_TAR equals tar_detail.id_TAR
                              join data_owner in _context.Data_Owner on cl_detail.id_CL_Detail equals data_owner.id_CL_Detail
                              where header.id_header == Int64.Parse(Request.Form["id"].FirstOrDefault()) && cl_detail.id_CL_Detail == data_owner.id_CL_Detail
                              && tar_detail.MID == cl_detail.MID
                              select new
                              {
                                  id_header = header.id_header,
                                  id_cl = header.id_AttachCL,
                                  id_tar = header.Id_AttachTAR,
                                  id_detail_cl = cl_detail.id_CL_Detail,
                                  id_owner_code = data_owner.id_owner,
                                  kode_wilayah = cl_detail.KODEWILAYAH,
                                  mid = tar_detail.MID,
                                  merchant = cl_detail.MERCHANTNAME,
                                  address = cl_detail.ADDRESS1,
                                  npwp = cl_detail.NPWP,
                                  tid = tar_detail.TID,
                                  status_owner = data_owner.status_owner,
                                  kode_owner = data_owner.kode_owner,
                                  merek_edc = data_owner.merek_edc,
                                  serial_number = data_owner.serial_number,
                                  master = data_owner.master
                              }).AsQueryable();

                if (authorization.role.type == "Wilayah" || authorization.role.type == "Vendor")
                {
                    string kode_wilayah = authorization.wilayah.code;
                    result = result.Where(x => x.kode_wilayah == kode_wilayah);
                }

                if (!(string.IsNullOrEmpty(sortColumn) && string.IsNullOrEmpty(sortColumnDirection)))
                {
                    sortColumn = sortColumn + '_' + sortColumnDirection;
                    switch (sortColumn)
                    {
                        case "mid_asc":
                            result = result.OrderBy(s => s.mid);
                            break;
                        case "mid_desc":
                            result = result.OrderByDescending(s => s.mid);
                            break;
                        case "merchant_asc":
                            result = result.OrderBy(s => s.merchant);
                            break;
                        case "merchant_desc":
                            result = result.OrderByDescending(s => s.merchant);
                            break;
                        case "address_asc":
                            result = result.OrderBy(s => s.address);
                            break;
                        case "address_desc":
                            result = result.OrderByDescending(s => s.address);
                            break;
                        case "npwp_asc":
                            result = result.OrderBy(s => s.npwp);
                            break;
                        case "npwp_desc":
                            result = result.OrderByDescending(s => s.npwp);
                            break;
                        case "tid_asc":
                            result = result.OrderBy(s => s.tid);
                            break;
                        case "tid_desc":
                            result = result.OrderByDescending(s => s.tid);
                            break;
                    }
                }
                if (!string.IsNullOrEmpty(searchValue))
                {
                    result = result.Where(m => m.mid.Contains(searchValue)
                                                || m.merchant.Contains(searchValue)
                                                || m.address.Contains(searchValue)
                                                || m.npwp.Contains(searchValue));
                }
                recordsTotal = result.Count();
                var data = result.Skip(skip).Take(pageSize).ToList();
                var jsonData = new { draw = draw, recordsFiltered = recordsTotal, recordsTotal = recordsTotal, data = data };
                return Ok(jsonData);
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        [HttpGet("OwnerCode/{id_owner_code}")]
        public async Task<IActionResult> getOwnerCode([FromHeader] string token, int id_owner_code)
        {
            var owner_code = _context.Data_Owner.Where(x => x.id_owner == id_owner_code).FirstOrDefault();

            return Ok(new ResponseStatus { Status = "Success", Message = "Owner Code data has been updated", Data = owner_code });
        }

        [HttpPut("UpdateOwnerCode/{id_owner_code}")]
        public async Task<IActionResult> updateOwnerCode([FromBody] JsonElement request, [FromHeader] string token, int id_owner_code)
        {
            var owner_code = _context.Data_Owner.Where(x => x.id_owner == id_owner_code).FirstOrDefault();
            owner_code.status_owner = request.GetProperty("status_owner").ToString();
            owner_code.kode_owner = request.GetProperty("kode_owner").ToString();
            owner_code.master = request.GetProperty("master").ToString();
            owner_code.merek_edc = (request.GetProperty("status_owner").ToString() == "Sewa") ? "" : request.GetProperty("merek_edc").ToString();
            owner_code.serial_number = (request.GetProperty("status_owner").ToString() == "Sewa") ? "" : request.GetProperty("serial_number").ToString();

            await _context.SaveChangesAsync();

            return Ok(new ResponseStatus { Status = "Success", Message = "Owner Code data has been updated", Data = owner_code });
        }

        [HttpGet("OwnerCode/validation/{id}")]
        public async Task<IActionResult> validationOnwerCode([FromHeader] string token, int id)
        {
            var validation = (from header in _context.RequestPemasanganSPK_Header
                              join cl_header in _context.Data_CL on header.id_AttachCL equals cl_header.id_CL
                              join cl_detail in _context.Data_CL_Detail on cl_header.id_CL equals cl_detail.id_CL
                              join tar_header in _context.Data_TAR on header.Id_AttachTAR equals tar_header.id_TAR
                              join tar_detail in _context.Data_TAR_Detail on tar_header.id_TAR equals tar_detail.id_TAR
                              join data_owner in _context.Data_Owner on cl_detail.id_CL_Detail equals data_owner.id_CL_Detail
                              where header.id_header == id && cl_detail.id_CL_Detail == data_owner.id_CL_Detail
                              && tar_detail.MID == cl_detail.MID && data_owner.status_owner == ""
                              select new
                              {
                                  data_owner
                              }).AsQueryable().ToList();

            return Ok(new ResponseStatus { Status = "Success", Message = "Owner Code check validation has been retrieve", Data = validation });
        }

        [HttpPut("SubmitOwnerCode/{id}")]
        public async Task<IActionResult> SubmitOwnerCode([FromHeader] string token, int id)
        {
            var authorization = authenticateManager.isLoggedIn(token);
            if (authorization is false)
                return StatusCode(StatusCodes.Status401Unauthorized, new ResponseStatus { Status = "Error", Message = "You can't access this method !" });

            try
            {
                var rpspk = _context.RequestPemasanganSPK_Header.Where(x => x.id_header == id).FirstOrDefault();
                rpspk.staging = 4;

                DetailRequestPemasanganSPK drpspk = new DetailRequestPemasanganSPK()
                {
                    id_header_RPSPK = rpspk.id_header,
                    staging = 4,
                    create_by = authorization.id_user,
                    create_time = DateTime.Now
                };
                _context.Detail_RequestPemasanganSPK.Add(drpspk);

                await _context.SaveChangesAsync();

                return Ok(new ResponseStatus { Status = "Success", Message = "Input Owner Code has been submitted", Data = new { rpspk = rpspk, drpspk = drpspk } });
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        [HttpPost("SettingProfiling/Sewa")]
        public IActionResult SettingsProfilingSewa([FromHeader] string token)
        {

            var authorization = authenticateManager.isLoggedIn(token);
            if (authorization is false)
                return StatusCode(StatusCodes.Status401Unauthorized, new ResponseStatus { Status = "Error", Message = "You can't access this method !" });

            try
            {
                var draw = Request.Form["draw"].FirstOrDefault();
                var start = Request.Form["start"].FirstOrDefault();
                var length = Request.Form["length"].FirstOrDefault();
                var sortColumn = Request.Form["columns[" + Request.Form["order[0][column]"].FirstOrDefault() + "][name]"].FirstOrDefault();
                var sortColumnDirection = Request.Form["order[0][dir]"].FirstOrDefault();
                var searchValue = Request.Form["search[value]"].FirstOrDefault();
                int pageSize = length != null ? Convert.ToInt32(length) : 0;
                int skip = start != null ? Convert.ToInt32(start) : 0;
                int recordsTotal = 0;
                var result = (from header in _context.RequestPemasanganSPK_Header
                              join cl_header in _context.Data_CL on header.id_AttachCL equals cl_header.id_CL
                              join cl_detail in _context.Data_CL_Detail on cl_header.id_CL equals cl_detail.id_CL
                              join tar_header in _context.Data_TAR on header.Id_AttachTAR equals tar_header.id_TAR
                              join tar_detail in _context.Data_TAR_Detail on tar_header.id_TAR equals tar_detail.id_TAR
                              join data_owner in _context.Data_Owner on cl_detail.id_CL_Detail equals data_owner.id_CL_Detail
                              where header.id_header == Int64.Parse(Request.Form["id"].FirstOrDefault()) && cl_detail.id_CL_Detail == data_owner.id_CL_Detail
                              && tar_detail.MID == cl_detail.MID && data_owner.status_owner == "Sewa"
                              select new
                              {
                                  id_header = header.id_header,
                                  id_cl = header.id_AttachCL,
                                  id_tar = header.Id_AttachTAR,
                                  kode_wilayah = cl_detail.KODEWILAYAH,
                                  mid = cl_detail.MID,
                                  merchant = cl_detail.MERCHANTNAME,
                                  address = cl_detail.ADDRESS1,
                                  npwp = cl_detail.NPWP,
                                  tid = tar_detail.TID,
                                  status_owner = data_owner.status_owner,
                                  kode_owner = data_owner.kode_owner,
                                  merek_edc = data_owner.merek_edc,
                                  serial_number = data_owner.serial_number,
                                  master = data_owner.master
                              }).AsQueryable();

                if (authorization.role.type == "Wilayah" || authorization.role.type == "Vendor")
                {
                    string kode_wilayah = authorization.wilayah.code;
                    result = result.Where(x => x.kode_wilayah == kode_wilayah);
                }

                if (!(string.IsNullOrEmpty(sortColumn) && string.IsNullOrEmpty(sortColumnDirection)))
                {
                    sortColumn = sortColumn + '_' + sortColumnDirection;
                    switch (sortColumn)
                    {
                        case "mid_asc":
                            result = result.OrderBy(s => s.mid);
                            break;
                        case "mid_desc":
                            result = result.OrderByDescending(s => s.mid);
                            break;
                        case "merchant_asc":
                            result = result.OrderBy(s => s.merchant);
                            break;
                        case "merchant_desc":
                            result = result.OrderByDescending(s => s.merchant);
                            break;
                        case "address_asc":
                            result = result.OrderBy(s => s.address);
                            break;
                        case "address_desc":
                            result = result.OrderByDescending(s => s.address);
                            break;
                        case "npwp_asc":
                            result = result.OrderBy(s => s.npwp);
                            break;
                        case "npwp_desc":
                            result = result.OrderByDescending(s => s.npwp);
                            break;
                        case "tid_asc":
                            result = result.OrderBy(s => s.tid);
                            break;
                        case "tid_desc":
                            result = result.OrderByDescending(s => s.tid);
                            break;
                        case "status_owner_asc":
                            result = result.OrderBy(s => s.status_owner);
                            break;
                        case "status_owner_desc":
                            result = result.OrderByDescending(s => s.status_owner);
                            break;
                        case "kode_owner_asc":
                            result = result.OrderBy(s => s.kode_owner);
                            break;
                        case "kode_owner_desc":
                            result = result.OrderByDescending(s => s.kode_owner);
                            break;
                        case "merek_edc_asc":
                            result = result.OrderBy(s => s.merek_edc);
                            break;
                        case "merek_edc_desc":
                            result = result.OrderByDescending(s => s.merek_edc);
                            break;
                        case "serial_number_asc":
                            result = result.OrderBy(s => s.serial_number);
                            break;
                        case "serial_number_desc":
                            result = result.OrderByDescending(s => s.serial_number);
                            break;
                    }
                }
                if (!string.IsNullOrEmpty(searchValue))
                {
                    result = result.Where(m => m.mid.Contains(searchValue)
                                            || m.merchant.Contains(searchValue)
                                            || m.address.Contains(searchValue)
                                            || m.npwp.Contains(searchValue)
                                            || m.tid.ToString().Contains(searchValue)
                                            || m.status_owner.Contains(searchValue)
                                            || m.kode_owner.Contains(searchValue)
                                            || m.merek_edc.Contains(searchValue)
                                            || m.serial_number.Contains(searchValue));
                }
                recordsTotal = result.Count();
                var data = result.Skip(skip).Take(pageSize).ToList();
                var jsonData = new { draw = draw, recordsFiltered = recordsTotal, recordsTotal = recordsTotal, data = data };
                return Ok(jsonData);
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        [HttpPost("SettingProfiling/Milik")]
        public IActionResult SettingsProfilingMilik([FromHeader] string token)
        {

            var authorization = authenticateManager.isLoggedIn(token);
            if (authorization is false)
                return StatusCode(StatusCodes.Status401Unauthorized, new ResponseStatus { Status = "Error", Message = "You can't access this method !" });

            try
            {
                var draw = Request.Form["draw"].FirstOrDefault();
                var start = Request.Form["start"].FirstOrDefault();
                var length = Request.Form["length"].FirstOrDefault();
                var sortColumn = Request.Form["columns[" + Request.Form["order[0][column]"].FirstOrDefault() + "][name]"].FirstOrDefault();
                var sortColumnDirection = Request.Form["order[0][dir]"].FirstOrDefault();
                var searchValue = Request.Form["search[value]"].FirstOrDefault();
                int pageSize = length != null ? Convert.ToInt32(length) : 0;
                int skip = start != null ? Convert.ToInt32(start) : 0;
                int recordsTotal = 0;
                var result = (from header in _context.RequestPemasanganSPK_Header
                              join cl_header in _context.Data_CL on header.id_AttachCL equals cl_header.id_CL
                              join cl_detail in _context.Data_CL_Detail on cl_header.id_CL equals cl_detail.id_CL
                              join tar_header in _context.Data_TAR on header.Id_AttachTAR equals tar_header.id_TAR
                              join tar_detail in _context.Data_TAR_Detail on tar_header.id_TAR equals tar_detail.id_TAR
                              join data_owner in _context.Data_Owner on cl_detail.id_CL_Detail equals data_owner.id_CL_Detail
                              where header.id_header == Int64.Parse(Request.Form["id"].FirstOrDefault()) && cl_detail.id_CL_Detail == data_owner.id_CL_Detail
                              && tar_detail.MID == cl_detail.MID && data_owner.status_owner == "Milik"
                              select new
                              {
                                  id_header = header.id_header,
                                  id_cl = header.id_AttachCL,
                                  id_tar = header.Id_AttachTAR,
                                  kode_wilayah = cl_detail.KODEWILAYAH,
                                  mid = cl_detail.MID,
                                  merchant = cl_detail.MERCHANTNAME,
                                  address = cl_detail.ADDRESS1,
                                  npwp = cl_detail.NPWP,
                                  tid = tar_detail.TID,
                                  status_owner = data_owner.status_owner,
                                  kode_owner = data_owner.kode_owner,
                                  merek_edc = data_owner.merek_edc,
                                  serial_number = data_owner.serial_number,
                                  master = data_owner.master
                              }).AsQueryable();

                if (authorization.role.type == "Wilayah" || authorization.role.type == "Vendor")
                {
                    string kode_wilayah = authorization.wilayah.code;
                    result = result.Where(x => x.kode_wilayah == kode_wilayah);
                }

                if (!(string.IsNullOrEmpty(sortColumn) && string.IsNullOrEmpty(sortColumnDirection)))
                {
                    sortColumn = sortColumn + '_' + sortColumnDirection;
                    switch (sortColumn)
                    {
                        case "mid_asc":
                            result = result.OrderBy(s => s.mid);
                            break;
                        case "mid_desc":
                            result = result.OrderByDescending(s => s.mid);
                            break;
                        case "merchant_asc":
                            result = result.OrderBy(s => s.merchant);
                            break;
                        case "merchant_desc":
                            result = result.OrderByDescending(s => s.merchant);
                            break;
                        case "address_asc":
                            result = result.OrderBy(s => s.address);
                            break;
                        case "address_desc":
                            result = result.OrderByDescending(s => s.address);
                            break;
                        case "npwp_asc":
                            result = result.OrderBy(s => s.npwp);
                            break;
                        case "npwp_desc":
                            result = result.OrderByDescending(s => s.npwp);
                            break;
                        case "tid_asc":
                            result = result.OrderBy(s => s.tid);
                            break;
                        case "tid_desc":
                            result = result.OrderByDescending(s => s.tid);
                            break;
                        case "status_owner_asc":
                            result = result.OrderBy(s => s.status_owner);
                            break;
                        case "status_owner_desc":
                            result = result.OrderByDescending(s => s.status_owner);
                            break;
                        case "kode_owner_asc":
                            result = result.OrderBy(s => s.kode_owner);
                            break;
                        case "kode_owner_desc":
                            result = result.OrderByDescending(s => s.kode_owner);
                            break;
                        case "merek_edc_asc":
                            result = result.OrderBy(s => s.merek_edc);
                            break;
                        case "merek_edc_desc":
                            result = result.OrderByDescending(s => s.merek_edc);
                            break;
                        case "serial_number_asc":
                            result = result.OrderBy(s => s.serial_number);
                            break;
                        case "serial_number_desc":
                            result = result.OrderByDescending(s => s.serial_number);
                            break;
                    }
                }
                if (!string.IsNullOrEmpty(searchValue))
                {
                    result = result.Where(m => m.mid.Contains(searchValue)
                                            || m.merchant.Contains(searchValue)
                                            || m.address.Contains(searchValue)
                                            || m.npwp.Contains(searchValue)
                                            || m.tid.ToString().Contains(searchValue)
                                            || m.status_owner.Contains(searchValue)
                                            || m.kode_owner.Contains(searchValue)
                                            || m.merek_edc.Contains(searchValue)
                                            || m.serial_number.Contains(searchValue));
                }
                recordsTotal = result.Count();
                var data = result.Skip(skip).Take(pageSize).ToList();
                var jsonData = new { draw = draw, recordsFiltered = recordsTotal, recordsTotal = recordsTotal, data = data };
                return Ok(jsonData);
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        [HttpPost("SettingProfiling/Ingenico")]
        public IActionResult SettingsProfilingIngenico()
        {
            try
            {
                var draw = Request.Form["draw"].FirstOrDefault();
                var start = Request.Form["start"].FirstOrDefault();
                var length = Request.Form["length"].FirstOrDefault();
                var sortColumn = Request.Form["columns[" + Request.Form["order[0][column]"].FirstOrDefault() + "][name]"].FirstOrDefault();
                var sortColumnDirection = Request.Form["order[0][dir]"].FirstOrDefault();
                var searchValue = Request.Form["search[value]"].FirstOrDefault();
                int pageSize = length != null ? Convert.ToInt32(length) : 0;
                int skip = start != null ? Convert.ToInt32(start) : 0;
                int recordsTotal = 0;
                var result = (from header in _context.RequestPemasanganSPK_Header
                              join cl_header in _context.Data_CL on header.id_AttachCL equals cl_header.id_CL
                              join cl_detail in _context.Data_CL_Detail on cl_header.id_CL equals cl_detail.id_CL
                              join tar_header in _context.Data_TAR on header.Id_AttachTAR equals tar_header.id_TAR
                              join tar_detail in _context.Data_TAR_Detail on tar_header.id_TAR equals tar_detail.id_TAR
                              join data_owner in _context.Data_Owner on cl_detail.id_CL_Detail equals data_owner.id_CL_Detail
                              where header.id_header == Int64.Parse(Request.Form["id"].FirstOrDefault()) && cl_detail.id_CL_Detail == data_owner.id_CL_Detail
                              && tar_detail.MID == cl_detail.MID && data_owner.status_owner == "Milik" && data_owner.merek_edc == "Ingenico"
                              select new
                              {
                                  id_header = header.id_header,
                                  id_cl = header.id_AttachCL,
                                  id_tar = header.Id_AttachTAR,
                                  mid = cl_detail.MID,
                                  merchant = cl_detail.MERCHANTNAME,
                                  address = cl_detail.ADDRESS1,
                                  npwp = cl_detail.NPWP,
                                  tid = tar_detail.TID,
                                  status_owner = data_owner.status_owner,
                                  kode_owner = data_owner.kode_owner,
                                  merek_edc = data_owner.merek_edc,
                                  serial_number = data_owner.serial_number

                              }).AsQueryable();
                if (!(string.IsNullOrEmpty(sortColumn) && string.IsNullOrEmpty(sortColumnDirection)))
                {
                    sortColumn = sortColumn + '_' + sortColumnDirection;
                    switch (sortColumn)
                    {
                        case "mid_asc":
                            result = result.OrderBy(s => s.mid);
                            break;
                        case "mid_desc":
                            result = result.OrderByDescending(s => s.mid);
                            break;
                        case "merchant_asc":
                            result = result.OrderBy(s => s.merchant);
                            break;
                        case "merchant_desc":
                            result = result.OrderByDescending(s => s.merchant);
                            break;
                        case "address_asc":
                            result = result.OrderBy(s => s.address);
                            break;
                        case "address_desc":
                            result = result.OrderByDescending(s => s.address);
                            break;
                        case "npwp_asc":
                            result = result.OrderBy(s => s.npwp);
                            break;
                        case "npwp_desc":
                            result = result.OrderByDescending(s => s.npwp);
                            break;
                        case "tid_asc":
                            result = result.OrderBy(s => s.tid);
                            break;
                        case "tid_desc":
                            result = result.OrderByDescending(s => s.tid);
                            break;
                        case "status_owner_asc":
                            result = result.OrderBy(s => s.status_owner);
                            break;
                        case "status_owner_desc":
                            result = result.OrderByDescending(s => s.status_owner);
                            break;
                        case "kode_owner_asc":
                            result = result.OrderBy(s => s.kode_owner);
                            break;
                        case "kode_owner_desc":
                            result = result.OrderByDescending(s => s.kode_owner);
                            break;
                        case "merek_edc_asc":
                            result = result.OrderBy(s => s.merek_edc);
                            break;
                        case "merek_edc_desc":
                            result = result.OrderByDescending(s => s.merek_edc);
                            break;
                        case "serial_number_asc":
                            result = result.OrderBy(s => s.serial_number);
                            break;
                        case "serial_number_desc":
                            result = result.OrderByDescending(s => s.serial_number);
                            break;
                    }
                }
                if (!string.IsNullOrEmpty(searchValue))
                {
                    result = result.Where(m => m.mid.Contains(searchValue)
                                            || m.merchant.Contains(searchValue)
                                            || m.address.Contains(searchValue)
                                            || m.npwp.Contains(searchValue)
                                            || m.tid.ToString().Contains(searchValue)
                                            || m.status_owner.Contains(searchValue)
                                            || m.kode_owner.Contains(searchValue)
                                            || m.merek_edc.Contains(searchValue)
                                            || m.serial_number.Contains(searchValue));
                }
                recordsTotal = result.Count();
                var data = result.Skip(skip).Take(pageSize).ToList();
                var jsonData = new { draw = draw, recordsFiltered = recordsTotal, recordsTotal = recordsTotal, data = data };
                return Ok(jsonData);
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        [HttpPost("SettingProfiling/Verifone")]
        public IActionResult SettingsProfilingVerifone()
        {
            try
            {
                var draw = Request.Form["draw"].FirstOrDefault();
                var start = Request.Form["start"].FirstOrDefault();
                var length = Request.Form["length"].FirstOrDefault();
                var sortColumn = Request.Form["columns[" + Request.Form["order[0][column]"].FirstOrDefault() + "][name]"].FirstOrDefault();
                var sortColumnDirection = Request.Form["order[0][dir]"].FirstOrDefault();
                var searchValue = Request.Form["search[value]"].FirstOrDefault();
                int pageSize = length != null ? Convert.ToInt32(length) : 0;
                int skip = start != null ? Convert.ToInt32(start) : 0;
                int recordsTotal = 0;
                var result = (from header in _context.RequestPemasanganSPK_Header
                              join cl_header in _context.Data_CL on header.id_AttachCL equals cl_header.id_CL
                              join cl_detail in _context.Data_CL_Detail on cl_header.id_CL equals cl_detail.id_CL
                              join tar_header in _context.Data_TAR on header.Id_AttachTAR equals tar_header.id_TAR
                              join tar_detail in _context.Data_TAR_Detail on tar_header.id_TAR equals tar_detail.id_TAR
                              join data_owner in _context.Data_Owner on cl_detail.id_CL_Detail equals data_owner.id_CL_Detail
                              where header.id_header == Int64.Parse(Request.Form["id"].FirstOrDefault()) && cl_detail.id_CL_Detail == data_owner.id_CL_Detail
                              && tar_detail.MID == cl_detail.MID && data_owner.status_owner == "Milik" && data_owner.merek_edc == "Verifone"
                              select new
                              {
                                  id_header = header.id_header,
                                  id_cl = header.id_AttachCL,
                                  id_tar = header.Id_AttachTAR,
                                  mid = cl_detail.MID,
                                  merchant = cl_detail.MERCHANTNAME,
                                  address = cl_detail.ADDRESS1,
                                  npwp = cl_detail.NPWP,
                                  tid = tar_detail.TID,
                                  status_owner = data_owner.status_owner,
                                  kode_owner = data_owner.kode_owner,
                                  merek_edc = data_owner.merek_edc,
                                  serial_number = data_owner.serial_number

                              }).AsQueryable();
                if (!(string.IsNullOrEmpty(sortColumn) && string.IsNullOrEmpty(sortColumnDirection)))
                {
                    sortColumn = sortColumn + '_' + sortColumnDirection;
                    switch (sortColumn)
                    {
                        case "mid_asc":
                            result = result.OrderBy(s => s.mid);
                            break;
                        case "mid_desc":
                            result = result.OrderByDescending(s => s.mid);
                            break;
                        case "merchant_asc":
                            result = result.OrderBy(s => s.merchant);
                            break;
                        case "merchant_desc":
                            result = result.OrderByDescending(s => s.merchant);
                            break;
                        case "address_asc":
                            result = result.OrderBy(s => s.address);
                            break;
                        case "address_desc":
                            result = result.OrderByDescending(s => s.address);
                            break;
                        case "npwp_asc":
                            result = result.OrderBy(s => s.npwp);
                            break;
                        case "npwp_desc":
                            result = result.OrderByDescending(s => s.npwp);
                            break;
                        case "tid_asc":
                            result = result.OrderBy(s => s.tid);
                            break;
                        case "tid_desc":
                            result = result.OrderByDescending(s => s.tid);
                            break;
                        case "status_owner_asc":
                            result = result.OrderBy(s => s.status_owner);
                            break;
                        case "status_owner_desc":
                            result = result.OrderByDescending(s => s.status_owner);
                            break;
                        case "kode_owner_asc":
                            result = result.OrderBy(s => s.kode_owner);
                            break;
                        case "kode_owner_desc":
                            result = result.OrderByDescending(s => s.kode_owner);
                            break;
                        case "merek_edc_asc":
                            result = result.OrderBy(s => s.merek_edc);
                            break;
                        case "merek_edc_desc":
                            result = result.OrderByDescending(s => s.merek_edc);
                            break;
                        case "serial_number_asc":
                            result = result.OrderBy(s => s.serial_number);
                            break;
                        case "serial_number_desc":
                            result = result.OrderByDescending(s => s.serial_number);
                            break;
                    }
                }
                if (!string.IsNullOrEmpty(searchValue))
                {
                    result = result.Where(m => m.mid.Contains(searchValue)
                                            || m.merchant.Contains(searchValue)
                                            || m.address.Contains(searchValue)
                                            || m.npwp.Contains(searchValue)
                                            || m.tid.ToString().Contains(searchValue)
                                            || m.status_owner.Contains(searchValue)
                                            || m.kode_owner.Contains(searchValue)
                                            || m.merek_edc.Contains(searchValue)
                                            || m.serial_number.Contains(searchValue));
                }
                recordsTotal = result.Count();
                var data = result.Skip(skip).Take(pageSize).ToList();
                var jsonData = new { draw = draw, recordsFiltered = recordsTotal, recordsTotal = recordsTotal, data = data };
                return Ok(jsonData);
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        [HttpPost("SettingProfiling/PAX")]
        public IActionResult SettingsProfilingPAX()
        {
            try
            {
                var draw = Request.Form["draw"].FirstOrDefault();
                var start = Request.Form["start"].FirstOrDefault();
                var length = Request.Form["length"].FirstOrDefault();
                var sortColumn = Request.Form["columns[" + Request.Form["order[0][column]"].FirstOrDefault() + "][name]"].FirstOrDefault();
                var sortColumnDirection = Request.Form["order[0][dir]"].FirstOrDefault();
                var searchValue = Request.Form["search[value]"].FirstOrDefault();
                int pageSize = length != null ? Convert.ToInt32(length) : 0;
                int skip = start != null ? Convert.ToInt32(start) : 0;
                int recordsTotal = 0;
                var result = (from header in _context.RequestPemasanganSPK_Header
                              join cl_header in _context.Data_CL on header.id_AttachCL equals cl_header.id_CL
                              join cl_detail in _context.Data_CL_Detail on cl_header.id_CL equals cl_detail.id_CL
                              join tar_header in _context.Data_TAR on header.Id_AttachTAR equals tar_header.id_TAR
                              join tar_detail in _context.Data_TAR_Detail on tar_header.id_TAR equals tar_detail.id_TAR
                              join data_owner in _context.Data_Owner on cl_detail.id_CL_Detail equals data_owner.id_CL_Detail
                              where header.id_header == Int64.Parse(Request.Form["id"].FirstOrDefault()) && cl_detail.id_CL_Detail == data_owner.id_CL_Detail
                              && tar_detail.MID == cl_detail.MID && data_owner.status_owner == "Milik" && data_owner.merek_edc == "PAX"
                              select new
                              {
                                  id_header = header.id_header,
                                  id_cl = header.id_AttachCL,
                                  id_tar = header.Id_AttachTAR,
                                  mid = cl_detail.MID,
                                  merchant = cl_detail.MERCHANTNAME,
                                  address = cl_detail.ADDRESS1,
                                  npwp = cl_detail.NPWP,
                                  tid = tar_detail.TID,
                                  status_owner = data_owner.status_owner,
                                  kode_owner = data_owner.kode_owner,
                                  merek_edc = data_owner.merek_edc,
                                  serial_number = data_owner.serial_number

                              }).AsQueryable();
                if (!(string.IsNullOrEmpty(sortColumn) && string.IsNullOrEmpty(sortColumnDirection)))
                {
                    sortColumn = sortColumn + '_' + sortColumnDirection;
                    switch (sortColumn)
                    {
                        case "mid_asc":
                            result = result.OrderBy(s => s.mid);
                            break;
                        case "mid_desc":
                            result = result.OrderByDescending(s => s.mid);
                            break;
                        case "merchant_asc":
                            result = result.OrderBy(s => s.merchant);
                            break;
                        case "merchant_desc":
                            result = result.OrderByDescending(s => s.merchant);
                            break;
                        case "address_asc":
                            result = result.OrderBy(s => s.address);
                            break;
                        case "address_desc":
                            result = result.OrderByDescending(s => s.address);
                            break;
                        case "npwp_asc":
                            result = result.OrderBy(s => s.npwp);
                            break;
                        case "npwp_desc":
                            result = result.OrderByDescending(s => s.npwp);
                            break;
                        case "tid_asc":
                            result = result.OrderBy(s => s.tid);
                            break;
                        case "tid_desc":
                            result = result.OrderByDescending(s => s.tid);
                            break;
                        case "status_owner_asc":
                            result = result.OrderBy(s => s.status_owner);
                            break;
                        case "status_owner_desc":
                            result = result.OrderByDescending(s => s.status_owner);
                            break;
                        case "kode_owner_asc":
                            result = result.OrderBy(s => s.kode_owner);
                            break;
                        case "kode_owner_desc":
                            result = result.OrderByDescending(s => s.kode_owner);
                            break;
                        case "merek_edc_asc":
                            result = result.OrderBy(s => s.merek_edc);
                            break;
                        case "merek_edc_desc":
                            result = result.OrderByDescending(s => s.merek_edc);
                            break;
                        case "serial_number_asc":
                            result = result.OrderBy(s => s.serial_number);
                            break;
                        case "serial_number_desc":
                            result = result.OrderByDescending(s => s.serial_number);
                            break;
                    }
                }
                if (!string.IsNullOrEmpty(searchValue))
                {
                    result = result.Where(m => m.mid.Contains(searchValue)
                                            || m.merchant.Contains(searchValue)
                                            || m.address.Contains(searchValue)
                                            || m.npwp.Contains(searchValue)
                                            || m.tid.ToString().Contains(searchValue)
                                            || m.status_owner.Contains(searchValue)
                                            || m.kode_owner.Contains(searchValue)
                                            || m.merek_edc.Contains(searchValue)
                                            || m.serial_number.Contains(searchValue));
                }
                recordsTotal = result.Count();
                var data = result.Skip(skip).Take(pageSize).ToList();
                var jsonData = new { draw = draw, recordsFiltered = recordsTotal, recordsTotal = recordsTotal, data = data };
                return Ok(jsonData);
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        [HttpGet("downloadProfiling/Milik/{id}")]
        public IActionResult downloadProfilingMilik(int id)
        {
            string basePath = _environment.WebRootPath + @"\Template\PROFILING.xlsx";
            using (var workbook = new XLWorkbook(basePath))
            {
                var worksheet = workbook.Worksheets.First();
                var currentRow = 2;

                var firstRowUsed = worksheet.FirstRowUsed();
                var firstPossibleAddress = worksheet.Row(firstRowUsed.RowNumber()).FirstCell().Address;
                var lastPossibleAddress = worksheet.LastCellUsed().Address;

                var range = worksheet.Range(firstPossibleAddress, lastPossibleAddress).ColumnCount();

                var data = (from header in _context.RequestPemasanganSPK_Header
                            join cl_header in _context.Data_CL on header.id_AttachCL equals cl_header.id_CL
                            join cl_detail in _context.Data_CL_Detail on cl_header.id_CL equals cl_detail.id_CL
                            join tar_header in _context.Data_TAR on header.Id_AttachTAR equals tar_header.id_TAR
                            join tar_detail in _context.Data_TAR_Detail on tar_header.id_TAR equals tar_detail.id_TAR
                            join data_owner in _context.Data_Owner on cl_detail.id_CL_Detail equals data_owner.id_CL_Detail
                            where header.id_header == id && cl_detail.id_CL_Detail == data_owner.id_CL_Detail
                            && tar_detail.MID == cl_detail.MID && data_owner.status_owner == "Milik"
                            select new
                            {
                                data_owner.serial_number,
                                cl_detail.MERCHANTNAME,
                                cl_detail.ADDRESS1,
                                cl_detail.ADDRESS2,
                                cl_detail.ADDRESS3,
                                cl_detail.ADDRESS4,
                                tar_detail.MID,
                                tar_detail.TID,
                                data_owner.master,
                                data_owner.merek_edc
                            }).ToList();

                foreach (var item in data)
                {
                    for (int i = 1; i <= range; i++)
                    {
                        if (i == 1)
                        {
                            worksheet.Cell(currentRow, i).Value = "'" + item.MID.ToString();
                        }
                        else if (i == 2)
                        {
                            worksheet.Cell(currentRow, i).Value = "'" + item.TID.ToString();
                        }
                        else if (i == 3)
                        {
                            worksheet.Cell(currentRow, i).Value = item.MERCHANTNAME;
                        }
                        else if (i == 4)
                        {
                            worksheet.Cell(currentRow, i).Value = item.ADDRESS1;
                        }
                        else if (i == 5)
                        {
                            worksheet.Cell(currentRow, i).Value = item.ADDRESS2;
                        }
                        else if (i == 6)
                        {
                            worksheet.Cell(currentRow, i).Value = item.ADDRESS3;
                        }
                        else if (i == 7)
                        {
                            worksheet.Cell(currentRow, i).Value = item.master;
                        }
                        else if(i == 8)
                        {
                            worksheet.Cell(currentRow, i).Value = item.merek_edc;
                         }
                        else
                        {
                            worksheet.Cell(currentRow, i).Value = "";
                        }
                    }
                    currentRow++;
                }

                using (var stream = new MemoryStream())
                {
                    workbook.SaveAs(stream);
                    var content = stream.ToArray();

                    return File(
                        content,
                        "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
                        "PROFILING.xlsx");
                }
            }
        }

        //[HttpGet("downloadProfiling/Ingenico/{id}")]
        //public IActionResult downloadProfilingIngenico(int id)
        //{
        //    using (var workbook = new XLWorkbook())
        //    {
        //        var worksheet = workbook.Worksheets.Add("Sheet1");
        //        var currentRow = 1;
        //        string[] title = { "TerminalID (Serial Number)", "Merchant Name 1 (Merchant Name)",
        //            "Merchant Name 2 (Address 1)", "Merchant Name 3 (Address 3 & 4)", "F99 Password", "F2 Password", "Admin Password",
        //            "Initialise Password", "RD NAME", "YAP_PAN_Merchant_Debit", "YAP_PAN_Merchant_Visa", "YAP_PAN_Merchant_Master",
        //            "YAP_PAN_Merchant_UnikQu", "Master", "CREDIT_Merchant ID (MID)", "CREDIT_Terminal ID (TID)", "CREDIT_Txn Primary Ph", 
        //            "CREDIT_Txn Secondary Ph", "CREDIT_TLE Acquirer", "DEBIT_Merchant ID", "DEBIT_Terminal ID", "DEBIT_Txn Primary Ph", 
        //            "DEBIT_Txn Secondary Ph", "DEBIT_TLE Acquirer", "PREPAID_Merchant ID", "PREPAID_Terminal ID", "PREPAID_Txn Primary Ph", 
        //            "PREPAID_Txn Secondary Ph", "PREPAID_TLE Acquirer", "PREPAID_TLE Acquirer", "MINIATM_Merchant ID", "MINIATM_Terminal ID", 
        //            "MINIATM_Txn Primary Ph", "MINIATM_Txn Secondary Ph", "MINIATM_TLE Acquirer", "ERET_PP_Merchant ID", "ERET_PP_Terminal ID", 
        //            "ERET_PP_Txn Primary Ph", "ERET_PP_Txn Secondary Ph", "ERET_PP_TLE Acquirer", "ERET_MA_Merchant ID", "ERET_MA_Terminal ID", 
        //            "ERET_MA_Txn Primary Ph", "ERET_MA_Txn Secondary Ph", "ERET_MA_TLE Acquirer"};

        //        for(int i = 0; i < title.Length; i++)
        //        {
        //            worksheet.Cell(currentRow, i + 1).Value = title[i];
        //        }

        //        worksheet.Cells("A1:AT1").Style.Alignment.WrapText = true;

        //        var data = (from header in _context.RequestPemasanganSPK_Header
        //                      join cl_header in _context.Data_CL on header.id_AttachCL equals cl_header.id_CL
        //                      join cl_detail in _context.Data_CL_Detail on cl_header.id_CL equals cl_detail.id_CL
        //                      join tar_header in _context.Data_TAR on header.Id_AttachTAR equals tar_header.id_TAR
        //                      join tar_detail in _context.Data_TAR_Detail on tar_header.id_TAR equals tar_detail.id_TAR
        //                      join data_owner in _context.Data_Owner on cl_detail.id_CL_Detail equals data_owner.id_CL_Detail
        //                      where header.id_header == id && cl_detail.id_CL_Detail == data_owner.id_CL_Detail
        //                      && tar_detail.MID == cl_detail.MID && data_owner.status_owner == "Milik" && data_owner.merek_edc == "Ingenico"
        //                      select new
        //                      {
        //                        data_owner.serial_number,
        //                        cl_detail.MERCHANTNAME, 
        //                        cl_detail.ADDRESS1,
        //                        cl_detail.ADDRESS2,
        //                        cl_detail.ADDRESS3,
        //                        cl_detail.ADDRESS4,
        //                        tar_detail.MID,
        //                        tar_detail.TID
        //                      }).ToList();

        //        foreach (var item in data)
        //        {
        //            currentRow++;
        //            for (int i = 1; i <= title.Length; i++)
        //            {
        //                if (i == 1)
        //                {
        //                    worksheet.Cell(currentRow, i).Value = "'" + item.serial_number;
        //                }
        //                else if (i == 2)
        //                {
        //                    worksheet.Cell(currentRow, i).Value = item.MERCHANTNAME;
        //                }
        //                else if (i == 3)
        //                {
        //                    worksheet.Cell(currentRow, i).Value = item.ADDRESS1;
        //                }
        //                else if (i == 4)
        //                {
        //                    worksheet.Cell(currentRow, i).Value = item.ADDRESS3 + " " + item.ADDRESS4;
        //                }
        //                else if (i == 15)
        //                {
        //                    worksheet.Cell(currentRow, i).Value = "'" + item.MID.ToString();
        //                }
        //                else if (i == 16)
        //                {
        //                    worksheet.Cell(currentRow, i).Value = "'" + item.TID.ToString();
        //                }else
        //                {
        //                    worksheet.Cell(currentRow, i).Value = "";
        //                }
        //            }
        //        }

        //        using (var stream = new MemoryStream())
        //        {
        //            workbook.SaveAs(stream);
        //            var content = stream.ToArray();

        //            return File(
        //                content,
        //                "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
        //                "Profiling-Ingenico.xlsx");
        //        }
        //    }
        //}

        [HttpGet("downloadProfiling/Ingenico/{id}")]
        public IActionResult downloadProfilingIngenico(int id)
        {
            string basePath = _environment.WebRootPath + @"\Template\PROFILING-EDC-INGENICO.xlsx";
            using (var workbook = new XLWorkbook(basePath))
            {
                var worksheet = workbook.Worksheets.First();
                var currentRow = 3;

                var firstRowUsed = worksheet.FirstRowUsed();
                var firstPossibleAddress = worksheet.Row(firstRowUsed.RowNumber()).FirstCell().Address;
                var lastPossibleAddress = worksheet.LastCellUsed().Address;

                var range = worksheet.Range(firstPossibleAddress, lastPossibleAddress).ColumnCount();

                var data = (from header in _context.RequestPemasanganSPK_Header
                            join cl_header in _context.Data_CL on header.id_AttachCL equals cl_header.id_CL
                            join cl_detail in _context.Data_CL_Detail on cl_header.id_CL equals cl_detail.id_CL
                            join tar_header in _context.Data_TAR on header.Id_AttachTAR equals tar_header.id_TAR
                            join tar_detail in _context.Data_TAR_Detail on tar_header.id_TAR equals tar_detail.id_TAR
                            join data_owner in _context.Data_Owner on cl_detail.id_CL_Detail equals data_owner.id_CL_Detail
                            where header.id_header == id && cl_detail.id_CL_Detail == data_owner.id_CL_Detail
                            && tar_detail.MID == cl_detail.MID && data_owner.status_owner == "Milik" && data_owner.merek_edc == "Ingenico"
                            select new
                            {
                                data_owner.serial_number,
                                cl_detail.MERCHANTNAME,
                                cl_detail.ADDRESS1,
                                cl_detail.ADDRESS2,
                                cl_detail.ADDRESS3,
                                cl_detail.ADDRESS4,
                                tar_detail.MID,
                                tar_detail.TID
                            }).ToList();

                foreach (var item in data)
                {
                    for (int i = 1; i <= range; i++)
                    {
                        if (i == 1)
                        {
                            worksheet.Cell(currentRow, i).Value = "'" + item.serial_number;
                        }
                        else if (i == 2)
                        {
                            worksheet.Cell(currentRow, i).Value = item.MERCHANTNAME;
                        }
                        else if (i == 3)
                        {
                            worksheet.Cell(currentRow, i).Value = item.ADDRESS1;
                        }
                        else if (i == 4)
                        {
                            worksheet.Cell(currentRow, i).Value = item.ADDRESS2;
                        }
                        else if (i == 5)
                        {
                            worksheet.Cell(currentRow, i).Value = item.ADDRESS3;
                        }
                        else if (i == 16)
                        {
                            worksheet.Cell(currentRow, i).Value = "'" + item.MID.ToString();
                        }
                        else if (i == 17)
                        {
                            worksheet.Cell(currentRow, i).Value = "'" + item.TID.ToString();
                        }
                        else
                        {
                            worksheet.Cell(currentRow, i).Value = "";
                        }
                    }
                    currentRow++;
                }

                using (var stream = new MemoryStream())
                {
                    workbook.SaveAs(stream);
                    var content = stream.ToArray();

                    return File(
                        content,
                        "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
                        "PROFILING-EDC-INGENICO.xlsx");
                }
            }
        }

        //[HttpGet("downloadProfiling/Verifone/{id}")]
        //public IActionResult downloadProfilingVerifone(int id)
        //{
        //    using (var workbook = new XLWorkbook())
        //    {
        //        var worksheet = workbook.Worksheets.Add("Sheet1");
        //        var currentRow = 1;
        //        string[] title = { "Serial Number", "Merchant Name",
        //            "Address 1", "Address 2", "Address 3","YAP MID 6", "YAP MID 4", "YAP MID 5","YAP MID 8",
        //            "TID CREDIT", "MID CREDIT", "NAC CREDIT", "TID DEBIT","MID DEBIT", "NAC DEBIT",
        //            "TID TAPCASH", "MID TAPCASH", "NAC TAPCASH","TID MINIATM", "MID MINIATM", "NAC MINIATM",
        //            "TID TLE", "MID TLE","NAC TLE"};

        //        for (int i = 0; i < title.Length; i++)
        //        {
        //            worksheet.Cell(currentRow, i + 1).Value = title[i];
        //        }

        //        worksheet.Cells("A1:X1").Style.Alignment.WrapText = true;

        //        var data = (from header in _context.RequestPemasanganSPK_Header
        //                    join cl_header in _context.Data_CL on header.id_AttachCL equals cl_header.id_CL
        //                    join cl_detail in _context.Data_CL_Detail on cl_header.id_CL equals cl_detail.id_CL
        //                    join tar_header in _context.Data_TAR on header.Id_AttachTAR equals tar_header.id_TAR
        //                    join tar_detail in _context.Data_TAR_Detail on tar_header.id_TAR equals tar_detail.id_TAR
        //                    join data_owner in _context.Data_Owner on cl_detail.id_CL_Detail equals data_owner.id_CL_Detail
        //                    where header.id_header == id && cl_detail.id_CL_Detail == data_owner.id_CL_Detail
        //                    && tar_detail.MID == cl_detail.MID && data_owner.status_owner == "Milik" && data_owner.merek_edc == "Verifone"
        //                    select new
        //                    {
        //                        data_owner.serial_number,
        //                        cl_detail.MERCHANTNAME,
        //                        cl_detail.ADDRESS1,
        //                        cl_detail.ADDRESS2,
        //                        cl_detail.ADDRESS3,
        //                        cl_detail.ADDRESS4,
        //                        tar_detail.MID,
        //                        tar_detail.TID
        //                    }).ToList();

        //        foreach (var item in data)
        //        {
        //            currentRow++;
        //            for (int i = 1; i <= title.Length; i++)
        //            {
        //                if (i == 1)
        //                {
        //                    worksheet.Cell(currentRow, i).Value = "'" + item.serial_number;
        //                }
        //                else if (i == 2)
        //                {
        //                    worksheet.Cell(currentRow, i).Value = item.MERCHANTNAME;
        //                }
        //                else if (i == 3)
        //                {
        //                    worksheet.Cell(currentRow, i).Value = item.ADDRESS1;
        //                }
        //                else if (i == 4)
        //                {
        //                    worksheet.Cell(currentRow, i).Value = item.ADDRESS2;
        //                }
        //                else if (i == 5)
        //                {
        //                    worksheet.Cell(currentRow, i).Value = item.ADDRESS3;
        //                }
        //                else if (i == 10)
        //                {
        //                    worksheet.Cell(currentRow, i).Value = "'" + item.TID.ToString();
        //                }
        //                else if (i == 11)
        //                {
        //                    worksheet.Cell(currentRow, i).Value = "'" + item.MID.ToString();
        //                }else
        //                {
        //                    worksheet.Cell(currentRow, i).Value = "";
        //                }
        //            }
        //        }

        //        using (var stream = new MemoryStream())
        //        {
        //            workbook.SaveAs(stream);
        //            var content = stream.ToArray();

        //            return File(
        //                content,
        //                "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
        //                "Profiling-Verifone.xlsx");
        //        }
        //    }
        //}

        [HttpGet("downloadProfiling/Verifone/{id}")]
        public IActionResult downloadProfilingVerifone(int id)
        {
            string basePath = _environment.WebRootPath + @"\Template\PROFILING-EDC-VERIFONE.xlsx";
            using (var workbook = new XLWorkbook(basePath))
            {
                var worksheet = workbook.Worksheets.First();
                var currentRow = 3;

                var firstRowUsed = worksheet.FirstRowUsed();
                var firstPossibleAddress = worksheet.Row(firstRowUsed.RowNumber()).FirstCell().Address;
                var lastPossibleAddress = worksheet.LastCellUsed().Address;

                var range = worksheet.Range(firstPossibleAddress, lastPossibleAddress).ColumnCount();

                var data = (from header in _context.RequestPemasanganSPK_Header
                            join cl_header in _context.Data_CL on header.id_AttachCL equals cl_header.id_CL
                            join cl_detail in _context.Data_CL_Detail on cl_header.id_CL equals cl_detail.id_CL
                            join tar_header in _context.Data_TAR on header.Id_AttachTAR equals tar_header.id_TAR
                            join tar_detail in _context.Data_TAR_Detail on tar_header.id_TAR equals tar_detail.id_TAR
                            join data_owner in _context.Data_Owner on cl_detail.id_CL_Detail equals data_owner.id_CL_Detail
                            where header.id_header == id && cl_detail.id_CL_Detail == data_owner.id_CL_Detail
                            && tar_detail.MID == cl_detail.MID && data_owner.status_owner == "Milik" && data_owner.merek_edc == "Verifone"
                            select new
                            {
                                data_owner.serial_number,
                                cl_detail.MERCHANTNAME,
                                cl_detail.ADDRESS1,
                                cl_detail.ADDRESS2,
                                cl_detail.ADDRESS3,
                                cl_detail.ADDRESS4,
                                tar_detail.MID,
                                tar_detail.TID
                            }).ToList();

                foreach (var item in data)
                {
                    for (int i = 1; i <= range; i++)
                    {
                        if (i == 1)
                        {
                            worksheet.Cell(currentRow, i).Value = "'" + item.serial_number;
                        }
                        else if (i == 2)
                        {
                            worksheet.Cell(currentRow, i).Value = item.MERCHANTNAME;
                        }
                        else if (i == 3)
                        {
                            worksheet.Cell(currentRow, i).Value = item.ADDRESS1;
                        }
                        else if (i == 4)
                        {
                            worksheet.Cell(currentRow, i).Value = item.ADDRESS2;
                        }
                        else if (i == 5)
                        {
                            worksheet.Cell(currentRow, i).Value = item.ADDRESS3;
                        }
                        else if (i == 10)
                        {
                            worksheet.Cell(currentRow, i).Value = "'" + item.TID.ToString();
                        }
                        else if (i == 11)
                        {
                            worksheet.Cell(currentRow, i).Value = "'" + item.MID.ToString();
                        }
                        else
                        {
                            worksheet.Cell(currentRow, i).Value = "";
                        }
                    }
                    currentRow++;
                }

                using (var stream = new MemoryStream())
                {
                    workbook.SaveAs(stream);
                    var content = stream.ToArray();

                    return File(
                        content,
                        "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
                        "PROFILING-EDC-VERIFONE.xlsx");
                }
            }
        }

        //[HttpGet("downloadProfiling/PAX/{id}")]
        //public IActionResult downloadProfilingPAX(int id)
        //{
        //    using (var workbook = new XLWorkbook())
        //    {
        //        var worksheet = workbook.Worksheets.Add("Sheet1");
        //        var currentRow = 1;
        //        string[] title = {  "TerminalID", "Site name", "Merchant Name 1", "Merchant Name 2", "Merchant Name 3",
        //            "Init.Phone No", "F99 Password", "F87 Password", "Merchant Password", "Initialise Password/F7978",
        //            "Master", "Kode MID", "CREDIT_Merchant ID", "CREDIT_Terminal ID", "CREDIT_Txn Primary Ph",
        //            "CREDIT_Txn Secondary Ph", "CREDIT_Stl Primary Phone", "CREDIT_Stl Secondary Ph", "CREDIT_TLE Acquirer",
        //            "DEBIT_Merchant ID", "DEBIT_Terminal ID", "DEBIT_Txn Primary Ph", "DEBIT_Txn Secondary Ph",
        //            "DEBIT_Stl Primary Phone", "DEBIT_Stl Secondary Ph", "DEBIT_TLE Acquirer", "PREPAID_Merchant ID",
        //            "PREPAID_Terminal ID", "PREPAID_Txn Primary Ph", "PREPAID_Txn Secondary Ph", "PREPAID_Stl Primary Phone",
        //            "PREPAID_Stl Secondary Ph", "PREPAID_TLE Acquirer", "MINIATM_Merchant ID", "MINIATM_Terminal ID",
        //            "MINIATM_Txn Primary Ph", "MINIATM_Txn Secondary Ph", "MINIATM_Stl Primary Phone", "MINIATM_Stl Secondary Ph",
        //            "MINIATM_TLE Acquirer", "YAP_Merchant ID", "YAP_Terminal ID", "YAP_Txn Primary Ph", "YAP_Txn Secondary Ph",
        //            "YAP_Stl Primary Phone", "YAP_Stl Secondary Ph", "YAP_TLE Acquirer", "YAP_PAN_MERCHANT_DEBIT",
        //            "YAP_PAN_MERCHANT_VISA", "YAP_PAN_MERCHANT_MASTER", "YAP_PAN_MERCHANT_UNIKQU", "LINKAJA_Merchant ID",
        //            "LINKAJA_Terminal ID", "ERETREBUSI_Merchant ID", "ERETREBUSI_Terminal ID", "ERETREBUSI_Txn Primary Ph",
        //            "ERETREBUSI_Txn Secondary Ph", "ERETREBUSI_Stl Primary Phone", "ERETREBUSI_Stl Secondary Ph", "ERETREBUSI_TLE Acquirer"};

        //        for (int i = 0; i < title.Length; i++)
        //        {
        //            worksheet.Cell(currentRow, i + 1).Value = title[i];
        //        }

        //        worksheet.Cells("A1:BH1").Style.Alignment.WrapText = true;

        //        var data = (from header in _context.RequestPemasanganSPK_Header
        //                    join cl_header in _context.Data_CL on header.id_AttachCL equals cl_header.id_CL
        //                    join cl_detail in _context.Data_CL_Detail on cl_header.id_CL equals cl_detail.id_CL
        //                    join tar_header in _context.Data_TAR on header.Id_AttachTAR equals tar_header.id_TAR
        //                    join tar_detail in _context.Data_TAR_Detail on tar_header.id_TAR equals tar_detail.id_TAR
        //                    join data_owner in _context.Data_Owner on cl_detail.id_CL_Detail equals data_owner.id_CL_Detail
        //                    where header.id_header == id && cl_detail.id_CL_Detail == data_owner.id_CL_Detail
        //                    && tar_detail.MID == cl_detail.MID && data_owner.status_owner == "Milik" && data_owner.merek_edc == "PAX"
        //                    select new
        //                    {
        //                        data_owner.serial_number,
        //                        cl_detail.MERCHANTNAME,
        //                        cl_detail.ADDRESS1,
        //                        cl_detail.ADDRESS2,
        //                        cl_detail.ADDRESS3,
        //                        cl_detail.ADDRESS4,
        //                        tar_detail.MID,
        //                        tar_detail.TID
        //                    }).ToList();

        //        foreach (var item in data)
        //        {
        //            currentRow++;
        //            for (int i = 1; i <= title.Length; i++)
        //            {
        //                if (i == 1)
        //                {
        //                    worksheet.Cell(currentRow, i).Value = "'" + item.serial_number;
        //                }
        //                else if (i == 2)
        //                {
        //                    worksheet.Cell(currentRow, i).Value = item.MERCHANTNAME;
        //                }
        //                else if (i == 3)
        //                {
        //                    worksheet.Cell(currentRow, i).Value = item.ADDRESS1;
        //                }
        //                else if (i == 4)
        //                {
        //                    worksheet.Cell(currentRow, i).Value = item.ADDRESS2;
        //                }
        //                else if (i == 5)
        //                {
        //                    worksheet.Cell(currentRow, i).Value = item.ADDRESS3;
        //                }
        //                else if (i == 13)
        //                {
        //                    worksheet.Cell(currentRow, i).Value = "'" + item.MID.ToString();
        //                }
        //                else if (i == 14)
        //                {
        //                    worksheet.Cell(currentRow, i).Value = "'" + item.TID.ToString();
        //                }else
        //                {
        //                    worksheet.Cell(currentRow, i).Value = "";
        //                }
        //            }
        //        }

        //        using (var stream = new MemoryStream())
        //        {
        //            workbook.SaveAs(stream);
        //            var content = stream.ToArray();

        //            return File(
        //                content,
        //                "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
        //                "Profiling-PAX.xlsx");
        //        }
        //    }
        //}

        [HttpGet("downloadProfiling/PAX/{id}")]
        public IActionResult downloadProfilingPAX(int id)
        {
            string basePath = _environment.WebRootPath + @"\Template\PROFILING-EDC-PAX.xlsx";
            using (var workbook = new XLWorkbook(basePath))
            {
                var worksheet = workbook.Worksheets.First();
                var currentRow = 3;

                var firstRowUsed = worksheet.FirstRowUsed();
                var firstPossibleAddress = worksheet.Row(firstRowUsed.RowNumber()).FirstCell().Address;
                var lastPossibleAddress = worksheet.LastCellUsed().Address;

                var range = worksheet.Range(firstPossibleAddress, lastPossibleAddress).ColumnCount();

                var data = (from header in _context.RequestPemasanganSPK_Header
                            join cl_header in _context.Data_CL on header.id_AttachCL equals cl_header.id_CL
                            join cl_detail in _context.Data_CL_Detail on cl_header.id_CL equals cl_detail.id_CL
                            join tar_header in _context.Data_TAR on header.Id_AttachTAR equals tar_header.id_TAR
                            join tar_detail in _context.Data_TAR_Detail on tar_header.id_TAR equals tar_detail.id_TAR
                            join data_owner in _context.Data_Owner on cl_detail.id_CL_Detail equals data_owner.id_CL_Detail
                            where header.id_header == id && cl_detail.id_CL_Detail == data_owner.id_CL_Detail
                            && tar_detail.MID == cl_detail.MID && data_owner.status_owner == "Milik" && data_owner.merek_edc == "PAX"
                            select new
                            {
                                data_owner.serial_number,
                                cl_detail.MERCHANTNAME,
                                cl_detail.ADDRESS1,
                                cl_detail.ADDRESS2,
                                cl_detail.ADDRESS3,
                                cl_detail.ADDRESS4,
                                tar_detail.MID,
                                tar_detail.TID
                            }).ToList();

                foreach (var item in data)
                {
                    for (int i = 1; i <= range; i++)
                    {
                        if (i == 1)
                        {
                            worksheet.Cell(currentRow, i).Value = "'" + item.serial_number;
                        }
                        else if (i == 2)
                        {
                            worksheet.Cell(currentRow, i).Value = item.MERCHANTNAME;
                        }
                        else if (i == 3)
                        {
                            worksheet.Cell(currentRow, i).Value = item.ADDRESS1;
                        }
                        else if (i == 4)
                        {
                            worksheet.Cell(currentRow, i).Value = item.ADDRESS2;
                        }
                        else if (i == 5)
                        {
                            worksheet.Cell(currentRow, i).Value = item.ADDRESS3;
                        }
                        else if (i == 13)
                        {
                            worksheet.Cell(currentRow, i).Value = "'" + item.MID.ToString();
                        }
                        else if (i == 14)
                        {
                            worksheet.Cell(currentRow, i).Value = "'" + item.TID.ToString();
                        }
                        else
                        {
                            worksheet.Cell(currentRow, i).Value = "";
                        }
                    }
                    currentRow++;
                }

                using (var stream = new MemoryStream())
                {
                    workbook.SaveAs(stream);
                    var content = stream.ToArray();

                    return File(
                        content,
                        "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
                        "PROFILING-EDC-PAX.xlsx");
                }
            }
        }

        [HttpPut("SubmitSettingProfiling/{id}")]
        public async Task<IActionResult> SubmitSettingsProfiling([FromHeader] string token, int id)
        {
            var authorization = authenticateManager.isLoggedIn(token);
            if (authorization is false)
                return StatusCode(StatusCodes.Status401Unauthorized, new ResponseStatus { Status = "Error", Message = "You can't access this method !" });

            try
            {
                var rpspk = _context.RequestPemasanganSPK_Header.Where(x => x.id_header == id).FirstOrDefault();
                rpspk.staging = 5;

                DetailRequestPemasanganSPK drpspk = new DetailRequestPemasanganSPK()
                {
                    id_header_RPSPK = rpspk.id_header,
                    staging = 5,
                    create_by = authorization.id_user,
                    create_time = DateTime.Now
                };
                _context.Detail_RequestPemasanganSPK.Add(drpspk);
                await _context.SaveChangesAsync();

                return Ok(new ResponseStatus { Status = "Success", Message = "Profiling has been submitted", Data = new { rpspk = rpspk, drpspk = drpspk } });
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        [HttpGet("GenerateNewSPK/GetGroupOwnerCode/{id}")]
        public IActionResult GetGroupOwnerCode(int id)
        {
            var data = (from header in _context.RequestPemasanganSPK_Header
                        join cl_header in _context.Data_CL on header.id_AttachCL equals cl_header.id_CL
                        join cl_detail in _context.Data_CL_Detail on cl_header.id_CL equals cl_detail.id_CL
                        join tar_header in _context.Data_TAR on header.Id_AttachTAR equals tar_header.id_TAR
                        join tar_detail in _context.Data_TAR_Detail on tar_header.id_TAR equals tar_detail.id_TAR
                        join data_owner in _context.Data_Owner on cl_detail.id_CL_Detail equals data_owner.id_CL_Detail
                        where header.id_header == id && cl_detail.id_CL_Detail == data_owner.id_CL_Detail
                        && tar_detail.MID == cl_detail.MID
                        group data_owner by new { data_owner.kode_owner } into g
                        select new
                        {
                            g.Key.kode_owner
                        }).AsQueryable().Select(x => x.kode_owner).ToArray();

            return Ok(new ResponseStatus { Status = "Success", Message = "Data has been retrieve", Data = data });
        }

        [HttpPost("GenerateNewSPK/fetchData")]
        public IActionResult GenerateNewSPK([FromHeader] string token)
        {
            var authorization = authenticateManager.isLoggedIn(token);
            if (authorization is false)
                return StatusCode(StatusCodes.Status401Unauthorized, new ResponseStatus { Status = "Error", Message = "You can't access this method !" });

            try
            {
                var draw = Request.Form["draw"].FirstOrDefault();
                var start = Request.Form["start"].FirstOrDefault();
                var length = Request.Form["length"].FirstOrDefault();
                var sortColumn = Request.Form["columns[" + Request.Form["order[0][column]"].FirstOrDefault() + "][name]"].FirstOrDefault();
                var sortColumnDirection = Request.Form["order[0][dir]"].FirstOrDefault();
                var searchValue = Request.Form["search[value]"].FirstOrDefault();
                int pageSize = length != null ? Convert.ToInt32(length) : 0;
                int skip = start != null ? Convert.ToInt32(start) : 0;
                int recordsTotal = 0;
                var result = (from header in _context.RequestPemasanganSPK_Header
                              join cl_header in _context.Data_CL on header.id_AttachCL equals cl_header.id_CL
                              join cl_detail in _context.Data_CL_Detail on cl_header.id_CL equals cl_detail.id_CL
                              join tar_header in _context.Data_TAR on header.Id_AttachTAR equals tar_header.id_TAR
                              join tar_detail in _context.Data_TAR_Detail on tar_header.id_TAR equals tar_detail.id_TAR
                              join data_owner in _context.Data_Owner on cl_detail.id_CL_Detail equals data_owner.id_CL_Detail
                              where header.id_header == Int64.Parse(Request.Form["id"].FirstOrDefault()) && cl_detail.id_CL_Detail == data_owner.id_CL_Detail
                              && tar_detail.MID == cl_detail.MID && data_owner.kode_owner == Request.Form["owner_code"].FirstOrDefault()
                              select new
                              {
                                  id_header = header.id_header,
                                  id_cl = header.id_AttachCL,
                                  id_tar = header.Id_AttachTAR,
                                  id_cl_detail = cl_detail.id_CL_Detail,
                                  kode_wilayah = cl_detail.KODEWILAYAH,
                                  mid = cl_detail.MID,
                                  merchant = cl_detail.MERCHANTNAME,
                                  address = cl_detail.ADDRESS1,
                                  npwp = cl_detail.NPWP,
                                  tid = tar_detail.TID,
                                  status_owner = data_owner.status_owner,
                                  kode_owner = data_owner.kode_owner,
                                  merek_edc = data_owner.merek_edc,
                                  serial_number = data_owner.serial_number,
                                  isProcessed = cl_detail.isProcessed,
                                  status = _context.Status_Pemasangan.Where(x => x.id_spk == header.id_header && x.tid == tar_detail.TID).FirstOrDefault() != null ? "Generated" : "Not generated" 
                              }).AsQueryable();

                if (authorization.role.type == "Wilayah" || authorization.role.type == "Vendor")
                {
                    string kode_wilayah = authorization.wilayah.code;
                    result = result.Where(x => x.kode_wilayah == kode_wilayah);
                }

                if (!(string.IsNullOrEmpty(sortColumn) && string.IsNullOrEmpty(sortColumnDirection)))
                {
                    sortColumn = sortColumn + '_' + sortColumnDirection;
                    switch (sortColumn)
                    {
                        case "mid_asc":
                            result = result.OrderBy(s => s.mid);
                            break;
                        case "mid_desc":
                            result = result.OrderByDescending(s => s.mid);
                            break;
                        case "merchant_asc":
                            result = result.OrderBy(s => s.merchant);
                            break;
                        case "merchant_desc":
                            result = result.OrderByDescending(s => s.merchant);
                            break;
                        case "address_asc":
                            result = result.OrderBy(s => s.address);
                            break;
                        case "address_desc":
                            result = result.OrderByDescending(s => s.address);
                            break;
                        case "npwp_asc":
                            result = result.OrderBy(s => s.npwp);
                            break;
                        case "npwp_desc":
                            result = result.OrderByDescending(s => s.npwp);
                            break;
                        case "tid_asc":
                            result = result.OrderBy(s => s.tid);
                            break;
                        case "tid_desc":
                            result = result.OrderByDescending(s => s.tid);
                            break;
                        case "status_owner_asc":
                            result = result.OrderBy(s => s.status_owner);
                            break;
                        case "status_owner_desc":
                            result = result.OrderByDescending(s => s.status_owner);
                            break;
                        case "kode_owner_asc":
                            result = result.OrderBy(s => s.kode_owner);
                            break;
                        case "kode_owner_desc":
                            result = result.OrderByDescending(s => s.kode_owner);
                            break;
                        case "merek_edc_asc":
                            result = result.OrderBy(s => s.merek_edc);
                            break;
                        case "merek_edc_desc":
                            result = result.OrderByDescending(s => s.merek_edc);
                            break;
                        case "serial_number_asc":
                            result = result.OrderBy(s => s.serial_number);
                            break;
                        case "serial_number_desc":
                            result = result.OrderByDescending(s => s.serial_number);
                            break;
                    }
                }
                if (!string.IsNullOrEmpty(searchValue))
                {
                    result = result.Where(m => m.mid.Contains(searchValue)
                                            || m.merchant.Contains(searchValue)
                                            || m.address.Contains(searchValue)
                                            || m.npwp.Contains(searchValue)
                                            || m.tid.ToString().Contains(searchValue)
                                            || m.status_owner.Contains(searchValue)
                                            || m.kode_owner.Contains(searchValue)
                                            || m.merek_edc.Contains(searchValue)
                                            || m.serial_number.Contains(searchValue));
                }
                recordsTotal = result.Count();
                var data = result.Skip(skip).Take(pageSize).ToList();
                var jsonData = new { draw = draw, recordsFiltered = recordsTotal, recordsTotal = recordsTotal, data = data };
                return Ok(jsonData);
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        //[HttpGet("GenerateNewSPK/downloadSPK/{owner_code}/{id}")]
        //public IActionResult DownloadSPK(string owner_code, int id)
        //{
        //    try
        //    {
        //        using (var workbook = new XLWorkbook())
        //        {
        //            var worksheet = workbook.Worksheets.Add("Sheet1");
        //            var currentRow = 1;
        //            worksheet.Cell(currentRow, 1).Value = "MID";
        //            worksheet.Cell(currentRow, 2).Value = "Nama Merchant";
        //            worksheet.Cell(currentRow, 3).Value = "Alamat";
        //            worksheet.Cell(currentRow, 4).Value = "NPWP";
        //            worksheet.Cell(currentRow, 5).Value = "TID";
        //            worksheet.Cell(currentRow, 6).Value = "Status Owner";
        //            worksheet.Cell(currentRow, 7).Value = "Kode Owner";
        //            worksheet.Cell(currentRow, 8).Value = "Merek EDC";
        //            worksheet.Cell(currentRow, 9).Value = "Serial Number";

        //            var data = (from header in _context.RequestPemasanganSPK_Header
        //                        join cl_header in _context.Data_CL on header.id_AttachCL equals cl_header.id_CL
        //                        join cl_detail in _context.Data_CL_Detail on cl_header.id_CL equals cl_detail.id_CL
        //                        join tar_header in _context.Data_TAR on header.Id_AttachTAR equals tar_header.id_TAR
        //                        join tar_detail in _context.Data_TAR_Detail on tar_header.id_TAR equals tar_detail.id_TAR
        //                        join data_owner in _context.Data_Owner on cl_detail.id_CL_Detail equals data_owner.id_CL_Detail
        //                        where header.id_header == id && cl_detail.id_CL_Detail == data_owner.id_CL_Detail
        //                        && tar_detail.MID == cl_detail.MID && data_owner.kode_owner == owner_code
        //                        select new
        //                        {
        //                            id_header = header.id_header,
        //                            id_cl = header.id_AttachCL,
        //                            id_tar = header.Id_AttachTAR,
        //                            mid = cl_detail.MID,
        //                            merchant = cl_detail.MERCHANTNAME,
        //                            address = cl_detail.ADDRESS1,
        //                            npwp = cl_detail.NPWP,
        //                            tid = tar_detail.TID,
        //                            status_owner = data_owner.status_owner,
        //                            kode_owner = data_owner.kode_owner,
        //                            merek_edc = data_owner.merek_edc,
        //                            serial_number = data_owner.serial_number

        //                        }).ToList();
        //            foreach (var value in data)
        //            {
        //                currentRow++;
        //                worksheet.Cell(currentRow, 1).Value = "'" + value.mid;
        //                worksheet.Cell(currentRow, 2).Value = value.merchant;
        //                worksheet.Cell(currentRow, 3).Value = value.address;
        //                worksheet.Cell(currentRow, 4).Value = "'" + value.npwp;
        //                worksheet.Cell(currentRow, 5).Value = "'" + value.tid;
        //                worksheet.Cell(currentRow, 6).Value = value.status_owner;
        //                worksheet.Cell(currentRow, 7).Value = value.kode_owner;
        //                worksheet.Cell(currentRow, 8).Value = value.merek_edc;
        //                worksheet.Cell(currentRow, 9).Value = "'" + value.serial_number;
        //            }

        //            using (var stream = new MemoryStream())
        //            {
        //                workbook.SaveAs(stream);
        //                var content = stream.ToArray();

        //                return File(
        //                    content,
        //                    "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
        //                    "SPK - " + owner_code.ToUpper() + ".xlsx");
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        throw;
        //    }
        //}

        [HttpGet("GenerateNewSPK/downloadSPK/{owner_code}/{id}")]
        public IActionResult DownloadSPK(string owner_code, int id)
        {
            try
            {
                string basePath = _environment.WebRootPath + @"\Template\SPK-PASANG.xlsx";
                using (var workbook = new XLWorkbook(basePath))
                {
                    var worksheet = workbook.Worksheets.First();
                    var currentRow = 7;

                    var data = (from header in _context.RequestPemasanganSPK_Header
                                join detail in _context.Detail_RequestPemasanganSPK on header.id_header equals detail.id_header_RPSPK
                                join cl_header in _context.Data_CL on header.id_AttachCL equals cl_header.id_CL
                                join cl_detail in _context.Data_CL_Detail on cl_header.id_CL equals cl_detail.id_CL
                                join tar_header in _context.Data_TAR on header.Id_AttachTAR equals tar_header.id_TAR
                                join tar_detail in _context.Data_TAR_Detail on tar_header.id_TAR equals tar_detail.id_TAR
                                join data_owner in _context.Data_Owner on cl_detail.id_CL_Detail equals data_owner.id_CL_Detail
                                where header.id_header == id && header.staging == detail.staging && cl_detail.id_CL_Detail == data_owner.id_CL_Detail
                                && tar_detail.MID == cl_detail.MID && data_owner.kode_owner == owner_code
                                select new
                                {
                                    id_header = header.id_header,
                                    id_cl = header.id_AttachCL,
                                    id_tar = header.Id_AttachTAR,
                                    mid = cl_detail.MID,
                                    merchant = cl_detail.MERCHANTNAME,
                                    address = cl_detail.ADDRESS1,
                                    npwp = cl_detail.NPWP,
                                    tid = tar_detail.TID,
                                    status_owner = data_owner.status_owner,
                                    kode_owner = data_owner.kode_owner,
                                    merek_edc = data_owner.merek_edc,
                                    serial_number = data_owner.serial_number,
                                    spk_date = detail.create_time.ToString("dd-MMM-yy"),
                                    city = cl_detail.MERCHDBACITY,
                                    zip = cl_detail.ZIP,
                                    phone = cl_detail.PHONE,
                                    pic = cl_detail.CONTACT
                                }).ToList();

                    int no = 1;
                    foreach (var value in data)
                    {
                        for (int i = 1; i <= 56; i++)
                        {
                            if (i == 1)
                            {
                                worksheet.Cell(currentRow, i).Value = no;
                            }
                            else if (i == 4)
                            {
                                worksheet.Cell(currentRow, i).Value = value.spk_date;
                            }
                            else if (i == 8)
                            {
                                worksheet.Cell(currentRow, i).Value = "'" + value.mid;
                            }
                            else if (i == 9)
                            {
                                worksheet.Cell(currentRow, i).Value = "'" + value.tid;
                            }
                            else if (i == 10)
                            {
                                worksheet.Cell(currentRow, i).Value = value.merchant;
                            }
                            else if (i == 11)
                            {
                                worksheet.Cell(currentRow, i).Value = value.address;
                            }
                            else if (i == 12)
                            {
                                worksheet.Cell(currentRow, i).Value = value.city;
                            }
                            else if(i == 13)
                            {
                                worksheet.Cell(currentRow, i).Value = value.zip;
                            }
                            else if (i == 14)
                            {
                                worksheet.Cell(currentRow, i).Value = value.pic;
                            }
                            else if (i == 15)
                            {
                                worksheet.Cell(currentRow, i).Value = value.phone;
                            }
                            else
                            {
                                worksheet.Cell(currentRow, i).Value = "";
                            }
                        }
                        no++;
                        currentRow++;
                    }

                    using (var stream = new MemoryStream())
                    {
                        workbook.SaveAs(stream);
                        var content = stream.ToArray();

                        return File(
                            content,
                            "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
                            "SPK-" + owner_code.ToUpper() + ".xlsx");
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        [HttpPut("GenerateNewSPK/approve/{id}")]
        public IActionResult approveSPK(int id, JsonElement request)
        {
            try
            {
                var cl = _context.Data_CL_Detail.Where(x => x.id_CL_Detail == id).FirstOrDefault();
                if (request.GetProperty("isApprove").GetBoolean())
                {
                    cl.isProcessed = 2;
                }
                else
                {
                    cl.isProcessed = 0;
                }

                _context.SaveChanges();
                return Ok(new ResponseStatus { Status = "Success", Message = "Data has been approved", Data = cl });
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!_context.Data_CL_Detail.Any(e => e.id_CL_Detail == id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }
        }

        [HttpGet("GenerateNewSPK/CheckProcess/{id}")]
        public async Task<IActionResult> CheckProcess([FromHeader] string token, int id)
        {
            var check = (from header in _context.RequestPemasanganSPK_Header
                        join cl_header in _context.Data_CL on header.id_AttachCL equals cl_header.id_CL
                        join cl_detail in _context.Data_CL_Detail on cl_header.id_CL equals cl_detail.id_CL
                        where header.id_header == id && cl_detail.isProcessed == 2
                        select new
                        {
                            cl_detail
                        }).AsQueryable().ToList();            

            return Ok(new ResponseStatus { Status = "Success", Message = "Check process has been retrieve", Data = check });
        }

        [HttpGet("GenerateNewSPK/CheckGenerate/{id}")]
        public async Task<IActionResult> CheckGenerate([FromHeader] string token, int id)
        {
            var check_pemasangan = (from header in _context.RequestPemasanganSPK_Header
                         join cl_header in _context.Data_CL on header.id_AttachCL equals cl_header.id_CL
                         join cl_detail in _context.Data_CL_Detail on cl_header.id_CL equals cl_detail.id_CL
                         join tar_header in _context.Data_TAR on header.Id_AttachTAR equals tar_header.id_TAR
                         join tar_detail in _context.Data_TAR_Detail on tar_header.id_TAR equals tar_detail.id_TAR
                         join pemasangan in _context.Status_Pemasangan on header.id_header equals pemasangan.id_spk
                         where header.id_header == id && tar_detail.MID == cl_detail.MID && cl_detail.isProcessed == 1 && tar_detail.TID == pemasangan.tid
                         select pemasangan).AsQueryable().ToList();

            var check_is_processed = (from header in _context.RequestPemasanganSPK_Header
                                      join cl_header in _context.Data_CL on header.id_AttachCL equals cl_header.id_CL
                                      join cl_detail in _context.Data_CL_Detail on cl_header.id_CL equals cl_detail.id_CL
                                      where header.id_header == id && cl_detail.isProcessed == 1
                                      select cl_detail).AsQueryable().ToList();

            return Ok(new ResponseStatus { Status = "Success", Message = "Check generate has been retrieve", Data = new { check_pemasangan = check_pemasangan, check_is_processed = check_is_processed } });
        }

        [HttpPut("GenerateNewSPK/process/{id}")]
        public IActionResult processSPK(int id)
        {
            var cl = (from header in _context.RequestPemasanganSPK_Header
                      join cl_header in _context.Data_CL on header.id_AttachCL equals cl_header.id_CL
                      join cl_detail in _context.Data_CL_Detail on cl_header.id_CL equals cl_detail.id_CL
                      where header.id_header == id && cl_detail.isProcessed == 2
                      select cl_detail).AsQueryable().ToList();

            foreach (var item in cl)
            {
                item.isProcessed = 1;
            }

            _context.SaveChanges();

            return Ok(new ResponseStatus { Status = "Success", Message = cl.Count() + " of data has been processed", Data = cl });
        }

        [HttpPut("GenerateNewSPK/generate/{id}")]
        public async Task<IActionResult> generateSPK([FromHeader] string token, int id)
        {
            var authorization = authenticateManager.isLoggedIn(token);
            if (authorization is false)
                return StatusCode(StatusCodes.Status401Unauthorized, new ResponseStatus { Status = "Error", Message = "You can't access this method !" });

            var rpspk = _context.RequestPemasanganSPK_Header.Where(x => x.id_header == id).FirstOrDefault();

            if(rpspk.staging != 6)
            {
                rpspk.staging = 6;

                DetailRequestPemasanganSPK detail_spk = new DetailRequestPemasanganSPK()
                {
                    id_header_RPSPK = rpspk.id_header,
                    staging = 6,
                    create_by = authorization.id_user,
                    create_time = DateTime.Now
                };
                _context.Detail_RequestPemasanganSPK.Add(detail_spk);

                await _context.SaveChangesAsync();
            }

            var data = (from header in _context.RequestPemasanganSPK_Header
                        join detail in _context.Detail_RequestPemasanganSPK on header.id_header equals detail.id_header_RPSPK
                        join cl_header in _context.Data_CL on header.id_AttachCL equals cl_header.id_CL
                        join cl_detail in _context.Data_CL_Detail on cl_header.id_CL equals cl_detail.id_CL
                        join tar_header in _context.Data_TAR on header.Id_AttachTAR equals tar_header.id_TAR
                        join tar_detail in _context.Data_TAR_Detail on tar_header.id_TAR equals tar_detail.id_TAR
                        join data_owner in _context.Data_Owner on cl_detail.id_CL_Detail equals data_owner.id_CL_Detail
                        join user in _context.user on detail.create_by equals user.id_user
                        join role in _context.role on user.id_role equals role.id_role
                        where header.id_header == id && detail.staging == 1 && cl_detail.id_CL_Detail == data_owner.id_CL_Detail
                        && tar_detail.MID == cl_detail.MID && cl_detail.isProcessed == 1
                        select new
                        {
                            id_rpspk = header.id_header,
                            merchant = cl_detail.MERCHANTNAME,
                            tid = tar_detail.TID,
                            data_owner.kode_owner,
                            role_type = role.type
                        }).ToList();

            foreach (var item in data)
            {

                var check = _context.Status_Pemasangan.Where(x => x.tid == item.tid && x.id_spk == item.id_rpspk).FirstOrDefault();

                if(check == null)
                {
                    var datestring = $"{DateTime.Now.Year.ToString()}{DateTime.Now.ToString("MM")}";
                    var check_no = _context.Status_Pemasangan.Where(x => x.no_tugas.Substring(x.no_tugas.Length - 11, 11).Substring(0, 6) == datestring).OrderByDescending(x => x.no_tugas).FirstOrDefault();
                    var no_tugas = "";
                    if (check_no is null)
                    {
                        no_tugas = datestring + "00001";
                    }
                    else
                    {
                        Int64 id_request = Int64.Parse(check_no.no_tugas.Substring(check_no.no_tugas.Length - 11, 11)) + 1;
                        no_tugas = id_request.ToString();
                    }

                    var code = "NP";
                    if(item.role_type == "Wilayah")
                    {
                        code += authorization.wilayah.code;
                    }
                    else
                    {
                        code += "DGO";
                    }

                    Status_Pemasangan statusPemasangan = new Status_Pemasangan()
                    {
                        no_tugas = code + no_tugas,
                        id_spk = item.id_rpspk,
                        merchant = item.merchant,
                        tid = item.tid,
                        tanggal_penugasan = DateTime.Now,
                        staging = 1,
                        vendor = item.kode_owner,
                        create_by = authorization.id_user,
                        update_by = authorization.id_user,
                        updated_time = DateTime.Now
                    };

                    _context.Status_Pemasangan.Add(statusPemasangan);

                    await _context.SaveChangesAsync();

                    Detail_Status_Pemasangan detailStatusPemasangan = new Detail_Status_Pemasangan()
                    {
                        id_header_pemasangan = statusPemasangan.id_pemasangan,
                        staging = 1,
                        create_by = authorization.id_user
                    };

                    _context.Detail_Status_Pemasangan.Add(detailStatusPemasangan);

                    await _context.SaveChangesAsync();
                }
            }

            return Ok(new ResponseStatus { Status = "Success", Message = data.Count() + " of data has been generated", Data = new { rpspk = rpspk } });
        }

        [HttpPut("SubmitGenerateNewSPK/{id}")]
        public async Task<IActionResult> SubmitGenerateNewSPK(int id, [FromHeader] string token)
        {
            var authorization = authenticateManager.isLoggedIn(token);
            if (authorization is false)
                return StatusCode(StatusCodes.Status401Unauthorized, new ResponseStatus { Status = "Error", Message = "You can't access this method !" });

            try
            {
                var rpspk = _context.RequestPemasanganSPK_Header.Where(x => x.id_header == id).FirstOrDefault();
                rpspk.staging = 6;

                DetailRequestPemasanganSPK drpspk = new DetailRequestPemasanganSPK()
                {
                    id_header_RPSPK = rpspk.id_header,
                    staging = 6,
                    create_by = authorization.id_user,
                    create_time = DateTime.Now
                };
                _context.Detail_RequestPemasanganSPK.Add(drpspk);

                await _context.SaveChangesAsync();

                var data = (from header in _context.RequestPemasanganSPK_Header
                              join cl_header in _context.Data_CL on header.id_AttachCL equals cl_header.id_CL
                              join cl_detail in _context.Data_CL_Detail on cl_header.id_CL equals cl_detail.id_CL
                              join tar_header in _context.Data_TAR on header.Id_AttachTAR equals tar_header.id_TAR
                              join tar_detail in _context.Data_TAR_Detail on tar_header.id_TAR equals tar_detail.id_TAR
                              join data_owner in _context.Data_Owner on cl_detail.id_CL_Detail equals data_owner.id_CL_Detail
                              where header.id_header == id && cl_detail.id_CL_Detail == data_owner.id_CL_Detail
                              && tar_detail.MID == cl_detail.MID
                              select new
                              {
                                  merchant = cl_detail.MERCHANTNAME,
                                  tid = tar_detail.TID,
                                  data_owner.kode_owner
                              }).ToList();

                foreach(var item in data)
                {
                    var datestring = $"{DateTime.Now.Year.ToString()}{DateTime.Now.ToString("MM")}";
                    var check_no = _context.Status_Pemasangan.Where(x => x.no_tugas.Substring(x.no_tugas.Length - 11, 11).Substring(0, 6) == datestring).OrderByDescending(x => x.no_tugas).FirstOrDefault();
                    var no_tugas = "";
                    if (check_no is null)
                    {
                        no_tugas = datestring + "00001";
                    }
                    else
                    {
                        Int64 id_request = Int64.Parse(check_no.no_tugas.Substring(check_no.no_tugas.Length - 11, 11)) + 1;
                        no_tugas = id_request.ToString();
                    }

                    Status_Pemasangan statusPemasangan = new Status_Pemasangan()
                    {
                        no_tugas = "NPWJS" + no_tugas,
                        merchant = item.merchant,
                        tid = item.tid,
                        tanggal_penugasan = DateTime.Now,
                        staging = 1,
                        vendor = item.kode_owner,
                        create_by = authorization.id_user,
                        update_by = authorization.id_user,
                        updated_time = DateTime.Now
                    };

                    _context.Status_Pemasangan.Add(statusPemasangan);

                    await _context.SaveChangesAsync();

                    Detail_Status_Pemasangan detailStatusPemasangan = new Detail_Status_Pemasangan()
                    {
                        id_header_pemasangan = statusPemasangan.id_pemasangan,
                        staging = 1,
                        create_by = authorization.id_user
                    };

                    _context.Detail_Status_Pemasangan.Add(detailStatusPemasangan);

                    await _context.SaveChangesAsync();
                }

                return Ok(new ResponseStatus { Status = "Success", Message = "Request Pemasangan has been generated", Data = new { rpspk = rpspk, drpspk = drpspk } });
            }
            catch (Exception ex)
            {
                throw;
            }
        }
    }
}
