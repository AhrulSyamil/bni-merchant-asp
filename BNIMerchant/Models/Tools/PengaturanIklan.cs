﻿using System;
using System.ComponentModel.DataAnnotations;

namespace BNIMerchant.Models.Tools
{
    public class PengaturanIklan
    {
        [Key]
        public int id { get; set; }
        public string nama { get; set; }
        public string image { get; set; }
        public string position { get; set; }
        public string link { get; set; }
        public string status { get; set; }
        public DateTime create_time { get; set; }
        public DateTime update_time { get; set; }
    }
}
