﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace BNIMerchant.Models.Tools
{
    public class PicPelaksanaan
    {
        [Key]
        public int id { get; set; }
        public string nama { get; set; }
        public int no_ktp { get; set; }
        public int nik { get; set; }
        public int no_hp { get; set; }
        public string wilayah { get; set; }
        public DateTime create_time { get; set; }
        public DateTime update_time { get; set; }
    }
}
