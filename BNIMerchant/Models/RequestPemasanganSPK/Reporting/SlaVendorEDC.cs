﻿using System;
using System.ComponentModel.DataAnnotations;

namespace BNIMerchant.Models.Reporting
{
    public class SlaVendorEDC
    {
        [Key]
        public int id { get; set; }
        public string staging { get; set; }
        public DateTime date { get; set; }
        public DateTime create_time { get; set; }
        public DateTime update_time { get; set; }
    }
}
