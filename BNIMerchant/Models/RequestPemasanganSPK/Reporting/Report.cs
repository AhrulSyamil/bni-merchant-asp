﻿using System;
using System.ComponentModel.DataAnnotations;

namespace BNIMerchant.Models.Reporting
{
    public class Report
    {
        [Key]
        public int id { get; set; }
        public DateTime date { get; set; }
        public string wilayah { get; set; }
        public string vendor { get; set; }
        public DateTime create_time { get; set; }
        public DateTime update_time { get; set; }
    }
}
