﻿using System;
using System.ComponentModel.DataAnnotations;

namespace BNIMerchant.Models.RequestPemasanganSPK
{
    public class DetailRequestPemasanganSPK
    {

        public DetailRequestPemasanganSPK()
        {
            create_time = DateTime.Now;
        }

        [Key]
        public int id_detail { get; set; }
        public int id_header_RPSPK { get; set; }
        public int staging { get; set; }
        public int create_by { get; set; }
        public DateTime create_time { get; set; }
    }
}   
