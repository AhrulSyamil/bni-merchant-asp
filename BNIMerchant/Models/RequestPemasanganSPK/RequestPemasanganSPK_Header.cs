﻿using System.ComponentModel.DataAnnotations;

namespace BNIMerchant.Models.RequestPemasanganSPK
{
    public class RequestPemasanganSPK_Header
    {

        [Key]
        public int id_header { get; set; }
        public string id_request { get; set; }
        public int id_AttachMRF { get; set; }
        public int id_AttachCL { get; set; }
        public int Id_AttachProfiling { get; set; }
        public int Id_AttachMismer { get; set; }
        public int Id_AttachTAR { get; set; }
        public int staging { get; set; }
        public int status_CLProfiling { get; set; }
        public int status_TIDComplete { get; set; }
        public int status { get; set; }
    }
}
