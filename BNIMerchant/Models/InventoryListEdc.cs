﻿using System;
using System.ComponentModel.DataAnnotations;

namespace BNIMerchant.Models
{
    public class InventoryListEdc
    {
        [Key]
        public int id_inventory { get; set; }
        public string project { get; set; }
        public string tanggal_form { get; set; }
        public string month_sc { get; set; }
        public string batch { get; set; }
        public string no_spk { get; set; }
        public string tanggal_spk { get; set; }
        public string no_bast { get; set; }
        public string month_wh { get; set; }
        public string tanggal_bast { get; set; }
        public string tanggal_terima { get; set; }
        public string tanggal_done_init { get; set; }
        public string type { get; set; }
        public string part_number { get; set; }
        public string serial_number { get; set; }
        public string tid_sn { get; set; }
        public string status_simcard { get; set; }
        public string provider { get; set; }
        public string apn { get; set; }
        public string sim_card { get; set; }
        public string serial_number_sim_card { get; set; }
        public string iccid { get; set; }
        public string tid_bni { get; set; }
        public string mid { get; set; }
        public string nama_merchant { get; set; }
        public string kota { get; set; }
        public string cnm { get; set; }
        public string lokasi { get; set; }
        public string status_keluar { get; set; }
        public string tanggal_terpasang { get; set; }
        public string month_do { get; set; }
        public string month { get; set; }
        public string status { get; set; }
        public string status1 { get; set; }
        public string ip_apn_simcard { get; set; }
        public string tanggal_pengembalian { get; set; }
        public string tanggal_masuk_wh_pengembalian { get; set; }
        public string month_pengembalian { get; set; }
        public string status_edc { get; set; }
        public string status_wd { get; set; }
        public string tanggal_wd { get; set; }
        public string month_wd { get; set; }
        public string expedisi { get; set; }
        public string awb_pod { get; set; }
        public string note_produksi { get; set; }
        public string note_fs { get; set; }
        public string detail { get; set; }
        public DateTime? create_time { get; set; }
        public DateTime? update_time { get; set; }
    }
}
