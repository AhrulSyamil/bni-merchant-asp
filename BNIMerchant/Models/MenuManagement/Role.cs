﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace BNIMerchant.Models.MenuManagement
{
    public class Role
    {
        [Key]
        public int id_role { get; set; }
        public string name { get; set; }
        public string type { get; set; }
        public string description { get; set; }
        public int? status { get; set; }
        public DateTime? create_time { get; set; }
        public DateTime? update_time { get; set; }

        [DefaultValue(false)]
        public bool isDeleted { get; set; }
    }
}
