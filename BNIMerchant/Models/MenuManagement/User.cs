﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace BNIMerchant.Models.MenuManagement
{
    public class User : ISoftDelete
    {
        [Key]
        public int id_user { get; set; }
        public int id_role { get; set; }
        public int? id_wilayah { get; set; }
        public string username { get; set; }
        public string email { get; set; }
        public string password { get; set; }
        public string session_login { get; set; }
        public int status { get; set; }
        public string token { get; set; }
        public DateTime create_time { get; set; }
        public DateTime update_time { get; set; }

        [DefaultValue(false)]
        public bool isDeleted { get; set; }
    }
}
