﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace BNIMerchant.Models
{
   
    public class JSONModel
    {
        public class Rootobject
        {
            public List<Role> DataRoles { get; set; }
        }


        [Keyless]
        public class Role
        {
            
            public int id_role { get; set; }
            public string name { get; set; }
            public string description { get; set; }
            public int? status { get; set; }
            public DateTime? create_time { get; set; }
            public DateTime? update_time { get; set; }

        }

    }
}
