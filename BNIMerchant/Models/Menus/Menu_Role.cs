﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace BNIMerchant.Models.Menus
{
    public class Menu_Role
    {
        [Key]
        public int id { get; set; }
        public int id_menu { get; set; }
        public int id_role { get; set; }
    }
}
