﻿using System.ComponentModel.DataAnnotations;

namespace BNIMerchant.Models.Menus
{
    public class Menu
    {
        [Key]
        public int id_menu { get; set; }
        public string name { get; set; }
        public string route { get; set; }
        public string icon { get; set; }
        public string parent { get; set; }
        public int order { get; set; }
        public int active { get; set; }
    }
}
