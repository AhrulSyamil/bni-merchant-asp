﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace BNIMerchant.Models.StatusPemasangan
{
    public class Detail_Status_Pemasangan
    {
        public Detail_Status_Pemasangan()
        {
            create_time = DateTime.Now;
        }

        [Key]
        public int id_detail { get; set; }
        public int id_header_pemasangan { get; set; }
        public int staging { get; set; }
        public int create_by { get; set; }
        public DateTime create_time { get; set; }
    }
}
