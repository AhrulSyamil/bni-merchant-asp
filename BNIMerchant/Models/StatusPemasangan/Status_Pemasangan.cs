﻿using System;
using System.ComponentModel.DataAnnotations;

namespace BNIMerchant.Models.StatusPemasangan
{
    public class Status_Pemasangan
    {
        public Status_Pemasangan()
        {
            created_time = DateTime.Now;
        }

        [Key]
        public int id_pemasangan { get; set; }
        public int id_spk { get; set; }
        public string no_tugas { get; set; }
        public string merchant { get; set; }
        public string vendor { get; set; }
        public string tid { get; set; }
        public string? pic_vendor { get; set; }
        public string? hp { get; set; }
        public DateTime? tanggal_penugasan { get; set; }
        public DateTime? tanggal_dipasang { get; set; }
        public int? staging { get; set; }
        public string? reason { get; set; }
        public string? info_remark { get; set; }
        public string? photo_evidence { get; set; }
        public string? photo_toko { get; set; }
        public string? photo_fkm { get; set; }
        public string? photo_mesin_edc { get; set; }
        public string? photo_slip_transaksi { get; set; }
        public string? status { get; set; }
        public int? create_by { get; set; }
        public int? update_by { get; set; }
        public DateTime? created_time { get; set; }
        public DateTime? updated_time { get; set; }
    }
}
