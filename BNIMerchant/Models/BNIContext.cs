﻿using BNIMerchant.Models.Data;
using BNIMerchant.Models.MenuManagement;
using BNIMerchant.Models.Menus;
using BNIMerchant.Models.Reporting;
using BNIMerchant.Models.RequestPemasanganSPK;
using BNIMerchant.Models.StatusPemasangan;
using BNIMerchant.Models.Tools;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BNIMerchant.Models
{
    public class BNIContext : DbContext
    {
        public BNIContext(DbContextOptions<BNIContext> options) : base(options) { }
        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            builder.Entity<User>()
                .HasQueryFilter(p => !p.isDeleted);

            builder.Entity<Role>()
                .HasQueryFilter(p => !p.isDeleted);

            builder.Entity<Wilayah>()
                .HasQueryFilter(p => !p.isDeleted);
        }

        //MenuManagement
        public DbSet<User> user { get; set; }
        public DbSet<Role> role { get; set; }

        //Menu
        public DbSet<Menu> Menu { get; set; }
        public DbSet<Menu_Role> Menu_Role { get; set; }
        public DbSet<Wilayah> Wilayah { get; set; }

        //DaftarRequestPemasanganSPK
        public DbSet<RequestPemasanganSPK_Header> RequestPemasanganSPK_Header { get; set; }
        public DbSet<DetailRequestPemasanganSPK> Detail_RequestPemasanganSPK { get; set; }

        //Reporting
        public DbSet<Report> reporting { get; set; }
        public DbSet<SlaVendorEDC> sla_vendor_edc { get; set; }
        //Tools
        public DbSet<PengaturanIklan> pengaturan_iklan { get; set; }
        public DbSet<PicPelaksanaan> pic_pelaksanaan { get; set; }

        //Data
        public DbSet<Data_CL> Data_CL { get; set; }
        public DbSet<Data_CL_Detail> Data_CL_Detail { get; set; }
        public DbSet<Data_Mismer> Data_Mismer { get; set; }
        public DbSet<Data_Mismer_Detail> Data_Mismer_Detail { get; set; }
        public DbSet<Data_MRF> Data_MRF { get; set; }
        public DbSet<Data_Profiling> Data_Profiling { get; set; }
        public DbSet<Data_TAR> Data_TAR { get; set; }
        public DbSet<Data_Owner> Data_Owner { get; set; }
        public DbSet<Data_TAR_Detail> Data_TAR_Detail { get; set; }
        public DbSet<InventoryListEdc> Inventory { get; set; }
        public object TestRegisters { get; internal set; }

        public DbSet<Status_Pemasangan> Status_Pemasangan { get; set; }
        public DbSet<Detail_Status_Pemasangan> Detail_Status_Pemasangan { get; set; }

        public override int SaveChanges()
        {
            foreach (var entry in ChangeTracker.Entries())
            {
                var entity = entry.Entity;

                if (entry.State == EntityState.Deleted && entity is ISoftDelete)
                {
                    entry.State = EntityState.Modified;

                    entity.GetType().GetProperty("isDeleted").SetValue(entity, true);
                }
            }

            return base.SaveChanges();
        }
    }
}

