﻿using System;
using System.ComponentModel.DataAnnotations;

namespace BNIMerchant.Models.Data
{
    public class Data_Mismer_Detail
    {
        public Data_Mismer_Detail()
        {
            create_time = DateTime.Now;
        }

        [Key]
        public int id_Mismer_Detail { get; set; }
        public int id_Mismer { get; set; }
        public string SOURCEDATA { get; set; }
        public string WILAYAH { get; set; }
        public string KATEGORI_MERCHANT { get; set; }
        public string MID { get; set; }
        public string DBANAME { get; set; }
        public string DBACITY { get; set; }
        public string CONTACT { get; set; }
        public string MSO { get; set; }
        public string MMO { get; set; }
        public string SOURCECODE { get; set; }
        public string MERCHANTNAME { get; set; }
        public string ADDRESS { get; set; }
        public string KODEPOS { get; set; }
        public string MCC { get; set; }
        public string JUMLAHEDC { get; set; }
        public string OWNER { get; set; }
        public int create_by { get; set; }
        public DateTime create_time { get; set; }
    }
}
