﻿using System;
using System.ComponentModel.DataAnnotations;

namespace BNIMerchant.Models.Data
{
    public class Data_TAR_Detail
    {
        public Data_TAR_Detail()
        {
            create_time = DateTime.Now;
        }

        [Key]
        public int id_TAR_Detail { get; set; }
        public int id_TAR { get; set; }
        public string MID { get; set; }
        public string TID { get; set; }
        public int create_by { get; set; }
        public DateTime create_time { get; set; }
    }
}
