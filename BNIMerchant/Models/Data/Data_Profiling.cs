﻿using System.ComponentModel.DataAnnotations;

namespace BNIMerchant.Models.Data
{
    public class Data_Profiling
    {
        [Key]
        public int id_Profiling { get; set; }
        public int MID { get; set; }
        public int TID { get; set; }
        public string SERIALNUMBEREDC { get; set; }
        public string MERKTYPE { get; set; }
        public string PROVIDERSIMCARD { get; set; }
        public string VERSISOFTWARE { get; set; }
        public string MASTERPROFILING { get; set; }
    }
}
