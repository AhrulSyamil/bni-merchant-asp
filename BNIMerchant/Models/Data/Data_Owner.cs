﻿using System;
using System.ComponentModel.DataAnnotations;

namespace BNIMerchant.Models.Data
{
    public class Data_Owner
    {
        public Data_Owner()
        {
            create_time = DateTime.Now;
        }

        [Key]
        public int id_owner { get; set; }
        public string status_owner { get; set; }
        public string kode_owner { get; set; }
        public string merek_edc { get; set; }
        public string serial_number { get; set; }
        public string master { get; set; }
        public int create_by { get; set; }
        public DateTime create_time { get; set; }
        public int id_CL_Detail { get; set; }
    }
}
