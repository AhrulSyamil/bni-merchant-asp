﻿using BNIMerchant.Auth;
using System;
using System.ComponentModel.DataAnnotations;

namespace BNIMerchant.Models.Data
{
    public class Data_MRF
    {
        public Data_MRF()
        {
            create_time = DateTime.Now;
            isApproved = 0;
        }

        [Key]
        public int id_MRF { get; set; }
        public string name_file { get; set; }
        public int isApproved { get; set; }
        public int create_by { get; set; }
        public DateTime create_time { get; set; }
        public string? directory { get; set; }
        public int? approved_by { get; set; }
    }

}
