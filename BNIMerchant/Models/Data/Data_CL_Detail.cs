﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace BNIMerchant.Models.Data
{
    public class Data_CL_Detail
    {

        public Data_CL_Detail()
        {
            create_time = DateTime.Now;
            isApproved = 1;
        }

        [Key]
        public int id_CL_Detail { get; set; }
        public int id_CL { get; set; }
        public string ORG { get; set; }
        public string KATEGORI { get; set; }
        public string KODEWILAYAH { get; set; }
        public string MID { get; set; }
        public string KODECBG { get; set; }
        public string MERCHDBANAME { get; set; }
        public string MERCHDBACITY { get; set; }
        public string CONTACT { get; set; }
        public string MEMO { get; set; }
        public string PHONE { get; set; }
        public string MSO { get; set; }
        public string MMO { get; set; }
        public string MERCHANTTYPE { get; set; }
        public string SOURCECODE { get; set; }
        public string MERCHANTNAME { get; set; }
        public string ADDRESS1 { get; set; }
        public string ADDRESS2 { get; set; }
        public string ADDRESS3 { get; set; }
        public string ADDRESS4 { get; set; }
        public string ZIP { get; set; }
        public string MCC { get; set; }
        public string POS { get; set; }
        public string PLAN1 { get; set; }
        public string PLAN2 { get; set; }
        public string PLAN3 { get; set; }
        public string INSTLSPL1 { get; set; }
        public string INSTLSPL2 { get; set; }
        public string AGENTBANK { get; set; }
        public string BRANCH { get; set; }
        public string FLAGMERCHANT { get; set; }
        public string NAMANASABAH { get; set; }
        public string NOMORREKENING { get; set; }
        public string NPWP { get; set; }
        public string NAMABANK { get; set; }
        public string MAILORDERIND { get; set; }
        public string VISABNI { get; set; }
        public string VISAMCJCB { get; set; }
        public string MCBNI { get; set; }
        public string MCDEBITBNI { get; set; }
        public string LOCALDEBIT { get; set; }
        public string JCBBNI { get; set; }
        public string RESERVED { get; set; }
        public string PRIVATELABEL { get; set; }
        public string BNIPREPAID { get; set; }
        public string WHITELIST { get; set; }
        public string BNMS { get; set; }
        public int create_by { get; set; }
        public int isApproved { get; set; }
        public int isProcessed { get; set; }
        public DateTime create_time { get; set; }
    }
}
