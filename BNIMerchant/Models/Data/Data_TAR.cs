﻿using System;
using System.ComponentModel.DataAnnotations;

namespace BNIMerchant.Models.Data
{
    public class Data_TAR
    {
        public Data_TAR()
        {
            create_time = DateTime.Now;
            isApproved = 0;
        }

        [Key]
        public int id_TAR { get; set; }
        public int isApproved { get; set; }
        public int create_by { get; set; }
        public DateTime create_time { get; set; }
    }
}
