﻿using System;
using System.ComponentModel.DataAnnotations;

namespace BNIMerchant.Models.Data
{
    public class Data_CL
    {

        public Data_CL()
        {
            create_time = DateTime.Now;
            isApproved = 0;
        }

        [Key]
        public int id_CL { get; set; }
        public int isApproved { get; set; }
        public int create_by { get; set; }
        public DateTime create_time { get; set; }
        public int? approved_by { get; set; }
    }
}
