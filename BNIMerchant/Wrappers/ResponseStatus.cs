﻿namespace BNIMerchant.Wrappers
{
    public class ResponseStatus
    {
        public string Status { get; set; }
        public string Message { get; set; }
        public dynamic? Data { get; set; }
    }
}
