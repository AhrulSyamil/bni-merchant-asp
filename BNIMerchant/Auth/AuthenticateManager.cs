﻿using System;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using BNIMerchant.Models;

namespace BNIMerchant.Auth
{
    public interface IAuthenticateManager
    {
        string CreateToken(string username);
        dynamic isLoggedIn(string token);
        string Sha256Encrypt(string password);
    }
    public class AuthenticateManager : IAuthenticateManager
    {
        private readonly BNIContext _context;

        public AuthenticateManager(BNIContext context)
        {
            _context = context;
        }

        public string CreateToken(string username)
        {
            var token = Guid.NewGuid().ToString();
            return token;
        }

        public dynamic isLoggedIn(string token)
        {
            if (token == null) return false;
            var validate = _context.user.Where(x => x.token == token).FirstOrDefault();
            if (validate == null)
                return false;
            else
                return new {
                    id_user = validate.id_user,
                    id_wilayah = validate.id_wilayah,
                    id_role = validate.id_role,
                    role = _context.role.Where(x => x.id_role == validate.id_role).FirstOrDefault(),
                    wilayah = _context.Wilayah.Where(x => x.id_wilayah == validate.id_wilayah).FirstOrDefault()
                };
        }

        public string Sha256Encrypt(string password)
        {
            UTF8Encoding encoder = new UTF8Encoding();
            SHA256Managed sha256hasher = new SHA256Managed();
            byte[] hashedDataBytes = sha256hasher.ComputeHash(encoder.GetBytes(password));
            return Convert.ToBase64String(hashedDataBytes);
        }
    }
}
