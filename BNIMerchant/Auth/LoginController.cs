﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Linq;
using BNIMerchant.Auth;
using BNIMerchant.Models;
using BNIMerchant.Models.MenuManagement;
using BNIMerchant.Wrappers;

namespace BNIMerchant.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LoginController : ControllerBase
    {
        private readonly BNIContext _context;
        private readonly IAuthenticateManager authenticateManager;
        public LoginController(BNIContext context, IAuthenticateManager authenticateManager)
        {
            this.authenticateManager = authenticateManager;
            _context = context;
        }

        [HttpPost("SignIn")]
        public IActionResult SignIn([FromBody] User user)
        {
            string passEncrypted = authenticateManager.Sha256Encrypt(user.password);
            var entity = _context.user.FirstOrDefault(x => x.username == user.username && x.password == passEncrypted);
            if (entity == null)
                return Ok(new ResponseStatus { Status = "Error", Message = "Unauthorized" });
            return Ok(new ResponseStatus { Status = "Success", Message = "Signin successfully", Data = new {
                username = entity.username,
                email = entity.email,
                token = entity.token,
                role = _context.role.Where(x => x.id_role == entity.id_role).FirstOrDefault()
            } });
        }
    }
}
